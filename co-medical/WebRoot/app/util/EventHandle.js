Ext
		.define(
				'comedical.util.EventHandle',
				{
					singleton : true,

					config : {},
					constructor : function(config) {
						this.initConfig(config);
					},
					events : {

						leftMenuClick : function(parent, item, callback) {
							var desktop = Ext.ComponentQuery
									.query("panel[id='tab-main']")[0];

							if (!desktop.queryById(item.id)) {
								panel = Ext.create(item.panel, {
									title : item.name,
									id : item.id,
									closable : true
								})
								if (panel.initialData) {
									panel.initialData();
								}
								desktop.add(panel)
							}

							desktop.setActiveTab(item.id);
						},

						ShowForm : function(isUpdate, grid, record, title,
								form_name, callback, width, height) {
							w = 600
							h = 500
							if (width && height) {
								w = width;
								h = height;
							}
							form = Ext.create(form_name);
							if (form.check) {
								form.check();
							} 
							if (isUpdate) {
								if (record.length > 0) {
									record = record[0];
								}
									form.getForm().loadRecord(record)
							}

							Ext.create('Ext.window.Window', {
								title : title,
								height : h,
								width : w,
								layout : 'fit',
								autoScroll : true,
								items : [ form ],
								dockedItems : [ {
									xtype : 'toolbar',
									dock : 'bottom',
									ui : 'footer',
									items : [ {
										xtype : 'component',
										flex : 1
									}, {
										xtype : 'button',
										text : '提交',
										handler : function() {
											callback(form, grid, isUpdate)
										}

									} ]
								} ]
							}).show();
						},
						OpenFlowEditWindow : function(record) {
							win = Ext.getCmp('flow-win');
							if (win) {
								win.items.items[0].initialPanel(record);

							} else {
								win = Ext
										.create('comedical.view.config.panel.FlowContainer');
								win.initialPanel(record);

							}
							win.show();
						},

						OpenPrivateQuestionWindow : function(record) {
							panel = Ext
									.create('comedical.view.config.form.QuestionPanel');
							panel.setValue(record);
							panel.questionid = record.getData().id;
							Ext
									.create(
											'Ext.window.Window',
											{
												title : '医生咨询',
												height : 700,
												width : 900,
												autoScroll : true,
												items : [ panel ],
												dockedItems : [ {
													xtype : 'toolbar',
													dock : 'bottom',
													ui : 'footer',
													items : [
															{
																xtype : 'textareafield',
																grow : true,
																name : 'message',
																fieldLabel : '回复',
																anchor : '100%',
																flex : 1
															},
															{
																xtype : 'button',
																text : '提交',
																handler : function() {
																	uid = Ext.util.Cookies
																			.get("uid");
																	content = this
																			.up().items.items[0]
																			.getValue();
																	it = Ext
																			.getCmp('questionPanel');
																	comedical.util.DataApi.Core
																			.addPrivateQuestionAnswer(
																					function(
																							res,
																							scope) {
																						res = Ext
																								.decode(res);
																						if (res.code == 200) {
																							scope
																									.addDoctorAnwser(
																											res.result[0],
																											scope)
																						}

																					},
																					it,
																					{
																						'QuestionID' : it.questionid,
																						'uid' : uid,
																						'content' : content
																					})
																}

															} ]
												} ]
											}).show();
						}
					}

				});