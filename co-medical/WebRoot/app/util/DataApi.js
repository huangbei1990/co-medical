Ext.define('comedical.util.DataApi', {
    singleton : true,

    config : {   
    },
    constructor : function(config) {
        this.initConfig(config);
    },
    Core:{
    	/**
    	 * 菜单列表
    	 * @param {} callback
    	 * @param {} scope
    	 * @param {} param
    	 ******************************************/
    	getMenuList:function(callback,scope,param){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"getMenuList",param,
    	    		callback,scope)
    	},
    	/***************************************************************************/
    	
    	
/**×××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××
    	 * 类别操作
    	 * @param {} callback
    	 * @param {} scope
    	 * @param {} param
  **********************************************************************************************************/  	
    	getChildByID:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"CategoryService/getChildByID",param,
    	    		callback,scope)

    	},
    	
    	getAllAdminUser:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"getAllAdminUser",param,
    	    		callback,scope);

    	},
    	
    	getAllPermission:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"getAllPermission",param,
    	    		callback,scope,uppa);

    	},
    	
    	getAllRole:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"getAllRole",param,
    	    		callback,scope,uppa);

    	},
    	
    	addNewRecord:function(callback,scope,param,uppa){
    		comedical.util.DataApi.postData(
    	    		comedical.util.Config.getService()+"addNewRecord",param,
    	    		callback,scope,uppa);

    	},
    	deleteRecord:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"deleteRecord",param,
    	    		callback,scope,uppa);

    	},
    	getUserRole:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"getUserRole",param,
    	    		callback,scope,uppa);

    	},
    	getRootPermission:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"getRootPermission",param,
    	    		callback,scope,uppa);

    	},
    	getRolePermission:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"getRolePermission",param,
    	    		callback,scope,uppa);

    	},
    	addLnkUserRole:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"addLnkUserRole",param,
    	    		callback,scope,uppa);

    	},
    	addLnkRolePermission:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"addLnkRolePermission",param,
    	    		callback,scope,uppa);

    	},
    	
    	getAllArea:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"getAllArea",param,
    	    		callback,scope,uppa);

    	},
    	
    	getAllAreaJson:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"getAllAreaJson",param,
    	    		callback,scope,uppa);

    	},
    	
    	
    	getAreaByCity:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"getAreaByCity",param,
    	    		callback,scope,uppa);

    	},
    	
    	getHospitalByCityAndLevel:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"getHospitalByCityAndLevel",param,
    	    		callback,scope,uppa);

    	},
    	
    	getHospitalByArea:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"getHospitalByArea",param,
    	    		callback,scope,uppa);

    	},
    	
    	queryDepartmentByHospital:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"queryDepartmentByHospital",param,
    	    		callback,scope,uppa);

    	},
    	queryDoctorByDepartment:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"queryDoctorByDepartment",param,
    	    		callback,scope,uppa);

    	},
    	getNewsByDepartment:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"getNewsByDepartment",param,
    	    		callback,scope,uppa);

    	},
    	queryArrange:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"queryArrange",param,
    	    		callback,scope,uppa);

    	},
    	saveArrange:function(callback,scope,param,uppa){
    		comedical.util.DataApi.postData(
    	    		comedical.util.Config.getService()+"saveArrange",param,
    	    		callback,scope);

    	},
    	getKnowledgeItem:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"getKnowledgeItem",param,
    	    		callback,scope,uppa);

    	},
    	saveKnowledgeItem:function(callback,scope,param,uppa){
    		comedical.util.DataApi.postData(
    	    		comedical.util.Config.getService()+"saveKnowledgeItem",param,
    	    		callback,scope,uppa);

    	},
    	queryPrivateAdvisoryQuestion:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"queryPrivateAdvisoryQuestion",param,
    	    		callback,scope,uppa);

    	},
    	queryPrivateQuestionAnswer:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"queryPrivateQuestionAnswer",param,
    	    		callback,scope,uppa);

    	},
    	addPrivateQuestionAnswer:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"addPrivateQuestionAnswer",param,
    	    		callback,scope,uppa);

    	},
    	queryMsg:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"queryMsg",param,
    	    		callback,scope,uppa);

    	},
    	sendMsg:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"sendMsg",param,
    	    		callback,scope,uppa);

    	},
    	queryReport:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"queryReport",param,
    	    		callback,scope,uppa);

    	},
    	queryReportAnswer:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"queryReportAnswer",param,
    	    		callback,scope,uppa);

    	},
    	addReportAnswer:function(callback,scope,param,uppa){
    		comedical.util.DataApi.queryData(
    	    		comedical.util.Config.getService()+"addReportAnswer",param,
    	    		callback,scope,uppa);

    	}
    	
    },
    queryData:function(url, params, callback, scope,uppa) {
       // Ext.data.JsonP.request({
         Ext.Ajax.request({
         	url: url,
            params: params,
            method: 'GET',
            dataType:'json',
            scope: scope,
            success: function(response) {
                if (callback) {
                    callback(response.responseText, this,uppa);
                }
            },
            failure: function(a, b, c) {
                Ext.Msg.alert('提示', '远程连接失败:' + url);
            }
        });
    },
     postData:function(url, params, callback, scope,uppa) {
       // Ext.data.JsonP.request({
         Ext.Ajax.request({
         	url: url,
         	timeout: 600000,
         	headers: {
             'Content-Type': 'application/json;charset=utf-8'
            },
            params: Ext.JSON.encode(params),
            method: 'POST',
            dataType:'json',
            scope: scope,
            success: function(response) {
                if (callback) {
                    callback(response.responseText, this,uppa);
                }
            },
            failure: function(a, b, c) {
                Ext.Msg.alert('提示', '远程连接失败:' + url);
            }
        });
    }
    
});
