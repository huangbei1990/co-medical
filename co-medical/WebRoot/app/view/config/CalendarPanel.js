Ext.define('comedical.view.config.CalendarPanel',
		{
			extend : 'Ext.panel.Panel',
			alias : 'widget.cocalendarpanel',
			id : 'tab-main',
			fullscreen : true,
			region : 'center',
			defaults : {
				bodyPadding : 10,
				autoScroll : true
			},
			startDate : Ext.Date.add(new Date(), Ext.Date.DAY, 1 - new Date()
					.getDay()),

			// private
			initComponent : function() {

				this.tbar = {
					cls : 'ext-cal-toolbar',
					border : true,
					items : [ '->', {
						id : this.id + '-tb-prev',
						handler : this.onPrevClick,
						scope : this,
						iconCls : 'x-tbar-page-prev'
					} ]
				};

				this.viewCount = 0;

				this.tbar.items.push({
					xtype : 'tbtext',
					id : this.id + '-tb-week',
					text : this.refreshTitle(0),
					scope : this,
					toggleGroup : 'tb-views'
				});
				this.viewCount++;

				this.tbar.items.push({
					id : this.id + '-tb-next',
					handler : this.onNextClick,
					scope : this,
					iconCls : 'x-tbar-page-next'
				});
				this.tbar.items.push('->');

				this.callParent();

				this.layout = 'card';
				// do not allow override
				
					var hosid = Ext.util.Cookies.get("hosid");

					 // alert(id);
					if(!hosid || hosid=='undefined'){
						return;
					}else if(hosid=='super'){
						grid_taber=Ext.create('comedical.view.config.tbar.HospitalTbar',{id:'hospital_tbar'}).get_tbar(GridType.Doctor);
					}else{
						grid_taber=Ext.create('comedical.view.config.tbar.HospitalTbar',{id:'hospital_tbar'}).get_hospitalTbar(hosid);
					}
				
				
				grid_taber.pop();
				grid_taber.pop();
				grid_taber.pop();
				grid_taber.push({
					xtype : 'button',
					text : '保存',
					icon : 'js/resources/images/icons/fam/accept.png',
					handler : function(view, rowIndex, colIndex, actionItem, event,
							record, row) {
						this.up('grid').saveRecord();
					}
				});
				
				var grid = Ext
						.create('comedical.view.config.grid.CalendarGrid',{tbar:grid_taber});

				grid.id = this.id + '-week';
				grid.initialData(this.startDate, Ext.Date.add(this.startDate,
						Ext.Date.DAY, 7 - this.startDate.getDay()));

				this.add(grid);

			},
			refreshTitle : function(days) {
				this.startDate = Ext.Date.add(this.startDate, Ext.Date.DAY,
						days);
				endDate = Ext.Date.add(this.startDate, Ext.Date.DAY,
						7 - this.startDate.getDay());
				if (this.items && this.items.items[0]) {
					this.items.items[0].initialData(this.startDate, endDate);
				}
				return Ext.util.Format.date(this.startDate, "Y-m-d  l") + " - "
						+ Ext.util.Format.date(endDate, "Y-m-d  l");
			},
			// private
			onPrevClick : function() {
				Ext.getCmp('AP000013-tb-week').setText(this.refreshTitle(-7));

			},

			// private
			onNextClick : function() {
				Ext.getCmp('AP000013-tb-week').setText(this.refreshTitle(7));
			}
		});