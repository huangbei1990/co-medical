var doctor_store = Ext.create('Ext.data.Store', {
	fields : doctor_model.config.fields
});

Ext.define('comedical.view.config.grid.DoctorGrid', {
	extend : 'Ext.grid.Panel',
	height : 200,
	width : 400,
	scrollable : true,
	store : doctor_store,
	selectid:null,
	columns : doctor_model.config.columns,
	plugins : [ Ext.create('Ext.grid.plugin.CellEditing', {
		clicksToEdit : 1,
		listeners : {
			edit : function(editor, e, eOpts) {

				if (e.originalValue !== e.value) {
					e.record.data[e.field] = e.value;

					var wait_msg = Ext.Msg.wait('正在保存数据.....', '请稍侯');
					comedical.util.DataApi.Core.addNewRecord(function(res, scope) {
						res = Ext.decode(res);
						wait_msg.hide();
						if(res.code=='200'){
						Ext.Msg.alert('提示', '更新成功', function() {
							e.record.commit();
							e.grid.getView().refresh();
						});
						}else{
							Ext.Msg.alert('提示', '更新失败');
						}
					}, e.grid.store, {'table':'Doctor','json':Ext.JSON.encode(e.record.data)});

				}

				return true;

			}
		}

	}) ],
	tbar :[],
	listeners : {
		itemdblclick : function(dataview, record, item, index, e, eOpts) {
			// this.up('panel').setValue(record.data.id);
			// this.up('panel').close();

		}
	},initComponent : function() {
		var hosid = Ext.util.Cookies.get("hosid");

		 // alert(id);
		if(!hosid || hosid=='undefined'){
			return;
		}else if(hosid=='super'){
			this.tbar=Ext.create('comedical.view.config.tbar.HospitalTbar',{id:'hospital_tbar'}).get_tbar(GridType.Doctor);
		}else{
			this.tbar=Ext.create('comedical.view.config.tbar.HospitalTbar',{id:'hospital_tbar'}).get_hospitalTbar(hosid);
		}
		this.callParent();
	},
	initialData : function() {

	},
	loadRecords : function(id) {
		selectid=id;
		this.store.removeAll();
		comedical.util.DataApi.Core.queryDoctorByDepartment(function(res,
				scope) {
			res = Ext.decode(res);
			if (res.code == 200) {
				scope.loadData(res.result);
			}

		}, this.store, {
			departmentid : id
		});
	},
	createRecord:function(){
		comedical.util.EventHandle.events.ShowForm(false, this,
				this.getSelectionModel( ).getSelection(), '新建专家', 'comedical.view.config.form.FormDoctor',
				addHospitalCallback,800,700)
	},
	updateRecord:function(){
		comedical.util.EventHandle.events.ShowForm(true, this,
				this.getSelectionModel( ).getSelection(), '编辑专家', 'comedical.view.config.form.FormDoctor',
				addHospitalCallback,800,700)
	},
	removeRecord:function(){
		record = this.getSelectionModel( ).getSelection();
		record=Ext.JSON.encode(record[0].raw);
		comedical.util.DataApi.Core.deleteRecord(function(res,scope,id){
			res = Ext.decode(res);
			if (res.code == 200) {
				scope.store.removeAll();
				scope.loadRecords(id);
			}else{
				Ext.Msg.alert('提示','删除失败')
			}
		}, this, {'table':'Doctor','json':record},selectid);
	}
});
addHospitalCallback = function(form, grid, isUpdate) {
	this.callback = function(res, scope, id) {
		Ext.Msg.alert('提示', '执行操作成功！');
		scope.up('panel').close();
		var store;
		if (grid.dockedItems) {
			// grid.dockedItems.items[1].items.items[0].setValue(subjectId);
			store = grid.items.items[0].getStore();
		} else {
			store = grid.getStore();
			// grid.up().dockedItems.items[1].items.items[0].setValue(subjectId);
		}
		store.removeAll();
		grid.loadRecords(id);
		
	}
	if (form.isValid()) {

			comedical.util.DataApi.Core.addNewRecord(this.callback, form, {'table':'Doctor','json':Ext.JSON.encode(form.getValues())},form.getValues().departmentid);
	}
}
