treeStore = new Ext.data.TreeStore({
	autoLoad : true,
	fields : [ {
		name : 'id',
		type : 'string'
	}, {
		name : 'name',
		type : 'string'
	}, {
		name : 'leaf',
		type : 'string'
	}, {
		name : 'perid',
		type : 'string'
	}, {
		name : 'panel',
		type : 'string'
	} ],
	proxy : {
		type : 'ajax',
		url : comedical.util.Config.getService() + 'getAllPermission',
		reader : 'json'
	},
	folderSort : true

});

Ext
		.define(
				'comedical.view.config.grid.TreeGrid',
				{
					extend : 'Ext.tree.Panel',

					requires : [ 'Ext.data.*', 'Ext.grid.*', 'Ext.tree.*',
							'Ext.ux.CheckColumn' ],
					xtype : 'tree-grid',
					nodeParam : 'perid',
					title : '权限管理',
					height : '100%',
					id : 'tree-grid',
					tbar : [ {
						xtype : 'button',
						text : '刷新',
						icon : 'js/resources/images/icons/fam/information.png',
						handler : function(view, rowIndex, colIndex, actionItem, event, record,
								row) {
							grid=view.up('panel');
							grid.getStore().reload();
						
						}
					} ],
					useArrows : true,
					rootVisible : false,
					multiSelect : true,
					singleExpand : true,
					loadMask : true,
					initComponent : function() {
						this.width = '100%';
						 if(treeStore.getRootNode()){
							 treeStore.getRootNode().removeAll();
							 treeStore.reload();
						 }
						Ext
								.apply(
										this,
										{
											store : treeStore,
											columns : [
													{
														xtype : 'treecolumn', // this
																				// is
																				// so
																				// we
																				// know
														// which
														// column will
														// show the tree
														text : '标题',
														flex : 2,
														sortable : true,
														dataIndex : 'name'
													},
													{
														text : '指定panel',
														flex : 1,
														dataIndex : 'panel',
														sortable : false
													},
													{
														text : '添加权限',
														width : 80,
														menuDisabled : true,
														xtype : 'actioncolumn',
														tooltip : '添加权限',
														align : 'center',
														icon : 'js/resources/images/icons/fam/add.png',
														handler : function(
																grid, rowIndex,
																colIndex,
																actionItem,
																event, record,
																row) {
															ShowCategoryForm(
																	false,
																	grid,
																	record,
																	{
																		data : {
																			perid : record.data.id
																		}
																	})
														}
													},
													{
														text : '编辑权限',
														width : 55,
														menuDisabled : true,
														xtype : 'actioncolumn',
														tooltip : '编辑权限',
														align : 'center',
														icon : 'js/resources/images/edit_task.png',
														handler : function(
																grid, rowIndex,
																colIndex,
																actionItem,
																event, record,
																row) {
															ShowCategoryForm(
																	true, grid,
																	record)
														},
														// Only leaf level tasks
														// may be edited
														isDisabled : function(
																view, rowIdx,
																colIdx, item,
																record) {
															return !record.data.leaf;
														}
													},
													{
														text : '删除权限',
														width : 70,
														menuDisabled : true,
														xtype : 'actioncolumn',
														tooltip : '删除权限',
														align : 'center',
														icon : 'js/resources/images/icons/fam/delete.png'
													} ]
										});
						this.callParent();
					},
					listeners : {
						'afterrender' : function(view, e) {

							loading = new Ext.LoadMask(view, {
								msg : '正在加载...',
								removeMask : true
							// 完成后移除
							});
							loading.show();
						},// 加载前
						'load' : function() {
							loading.hide();
						} // 加载完成后
					}
				});

ShowCategoryForm = function(isUpdate, grid, record) {
	form = Ext.create('comedical.view.config.form.FormNewPermission');
	if (isUpdate) {
		form.getForm().loadRecord(record)
	} else {
		parentField = form.getForm().findField('perid');
		parentField.setValue(record.data.id);
		leafField = form.getForm().findField('leaf');
		leafField.setValue(1);
	}
	Ext.create(
			'Ext.window.Window',
			{
				title : '编辑分类',
				height : 500,
				width : 600,
				layout : 'fit',
				items : [ form ],
				dockedItems : [ {
					xtype : 'toolbar',
					dock : 'bottom',
					ui : 'footer',
					items : [
							{
								xtype : 'component',
								flex : 1
							},
							{
								xtype : 'button',
								text : '提交',
								handler : function() {

									if (form.isValid()) {
										
											comedical.util.DataApi.Core.addNewRecord(callback, form, {'table':'AdminPermission','json':Ext.JSON.encode(form.getValues())},grid);

										
									}
								}

							} ]
				} ]
			}).show();
}
callback = function(res, scope, grid) {
	Ext.Msg.alert('提示', '执行操作成功！');
	scope.up('panel').close();
	grid.getStore().treeStore.reload();
}