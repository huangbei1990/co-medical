var adminrole_store = Ext.create('Ext.data.Store', {
	fields : role_model.config.fields
});

Ext.define('comedical.view.config.grid.AdminRoleGrid', {
	extend : 'Ext.grid.Panel',
	height : 200,
	width : 400,
	scrollable : true,
	store : adminrole_store,
	columns : role_model.config.columns,
	tbar : [ {
		xtype : 'button',
		text : '新建角色',
		icon : 'js/resources/images/icons/fam/add.png',
		handler : function(view, rowIndex, colIndex, actionItem, event, record,
				row) {
			comedical.util.EventHandle.events.ShowForm(false, view.up('panel'),
					record, '新建角色', 'comedical.view.config.form.FormNewRole',
					addRoleCallback)
		}
	} ],
	listeners : {
		itemdblclick : function(dataview, record, item, index, e, eOpts) {
			// this.up('panel').setValue(record.data.id);
			// this.up('panel').close();

		}
	},
	initialData : function() {
		comedical.util.DataApi.Core.getAllRole(function(res, scope) {
			res = Ext.decode(res);
			if (res.code == 200) {
				scope.add(res.result);
			}

		}, adminrole_store);
	}
});
addRoleCallback = function(form, grid, isUpdate) {
	this.callback = function(res, scope, subjectId) {
		Ext.Msg.alert('提示', '执行操作成功！');
		scope.up('panel').close();
		var store;
			store = grid.getStore();
			store.removeAll();
		comedical.util.DataApi.Core.getAllRole(function(res, scope) {
			res = Ext.decode(res);
			if (res.code == 200) {
				scope.add(res.result);
			}

		}, store)
	}
	if (form.isValid()) {
		if (isUpdate) {
			comedical.util.DataApi.Core.updateLaws(this.callback, form, form
					.getValues(), form.getValues().subjectId)
		} else {

			comedical.util.DataApi.Core.addNewRecord(this.callback, form, {'table':'AdminRole','json':Ext.JSON.encode(form.getValues())});
		}
	}
}
