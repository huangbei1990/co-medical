var pagesize=10;

Ext.define('comedical.view.config.grid.MsgGrid', {
	extend : 'Ext.grid.Panel',
	height : 200,
	width : 400,
	scrollable : true,
	columns : msg_model.config.columns,
	initComponent:function(){
		this.store=new Ext.data.JsonStore({
			pageSize : pagesize,
			proxy : {
				type : 'ajax',
				url : comedical.util.Config.getService()
						+ 'queryMsg',
				reader : {
					type : 'json',
					root : 'data',
					idProperty : 'id'
				}
			},
			totalProperty : 'total',
			fields : msg_model.config.fields

		});
		
		this.bbar=new Ext.PagingToolbar({
			pageSize : pagesize,
			emptyMsg : "没有数据",
			displayInfo : true,
			displayMsg : "显示从{0}条数据到{1}条数据，共{2}条数据",
			store : this.store

		});
		this.store.load({
			params : {
				start : 0,
				limit : pagesize
			}
		});
		this.tbar=[{
			xtype : 'button',
			text : '新建',
			icon : 'js/resources/images/icons/fam/add.png',
			handler : function(view, rowIndex, colIndex, actionItem, event,
					record, row) {
				this.up('grid').createRecord();
			}
		}, {
			xtype : 'button',
			text : '编辑',
			icon : 'js/resources/images/edit_task.png',
			handler : function(view, rowIndex, colIndex, actionItem, event,
					record, row) {
				this.up('grid').updateRecord();
			}
		}, {
			xtype : 'button',
			text : '删除',
			icon : 'js/resources/images/icons/fam/delete.png',
			handler : function(view, rowIndex, colIndex, actionItem, event,
					record, row) {
				this.up('grid').removeRecord();
			}
		}, {
			xtype : 'button',
			text : '发送',
			icon : 'js/resources/images/edit_task.png',
			handler : function(view, rowIndex, colIndex, actionItem, event,
					record, row) {
				this.up('grid').sendRecord();
			}
		}];
		this.callParent();
	},
	listeners : {
		itemdblclick : function(dataview, record, item, index, e, eOpts) {
			// this.up('panel').setValue(record.data.id);
			// this.up('panel').close();

		}
	},
	initialData : function() {

	},
	loadRecords : function(id) {
		
	},
	createRecord:function(){
		comedical.util.EventHandle.events.ShowForm(false, this,
				this.getSelectionModel( ).getSelection(), '新建消息', 'comedical.view.config.form.FormMsg',
				addHospitalCallback,800,700);
	},
	updateRecord:function(){
		comedical.util.EventHandle.events.ShowForm(true, this,
				this.getSelectionModel( ).getSelection(), '编辑消息', 'comedical.view.config.form.FormMsg',
				addHospitalCallback,800,700);
	},
	sendRecord:function(){
		var records=this.getSelectionModel( ).getSelection();
		if(records&&records.length>0){
		comedical.util.DataApi.Core.sendMsg(function(res,scope,id){
			res = Ext.decode(res);
			if (res.code == 200) {
				scope.store.reload();
				Ext.Msg.alert('提示','发送成功');
			}else{
				Ext.Msg.alert('提示','发送失败');
			}
		}, this, {'id':records[0].data.id});
	  }
	},
	removeRecord:function(){
		record = this.getSelectionModel( ).getSelection();
		record=Ext.JSON.encode(record[0].raw);
		comedical.util.DataApi.Core.deleteRecord(function(res,scope,id){
			res = Ext.decode(res);
			if (res.code == 200) {
				scope.store.removeAll();
				scope.loadRecords(id);
			}else{
				Ext.Msg.alert('提示','删除失败')
			}
		}, this, {'table':'Msg','json':record},selectid);
	}
});
addHospitalCallback = function(form, grid, isUpdate) {
	this.callback = function(res, scope, id) {
		Ext.Msg.alert('提示', '执行操作成功！');
		scope.up('panel').close();
		var store;
		if (grid.dockedItems) {
			// grid.dockedItems.items[1].items.items[0].setValue(subjectId);
			store = grid.items.items[0].getStore();
		} else {
			store = grid.getStore();
			// grid.up().dockedItems.items[1].items.items[0].setValue(subjectId);
		}
		store.reload();
		
	}
	if (form.isValid()) {
	
	comedical.util.DataApi.Core.addNewRecord(this.callback, form, {'table':'Msg','json':Ext.JSON.encode(form.getValues())},form.getValues().departmentid);
		
	}
}
