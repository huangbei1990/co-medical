KnowledgeStore = new Ext.data.TreeStore({
	autoLoad : true,
	fields : [ {
		name : 'id',
		type : 'string'
	}, {
		name : 'name',
		type : 'string'
	}, {
		name : 'pic',
		type : 'string'
	}, {
		name : 'parentid',
		type : 'string'
	}, {
		name : 'leaf',
		type : 'int'
	} ],
	proxy : {
		type : 'ajax',
		url : comedical.util.Config.getService() + 'getKnowledge',
		reader : 'json'
	},
	folderSort : true

});
var loadMarsk = new Ext.LoadMask(Ext.getBody(), {
    msg : '数据处理中!',
    disabled : false,
    store : KnowledgeStore
});
KnowledgeStore.addListener('beforeload',function(){loadMarsk.show();});

Ext.define('comedical.view.config.grid.KnowledgeGrid',
				{
					extend : 'Ext.tree.Panel',

					requires : [ 'Ext.data.*', 'Ext.grid.*', 'Ext.tree.*',
							'Ext.ux.CheckColumn' ],
					xtype : 'tree-grid',
					nodeParam : 'parentid',
					title : '知识库管理',
					height : '100%',
					id : 'Knowledge-grid',
					tbar : [
							{
								xtype : 'button',
								text : '新建节点',
								icon : 'js/resources/images/icons/fam/add.png',
								handler : function(view, rowIndex, colIndex,
										actionItem, event, record, row) {
									grid = view.up('panel');
									record = grid.getSelectionModel()
											.getSelection();
									if (record[0].data.leaf == 1) {
										Ext.Msg.alert("提示", "知识内容节点无法添加子节点");
									} else {
										ShowCategoryForm(false, grid, record[0])
									}

								}
							},
							{
								xtype : 'button',
								text : '编辑节点',
								icon : 'js/resources/images/edit_task.png',
								handler : function(view, rowIndex, colIndex,
										actionItem, event, record, row) {
									grid = view.up('panel');
									record = grid.getSelectionModel()
											.getSelection();
									if (record[0].data.leaf == 1) {
										Ext.Msg.alert("提示", "知识内容节点无法添加子节点");
									} else {
										ShowCategoryForm(true, grid, record[0])
									}

								}
							},
							{
								xtype : 'button',
								text : '添加知识内容',
								icon : 'js/resources/images/icons/fam/add.png',
								handler : function(view, rowIndex, colIndex,
										actionItem, event, record, row) {
									grid = view.up('panel');
									record = grid.getSelectionModel()
											.getSelection();
									if (record[0].childNodes.length>0) {
										Ext.Msg.alert("提示", "请在叶节点添加知识内容");
									} else {
										ShowItemForm(false,record[0],grid);
									}

								}
							},{
								xtype : 'button',
								text : '编辑知识内容',
								icon : 'js/resources/images/edit_task.png',
								handler : function(view, rowIndex, colIndex,
										actionItem, event, record, row) {
									grid = view.up('panel');
									record = grid.getSelectionModel()
											.getSelection();
									if (record[0].data.leaf == 1) {
									
										comedical.util.DataApi.Core.getKnowledgeItem(function(res,
												scope,uup) {
											res = Ext.decode(res);
											if (res.code == 200) {
												currentstore = Ext.create('Ext.data.Store', {
													fields : [ {
														name : 'id',
														type : 'string'
													}, {
														name : 'name',
														type : 'string'
													}, {
														name : 'info',
														type : 'string'
													}, {
														name : 'parentid',
														type : 'string'
													}],
													data : res.result
												});
												ShowItemForm(true,currentstore.data.getAt(0),uup);
											}

										}, record[0], {
											classid : record[0].data.id
										},grid);
									}

								}
							} ],
					useArrows : true,
					rootVisible : false,
					multiSelect : true,
					singleExpand : true,
					
					initComponent : function() {
						this.width = '100%';
						if (KnowledgeStore.getRootNode()) {
							KnowledgeStore.getRootNode().removeAll();
							KnowledgeStore.reload();
						}
						Ext.apply(this,{
							store : KnowledgeStore,
							columns : [{
								xtype : 'treecolumn', 
								text : '标题',
								flex : 2,
								sortable : true,
								dataIndex : 'name'
									},
									{
										text : '图片',
										flex : 1,
										dataIndex : 'pic',
										renderer : function(value) {
															if (value&& value != ""&& value != "null") {
																return "<img src='Images/knowledge/"
																		+ value
																		+ "'  height='20px' width='20px'/>";
															}
															return "<span/>";
														},
														sortable : false
													},

													{
														text : '叶节点',
														width : 55,
														menuDisabled : true,
														xtype : 'actioncolumn',
														tooltip : '编辑节点',
														align : 'center',
														icon : 'js/resources/images/icons/fam/accept.png',
														handler : function(
																grid, rowIndex,
																colIndex,
																actionItem,
																event, record,
																row) {

														},
														// Only leaf level tasks
														// may be edited
														isDisabled : function(
																view, rowIdx,
																colIdx, item,
																record) {
															return !record.data.leaf;
														}
													},
													{
														text : '删除节点',
														width : 70,
														menuDisabled : true,
														xtype : 'actioncolumn',
														tooltip : '删除节点',
														align : 'center',
														icon : 'js/resources/images/icons/fam/delete.png',
														handler : function(grid, rowIndex,colIndex,actionItem,event, record,row) {
															record=Ext.JSON.encode(record.raw);
															loadMarsk.show();
															comedical.util.DataApi.Core.deleteRecord(function(res,scope,id){
																res = Ext.decode(res);
																if (res.code == 200) {
																	scope.getStore().treeStore.reload();
																}else{
																	Ext.Msg.alert('提示','删除失败')
																}
															}, grid, {'table':'Knowledgeclass','json':record});
														}
														
													} ]
										});
						this.callParent();
					}
					
				});

ShowCategoryForm = function(isUpdate, grid, record) {
	form = Ext.create('comedical.view.config.form.FormKnowledge');
	if (isUpdate) {
		form.getForm().loadRecord(record);
	} else {
		parentField = form.getForm().findField('parentid');
		parentField.setValue(record.data.id);
		leafField = form.getForm().findField('leaf');
		leafField.setValue(0);
	}
	Ext.create('Ext.window.Window',
					{
						title : '编辑分类',
						height : 500,
						width : 600,
						layout : 'fit',
						items : [ form ],
						dockedItems : [ {
							xtype : 'toolbar',
							dock : 'bottom',
							ui : 'footer',
							items : [{
										xtype : 'component',
										flex : 1
									},
									{
										xtype : 'button',
										text : '提交',
										handler : function() {

											if (form.isValid()) {

												comedical.util.DataApi.Core
														.addNewRecord(
																callback,
																form,
																{
																	'table' : 'Knowledgeclass',
																	'json' : Ext.JSON.encode(form.getValues())
																}, grid);

											}
										}

									} ]
						} ]
					}).show();
}
callback = function(res, scope, grid) {
	res = Ext.decode(res);
	if (res.code == 200) {
		Ext.Msg.alert('提示', '执行操作成功！');
		scope.up('panel').close();
		grid.getStore().tree.treeStore.reload();
	}
	
}
ShowItemForm = function(isUpdate, record,grid) {
	form = Ext.create('comedical.view.config.form.FormKnowledgeItem');
	if (isUpdate) {
		form.getForm().loadRecord(record);
	} else {
		idField = form.getForm().findField('id');
		idField.setValue(record.data.id);
		parentField = form.getForm().findField('parentid');
		parentField.setValue(record.data.parentid);
		nameField = form.getForm().findField('name');
		nameField.setValue(record.data.name);
	}
	Ext.create('Ext.window.Window',{
						title : '编辑知识内容',
						height : 500,
						width : 600,
						layout : 'fit',
						items : [ form ],
						dockedItems : [ {
							xtype : 'toolbar',
							dock : 'bottom',
							ui : 'footer',
							items : [{
										xtype : 'component',
										flex : 1
									},
									{
										xtype : 'button',
										text : '提交',
										handler : function() {

											if (form.isValid()) {

												comedical.util.DataApi.Core
														.saveKnowledgeItem(
																callback,
																form,
																form.getValues(), 
																grid);

											}
										}

									} ]
						} ]
					}).show();
}
