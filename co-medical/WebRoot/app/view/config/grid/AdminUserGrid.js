var adminuser_store = Ext.create('Ext.data.Store', {
	fields : user_model.config.fields
});

Ext.define('comedical.view.config.grid.AdminUserGrid', {
	extend : 'Ext.grid.Panel',
	height : 200,
	width : 400,
	scrollable : true,
	store : adminuser_store,
	columns : user_model.config.columns,
	tbar : [ {
		xtype : 'button',
		text : '新建用户',
		icon : 'js/resources/images/icons/fam/add.png',
		handler : function(view, rowIndex, colIndex, actionItem, event, record,
				row) {
			comedical.util.EventHandle.events.ShowForm(false, view.up('panel'),
					record, '新建用户', 'comedical.view.config.form.NewUserForm',
					addUserCallback)
		}
	} ],
	listeners : {
		itemdblclick : function(dataview, record, item, index, e, eOpts) {
			// this.up('panel').setValue(record.data.id);
			// this.up('panel').close();

		}
	},
	initialData : function() {
		comedical.util.DataApi.Core.getAllAdminUser(function(res, scope) {
			res = Ext.decode(res);
			if (res.code == 200) {
				scope.add(res.result);
			}

		}, adminuser_store);
	}
});
addUserCallback = function(form, grid, isUpdate) {
	this.callback = function(res, scope, subjectId) {
		Ext.Msg.alert('提示', '执行操作成功！');
		scope.up('panel').close();
		var store;
		if (grid.dockedItems) {
			// grid.dockedItems.items[1].items.items[0].setValue(subjectId);
			store = grid.items.items[0].getStore();
		} else {
			store = grid.getStore();
			// grid.up().dockedItems.items[1].items.items[0].setValue(subjectId);
		}
		store.removeAll();
		comedical.util.DataApi.Core.getAllAdminUser(function(res, scope) {
			res = Ext.decode(res);
			if (res.code == 200) {
				scope.add(res.result);
			}

		}, store);
	}
	if (form.isValid()) {	
			comedical.util.DataApi.Core.addNewRecord(this.callback, form, {'table':'AdminUser','json':Ext.JSON.encode(form.getValues())});
	}
}
