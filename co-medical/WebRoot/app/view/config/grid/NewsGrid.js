var news_store = Ext.create('Ext.data.Store', {
	fields : news_model.config.fields
});

Ext.define('comedical.view.config.grid.NewsGrid', {
	extend : 'Ext.grid.Panel',
	height : 200,
	width : 400,
	scrollable : true,
	store : news_store,
	selectid:null,
	columns : news_model.config.columns,
	tbar:[],
	listeners : {
		itemdblclick : function(dataview, record, item, index, e, eOpts) {
			// this.up('panel').setValue(record.data.id);
			// this.up('panel').close();

		}
	},
	initComponent : function() {
		var hosid = Ext.util.Cookies.get("hosid");

		 // alert(id);
		if(!hosid || hosid=='undefined'){
			return;
		}else if(hosid=='super'){
			this.tbar=Ext.create('comedical.view.config.tbar.HospitalTbar',{id:'hospital_tbar'}).get_tbar(GridType.News);
		}else{
			this.tbar=Ext.create('comedical.view.config.tbar.HospitalTbar',{id:'hospital_tbar'}).get_hospitalTbar(hosid);
		}
		this.callParent();
	},
	initialData : function() {

	},
	loadRecords : function(id) {
		selectid=id;
		this.store.removeAll();
		comedical.util.DataApi.Core.getNewsByDepartment(function(res,
				scope) {
			res = Ext.decode(res);
			if (res.code == 200) {
				scope.loadData(res.result);
			}

		}, this.store, {
			departmentid : id
		});
	},
	createRecord:function(){
		comedical.util.EventHandle.events.ShowForm(false, this,
				this.getSelectionModel( ).getSelection(), '新建新闻', 'comedical.view.config.form.FormNews',
				addHospitalCallback,1200,750)
	},
	updateRecord:function(){
		comedical.util.EventHandle.events.ShowForm(true, this,
				this.getSelectionModel( ).getSelection(), '编辑新闻', 'comedical.view.config.form.FormNews',
				addHospitalCallback,1200,750)
	},
	removeRecord:function(){
		record = this.getSelectionModel( ).getSelection();
		record=Ext.JSON.encode(record[0].raw);
		comedical.util.DataApi.Core.deleteRecord(function(res,scope,id){
			res = Ext.decode(res);
			if (res.code == 200) {
				scope.store.removeAll();
				scope.loadRecords(id);
			}else{
				Ext.Msg.alert('提示','删除失败')
			}
		}, this, {'table':'News','json':record},selectid);
	}
});
addHospitalCallback = function(form, grid, isUpdate) {
	this.callback = function(res, scope, id) {
		Ext.Msg.alert('提示', '执行操作成功！');
		scope.up('panel').close();
		var store;
		if (grid.dockedItems) {
			// grid.dockedItems.items[1].items.items[0].setValue(subjectId);
			store = grid.items.items[0].getStore();
		} else {
			store = grid.getStore();
			// grid.up().dockedItems.items[1].items.items[0].setValue(subjectId);
		}
		store.removeAll();
		grid.loadRecords(id);
		
	}
	if (form.isValid()) {
	
	comedical.util.DataApi.Core.addNewRecord(this.callback, form, {'table':'News','json':Ext.JSON.encode(form.getValues())},form.getValues().departmentid);
		
	}
}
