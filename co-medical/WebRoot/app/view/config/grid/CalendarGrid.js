var common_store = Ext.create('Ext.data.Store', {
	fields : [ {
		name : 'id',
		type : 'string'
	} ]
});

Ext.define('comedical.view.config.grid.CalendarGrid', {
	extend : 'Ext.grid.Panel',
	height : 200,
	width : 400,
	reportid : 0,
	scrollable : true,
	store : common_store,
	selectid : null,
	startdate : null,
	waitMask:new Ext.LoadMask(Ext.getBody(), {
		msg : "系统正在处理数据，请稍候..."
	}),
	enddate : null,
	plugins : [ Ext.create('Ext.grid.plugin.CellEditing', {
		clicksToEdit : 1,
		listeners : {
			edit : function(editor, e, eOpts) {
				return true;
			}
		}

	}) ],
	columns : [ {
		text : 'ID',
		dataIndex : 'id'
	} ],
	fields : [],
	calculate : null,

	listeners : {

	},
	initialData : function(start, end) {
		this.startdate = start;
		this.enddate = end;
		this.loadData();

	},
	loadRecords : function(id) {
		this.selectid = id;
		this.loadData();
	},
	loadData : function() {
		if (this.enddate && this.startdate && this.selectid) {
			this.waitMask.show();
			this.createFields(this.startdate, this.enddate);
			comedical.util.DataApi.Core.queryArrange(
					function(res, scope, uppa) {
						res = Ext.decode(res);
						scope.waitMask.hide();
						if (res.code == 200) {

							currentstore = Ext.create('Ext.data.Store', {
								fields : scope.fields,
								data : res.result
							});

							scope.reconfigure(currentstore, scope.columns);
							scope.store = currentstore;
						

						} else {
							Ext.Msg.alert("提示", "暂无数据！");
						}

					}, this, {
						'departmentid' : this.selectid,
						'startday' : Ext.util.Format.date(this.startdate,
								"Y-m-d"),
						'endday' : Ext.util.Format.date(this.enddate, "Y-m-d"),
					});
		}
	},
	createFields : function(start, end) {
		fields = [ {
			name : 'doctor_id',
			type : 'string'
		}, {
			name : 'doctor_name',
			type : 'string'
		} ];
		columns = [ {
			text : 'ID',
			dataIndex : 'doctor_id'
		}, {
			text : '专家',
			dataIndex : 'doctor_name',
			width : 100
		} ];

		while (Date.parse(start) <= Date.parse(end)) {
		
			var colindex = Ext.util.Format.date(start, "Y-m-d");
			var title = Ext.util.Format.date(start, "Y-m-d  l");
			var itemstore=Ext.create('Ext.data.Store', {
				fields : [ {
					name : "name",
					type : "string"
				}, {
					name : 'value',
					type : "int"
				}],
				data : [ {
					name : '上午',
					value : 0
				}, {
					name : '下午',
					value : 1
				}, {
					name : '全天',
					value : 2
				},{
					name : '休息',
					value : 3
				} ]
			});
			fields.push({
				name : colindex,
				type : 'int'
			});
			columns.push({
				text : title,
				dataIndex : colindex,
				width : 150,
				editor : {
					xtype : 'combobox',
					store : itemstore,
					editable: true,
					displayField : 'name',
					valueField : 'value',
					queryMode : 'local',
					allowBlank : false,
					
				},
				renderer: function(value,metadata,record){  
                    var index = itemstore.find('value',value);  
                    if(index!=-1){  
                        return itemstore.getAt(index).data.name;  
                    }  
                    return record.get(colindex);  
                }  
			});

			start = Ext.Date.add(start, Ext.Date.DAY, 1);
		}

		this.columns = columns;
		this.fields = fields;

	},
	saveRecord:function(){
		this.waitMask.show();
		var tempArray = this.items.items[0].store.data.items;
		var recordArray = new Array();
		for (i=0;i<tempArray.length;i++) {

			recordArray.push(tempArray[i].data);
		}
		
		comedical.util.DataApi.Core.saveArrange(function(res, scope) {
			res = Ext.decode(res);
			scope.waitMask.hide();
			Ext.Msg.alert('提示', res.result[0]);
		}, this, recordArray);
	}
});

