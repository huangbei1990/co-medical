var area_store = Ext.create('Ext.data.Store', {
	fields : area_model.config.fields
});

Ext.define('comedical.view.config.grid.AreaGrid', {
	extend : 'Ext.grid.Panel',
	height : 200,
	width : 400,
	scrollable : true,
	store : area_store,
	selectid:null,
	columns : area_model.config.columns,
	tbar : Ext.create('comedical.view.config.tbar.HospitalTbar',{id:'hospital_tbar'}).get_tbar(GridType.Area),
	listeners : {
		itemdblclick : function(dataview, record, item, index, e, eOpts) {
			// this.up('panel').setValue(record.data.id);
			// this.up('panel').close();

		}
	},
	initialData : function() {
	},
	loadRecords : function(id) {
		selectid=id;
		this.store.removeAll();
		comedical.util.DataApi.Core.getAreaByCity(function(res,
				scope) {
			res = Ext.decode(res);
			if (res.code == 200) {
				scope.loadData(res.result);
			}

		}, this.store, {
			cityid : id
		});
	},
	createRecord:function(){
		comedical.util.EventHandle.events.ShowForm(false, this,
				this.getSelectionModel( ).getSelection(), '新建区域', 'comedical.view.config.form.FormArea',
				addAreaCallback)
	},
	updateRecord:function(){
		comedical.util.EventHandle.events.ShowForm(true, this,
				this.getSelectionModel( ).getSelection(), '编辑区域', 'comedical.view.config.form.FormArea',
				addAreaCallback)
	},
	removeRecord:function(){
		record = this.getSelectionModel( ).getSelection();
		record=Ext.JSON.encode(record[0].raw);
		comedical.util.DataApi.Core.deleteRecord(function(res,scope,id){
			res = Ext.decode(res);
			if (res.code == 200) {
				scope.store.removeAll();
				scope.loadRecords(id);
			}else{
				Ext.Msg.alert('提示','删除失败')
			}
		}, this, {'table':'Area','json':record},selectid);
	}
});
addAreaCallback = function(form, grid, isUpdate) {
	this.callback = function(res, scope, id) {
		Ext.Msg.alert('提示', '执行操作成功！');
		scope.up('panel').close();
		var store;
		if (grid.dockedItems) {
			// grid.dockedItems.items[1].items.items[0].setValue(subjectId);
			store = grid.items.items[0].getStore();
		} else {
			store = grid.getStore();
			// grid.up().dockedItems.items[1].items.items[0].setValue(subjectId);
		}
		store.removeAll();
		grid.loadRecords(id);
		
	}
	if (form.isValid()) {
	
			comedical.util.DataApi.Core.addNewRecord(this.callback, form, {'table':'Area','json':Ext.JSON.encode(form.getValues())},form.getValues().cityid);
	}
}
