

var pagesize=10;

var pregnant_store = new Ext.data.JsonStore({
	pageSize : pagesize,
	proxy : {
		type : 'ajax',
		url : comedical.util.Config.getService()
				+ 'queryPregKnowledge',
		reader : {
			type : 'json',
			root : 'data',
			idProperty : 'id'
		}
	},
	totalProperty : 'total',
	fields : pregnantKnows_model.config.fields,

});
var pagingToolbar = new Ext.PagingToolbar({
	pageSize : pagesize,
	emptyMsg : "没有数据",
	displayInfo : true,
	displayMsg : "显示从{0}条数据到{1}条数据，共{2}条数据",
	store : pregnant_store,

});

Ext.define('comedical.view.config.grid.PregnantKnowsGrid', {
	extend : 'Ext.grid.Panel',
	height : 200,
	width : 400,
	scrollable : true,
	store : pregnant_store,
	selectid:null,
	columns : pregnantKnows_model.config.columns,
	bbar : pagingToolbar,
	tbar :[],
	listeners : {
		itemdblclick : function(dataview, record, item, index, e, eOpts) {
			// this.up('panel').setValue(record.data.id);
			// this.up('panel').close();

		}
	},
	initComponent : function() {
		var hosid = Ext.util.Cookies.get("hosid");

		 // alert(id);
		if(!hosid || hosid=='undefined'){
			return;
		}else if(hosid=='super'){
			this.tbar=Ext.create('comedical.view.config.tbar.HospitalTbar',{id:'hospital_tbar'}).get_tbar(GridType.News);
		}else{
			this.tbar=Ext.create('comedical.view.config.tbar.HospitalTbar',{id:'hospital_tbar'}).get_hospitalTbar(hosid);
		}
		this.callParent();
	},
	initialData : function() {

	},
	loadRecords : function(id) {
		selectid=id;
		this.store.removeAll();
		pregnant_store.load({
			params : {
				start : 0,
				limit : pagesize,
				departmentid : id
			}
		});
	},
	createRecord:function(){
		comedical.util.EventHandle.events.ShowForm(false, this,
				this.getSelectionModel( ).getSelection(), '新建孕妈知多少', 'comedical.view.config.form.FormPragnantKnows',
				addHospitalCallback,1200,750)
	},
	updateRecord:function(){
		comedical.util.EventHandle.events.ShowForm(true, this,
				this.getSelectionModel( ).getSelection(), '编辑孕妈知多少', 'comedical.view.config.form.FormPragnantKnows',
				addHospitalCallback,1200,750)
	},
	removeRecord:function(){
		record = this.getSelectionModel( ).getSelection();
		record=Ext.JSON.encode(record[0].raw);
		comedical.util.DataApi.Core.deleteRecord(function(res,scope,id){
			res = Ext.decode(res);
			if (res.code == 200) {
				scope.store.removeAll();
				scope.loadRecords(id);
			}else{
				Ext.Msg.alert('提示','删除失败')
			}
		}, this, {'table':'PregnantKnowledge','json':record},selectid);
	}
});
addHospitalCallback = function(form, grid, isUpdate) {
	this.callback = function(res, scope, id) {
		Ext.Msg.alert('提示', '执行操作成功！');
		scope.up('panel').close();
		var store;
		if (grid.dockedItems) {
			// grid.dockedItems.items[1].items.items[0].setValue(subjectId);
			store = grid.items.items[0].getStore();
		} else {
			store = grid.getStore();
			// grid.up().dockedItems.items[1].items.items[0].setValue(subjectId);
		}
		store.removeAll();
		grid.loadRecords(id);
		
	}
	if (form.isValid()) {
	
	comedical.util.DataApi.Core.addNewRecord(this.callback, form, {'table':'PregnantKnowledge','json':Ext.JSON.encode(form.getValues())},form.getValues().departmentid);
		
	}
}
