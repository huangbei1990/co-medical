Ext.define('comedical.view.config.form.FormHospital', {
	extend : 'Ext.form.Panel',
	win : null,
	// title: 'Basic Form',
	// renderTo: Ext.getBody(),
	bodyPadding : 5,
	width : 350,
	autoScroll:true,
	items : [ 
	          {
			xtype : 'combobox',
			fieldLabel : '选择城市',
			displayField : 'name',
			valueField : 'id',
			store : Ext.create('Ext.data.Store', {
				fields : city_model.config.fields,
				proxy : {
					type : 'ajax',
					url : comedical.util.Config.getService() + "getAllCity",
					reader : 'json'
				}
			}),
			listeners : {
				select : function(combo, records, eOpts) {				
						this.up().items.items[1].loadData(records[0].data.id);
				}
			}
		}, {
			xtype : 'combobox',
			fieldLabel : '选择区域',
			displayField : 'name',
			queryMode: 'local',
			valueField : 'id',
			name:'areaid',
			store : Ext.create('Ext.data.Store', {
				fields : area_model.config.fields
			}),
			loadData : function(id) {
				comedical.util.DataApi.Core.getAreaByCity(function(res, scope) {
					res = Ext.decode(res);
					if (res.code == 200) {
						//scope.store.removeAll();
						scope.store.loadData(res.result);
					}

				}, this, {
					cityid : id
				});
			}
		},{
		xtype : 'textfield',
		fieldLabel : 'ID',
		name : 'id',
		hidden : true
	}, {
		xtype : 'textfield',
		fieldLabel : '名称',
		name : 'name'
	},{
		fieldLabel : '级别',
		name : 'level',
			xtype : 'combobox',
			displayField : 'name',
			valueField : 'value',
			store : Ext.create('Ext.data.Store', {
				fields : ['name','value'],
				data : [
				        {"name":"一级甲等", "value":"一级甲等"},
				        {"name":"一级乙等", "value":"一级乙等"},
				        {"name":"二级甲等", "value":"二级甲等"},
				        {"name":"二级乙等", "value":"二级乙等"}
				    ]
			})
			
	},{
		fieldLabel : '类型',
		name : 'characters',
			xtype : 'combobox',
			displayField : 'name',
			valueField : 'value',
			store : Ext.create('Ext.data.Store', {
				fields : ['name','value'],
				data : [
				        {"name":"公立/综合", "value":"公立/综合"}
				    ]
			})
			
	},{
		xtype : 'textfield',
		fieldLabel : '地址',
		name : 'address'
	},{
		xtype : 'textfield',
		fieldLabel : '网站',
		name : 'website'
	}
	,{
		xtype : 'textfield',
		fieldLabel : '电话',
		name : 'tel'
	}
	,{
		xtype : 'textfield',
		fieldLabel : '预约电话',
		name : 'booktel'
	},{
		xtype : 'textfield',
		fieldLabel : '上午上班时间',
		name : 'forenoontime'
	},{
		xtype : 'textfield',
		fieldLabel : '下午上班时间',
		name : 'afternoontime'
	}, {
		xtype : 'htmleditor',
		fieldLabel : '介绍',
		width : 700,
		height : 400,
		name : 'introduce'
	}, {
		xtype : 'htmleditor',
		fieldLabel : '导航信息',
		width : 700,
		height : 400,
		name : 'daohang'
	}]

});