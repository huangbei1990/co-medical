Ext.define('comedical.view.config.form.FormKnowledge', {
	extend : 'Ext.form.Panel',
	win : null,
	// title: 'Basic Form',
	// renderTo: Ext.getBody(),
	bodyPadding : 5,
	width : 350,
	items : [{
				xtype : 'textfield',
				fieldLabel : 'ID',
				name : 'id',
				hidden : true
			}, {
				xtype : 'textfield',
				fieldLabel : '父节点ID',
				name : 'parentid',
				hidden : true
			}, {
				xtype : 'textfield',
				fieldLabel : '标题',
				name : 'name'
			}, {
				xtype : 'textfield',
				fieldLabel : '图标',
				name : 'pic',
			}, {
				xtype : 'textfield',
				fieldLabel : '叶节点',
				name : 'leaf',
				hidden : true
			}],
			buttons : [ {
				text : '上传图片',
				handler : function(view) {

					win = Ext.create('Ext.window.Window', {
						title : '选择图片',
						id : 'imgselector',
						height : 500,
						width : 600,
						autoScroll:true,
						layout : 'fit',
						trigger : view.up('form').items.items[3]
					}).show();
					win.add(Ext.create('comedical.view.config.form.uploadForm',{url:comedical.util.Config.upload+'knowledge'}));

				}
			} ]

		// buttons : [{
		// text : '提交',
		//		
		// }]
	});