Ext.define('comedical.view.config.form.FormDepartment', {
	extend : 'Ext.form.Panel',
	win : null,
	// title: 'Basic Form',
	// renderTo: Ext.getBody(),
	bodyPadding : 5,
	width : 350,
	items : [ 
	          {
			xtype : 'combobox',
			fieldLabel : '选择城市',
			displayField : 'name',
			valueField : 'id',
			store : Ext.create('Ext.data.Store', {
				fields : city_model.config.fields,
				proxy : {
					type : 'ajax',
					url : comedical.util.Config.getService() + "getAllCity",
					reader : 'json'
				}
			}),
			listeners : {
				select : function(combo, records, eOpts) {				
						this.up().items.items[1].loadData(records[0].data.id);
				}
			}
		}, {
			xtype : 'combobox',
			fieldLabel : '选择区域',
			displayField : 'name',
			queryMode: 'local',
			valueField : 'id',
			name:'areaid',
			store : Ext.create('Ext.data.Store', {
				fields : area_model.config.fields
			}),
			loadData : function(id) {
				comedical.util.DataApi.Core.getAreaByCity(function(res, scope) {
					res = Ext.decode(res);
					if (res.code == 200) {
						//scope.store.removeAll();
						scope.store.loadData(res.result);
					}

				}, this, {
					cityid : id
				});
			},
			listeners : {
				select : function(combo, records, eOpts) {				
						this.up().items.items[2].loadData(records[0].data.id);
				}
			}
		}, {
			xtype : 'combobox',
			fieldLabel : '选择医院',
			displayField : 'name',
			queryMode: 'local',
			valueField : 'id',
			name:'hospitalid',
			store : Ext.create('Ext.data.Store', {
				fields : hospital_model.config.fields
			}),
			loadData : function(id) {
				comedical.util.DataApi.Core.getHospitalByArea(function(res,
						scope) {
					res = Ext.decode(res);
					if (res.code == 200) {
						scope.store.removeAll();
						scope.store.loadData(res.result);
					}

				}, this, {
					areaid : id
				});
			}
		},{
		xtype : 'textfield',
		fieldLabel : 'ID',
		name : 'id',
		hidden : true
	}, {
		xtype : 'textfield',
		fieldLabel : '名称',
		name : 'name'
	}, {
		xtype : 'htmleditor',
		fieldLabel : '介绍',
		width : 580,
		height : 250,
		name : 'introduce'
	}]

});