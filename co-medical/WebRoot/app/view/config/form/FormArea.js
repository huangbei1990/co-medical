Ext.define('comedical.view.config.form.FormArea', {
	extend : 'Ext.form.Panel',
	win : null,
	// title: 'Basic Form',
	// renderTo: Ext.getBody(),
	bodyPadding : 5,
	width : 350,
	items : [ {
		xtype : 'textfield',
		fieldLabel : 'ID',
		name : 'id',
		hidden : true
	}, {
		xtype : 'textfield',
		fieldLabel : '名称',
		name : 'name'
	},{
		name:'cityid',
		xtype : 'combobox',
		fieldLabel : '选择城市',
		displayField : 'name',
		valueField : 'id',
		store : Ext.create('Ext.data.Store', {
			fields : city_model.config.fields,
			proxy : {
				type : 'ajax',
				url : comedical.util.Config.getService() + "getAllCity",
				reader : 'json'
			}
		})
	}]

});