Ext
		.define(
				'comedical.view.config.form.QuestionPanel',
				{
					extend : 'Ext.Panel',
					id : 'questionPanel',
					autoDestory : true,
					scrollable : true,
					border : false,
					layout : 'column',
					config : {
						// cls : 'nav-list',

						items : [ {
							xtype : 'panel',
							id : 'question',
							title : '医生咨询',
							columnWidth : 1,
							border : false,
						// layout : 'fit',
						}, {
							xtype : 'panel',
							columnWidth : 1,
							title : '回复列表',
							id : 'answer',
							border : false,

						} ]

					},
					setValue : function(record, mainEl) {
						item = this.queryById('question');

						item.title = "用户：" + record.getData().username
								+ "  医生咨询"

						html = '<div style="text-align:center;font-size: 20px;font-weight: bold;">咨询问题：'
								+ record.getData().title
								+ '</div></br></br><div>'
								+ record.getData().content
								+ '</div></br></br></br><div>';
						html += this.addImage(record.getData().pic);

						html += '</div>';

						item.html = html;
						comedical.util.DataApi.Core.queryPrivateQuestionAnswer(
								function(res, scope) {
									res = Ext.decode(res);
									if (res.code == 200) {
										scope.setAnswer(res.result, scope)
									}

								}, this, {
									'QuestionID' : record.getData().id
								})

					},
					clearValue : function() {
						this.items.items[0].setHtml('<div></div>')
						this.clearContact(this.items.items[4]);
						this.items.items[2].setHtml('<div></div>')
					},
					setAnswer : function(values, scope) {

						for (i = 0; i < values.length; i++) {

							if (values[i].adminUserid.indexOf('U') >= 0) {
								this.addUserAnwser(values[i], scope);
							} else {
								this.addDoctorAnwser(values[i], scope);
								
							}

						}
					},
					addUserAnwser : function(value, scope) {
						panel = scope.queryById('answer');
						item = Ext.create('Ext.Panel', {
							border : false,
						});
						html = "";
						html = '</br><div class="useranwser">'
								+ '<div style="margin-left:10px;font-size: 12px;font-weight: bold;color:blue">用户&nbsp&nbsp'
								+ value.createdate + '</div>'
								+ '</br><div style="margin-left:10px;">'
								+ value.content + '</div>'
								+ '<div style="margin-left:10px;">';

						html += this.addImage(value.answer_pic);
						html += "</div></div></br>";
						item.html = html;
						panel.add(item);
					},
					addDoctorAnwser : function(value, scope) {
						panel = scope.queryById('answer');
						item = Ext.create('Ext.Panel', {
							border : false,
						});
						html = "";
						html = '</br><div class="doctoranwser">'
								+ '<div style="margin-left:10px;font-size: 12px;font-weight: bold;text-align:left;color:red">医生&nbsp&nbsp'
								+ value.createdate
								+ '</div>'
								+ '</br><div style="margin-left:10px;text-align:left;">'
								+ value.content
								+ '</div></div></br>'
								+ '<div style="text-align:left;margin-left:10px;">';
						html += this.addImage(value.answer_pic);
						html += "</div></div></br>";
						item.html = html;
						panel.add(item);
					},
					addImage : function(values) {
						html = "";
						if (values) {
							picz = values.split(',');
							if (picz.length > 1) {
								for (n = 0; n < picz.length; n++) {
									src = '\'/co-medical/Images/question/'+ picz[n]+ '\'';
									html += '<div style="width:200px; height:200px;text-align:center;margin-left:10px">';
									html += '<img src='+src+'  style="width:100%;" onClick="openimg('+src+')" ></div>';
								}
							}
							
						}
						return html
					}

				});

function showDetail(title, content) {
	var win = Ext.create('YongYou.util.Window', {});
	win.initialPanelControl(new Ext.Panel({
		fullscreen : true,
		scrollable : true,
		layout : 'fit',
		height : '100%',
		width : '100%',
		html : content
	}), title)
}