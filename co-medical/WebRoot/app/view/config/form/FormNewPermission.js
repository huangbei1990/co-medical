Ext.define('comedical.view.config.form.FormNewPermission', {
	extend : 'Ext.form.Panel',
	win : null,
	// title: 'Basic Form',
	// renderTo: Ext.getBody(),
	bodyPadding : 5,
	width : 350,
	items : [{
				xtype : 'textfield',
				fieldLabel : 'ID',
				name : 'id',
				hidden : true
			}, {
				xtype : 'textfield',
				fieldLabel : '父节点ID',
				name : 'perid',
				hidden : true
			}, {
				xtype : 'textfield',
				fieldLabel : '标题',
				name : 'name'
			}, {
				xtype : 'textfield',
				fieldLabel : '定义面板',
				name : 'panel',
			}, {
				xtype : 'textfield',
				fieldLabel : '叶节点',
				name : 'leaf',
				hidden : true
			}]

		// buttons : [{
		// text : '提交',
		//		
		// }]
	});