Ext.define('comedical.view.config.form.FormMsg', {
	extend : 'Ext.form.Panel',
	win : null,
	// title: 'Basic Form',
	// renderTo: Ext.getBody(),
	bodyPadding : 5,
	width : 350,
	autoScroll:true,
	items : [  {
		xtype : 'textfield',
		fieldLabel : 'ID',
		name : 'id',
		hidden : true
	}, {
		xtype : 'textareafield',
		fieldLabel : '标题',
		width : 400,
		height : 50,
		name : 'title'
	}, {
		xtype : 'ad_htmlfield',
		fieldLabel : '内容',
		width : 750,
		height : 500,
		name : 'content',
		url:comedical.util.Config.upload+'msg',
		path:comedical.util.Config.url+'Images/msg/'
		
	},{
		xtype : 'textfield',
		fieldLabel : '图片',
		name : 'pic',
		readOnly:true,
		hidden : false
	} ,{
		xtype : 'textfield',
		fieldLabel : '创建时间',
		name : 'createdate',
		value:Ext.Date.format(new Date(), 'Y-m-d H:i:s'),
		readOnly:true,
		hidden : true
	} ,{
		xtype : 'textfield',
		fieldLabel : '已发送',
		name : 'issend',
		value:0,
		readOnly:true,
		hidden : true
	}],
//	buttons : [ {
//		text : '上传图片',
//		handler : function(view) {
//
//			win = Ext.create('Ext.window.Window', {
//				title : '选择图片',
//				id : 'imgselector',
//				height : 500,
//				width : 600,
//				autoScroll:true,
//				layout : 'fit',
//				trigger : view.up('form').items.items[3]
//			}).show();
//			win.add(Ext.create('comedical.view.config.form.uploadForm',{url:comedical.util.Config.upload+'msg'}));
//
//		}
//	} ],
	setPicValue:function(name){
		temp = this.items.items[3].getValue();
		if(temp.length>0){
			temp+=","
		}
		temp+=name;
		this.items.items[3].setValue(temp);
	}
});