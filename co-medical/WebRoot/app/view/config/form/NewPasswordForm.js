Ext.define('comedical.view.config.form.NewPasswordForm', {
	extend : 'Ext.form.Panel',
	win : null,
	// title: 'Basic Form',
	// renderTo: Ext.getBody(),
	bodyPadding : 5,
	width : 350,
	items : [ {
		xtype : 'textfield',
		fieldLabel : 'ID',
		name : 'id',
		hidden : true
	}, {
		xtype : 'textfield',
		fieldLabel : '原 密 码',
		name : 'password',
		inputType : "password",
		allowBlank : false
	}, {
		xtype : 'textfield',
		fieldLabel : '新 密 码',
		name : 'newpassword',
		inputType : "password",
		allowBlank : false
	}, {
		xtype : 'textfield',
		fieldLabel : '重复输入',
		name : 'validpassword',
		inputType : "password",
		allowBlank : false

	} ]

});