Ext.define('comedical.view.config.form.FormNews', {
	extend : 'Ext.form.Panel',
	win : null,
	// title: 'Basic Form',
	// renderTo: Ext.getBody(),
	bodyPadding : 5,
	width : 350,
	autoScroll:true,
	items : [ {
		xtype : 'combobox',
		fieldLabel : '选择城市',
		displayField : 'name',
		valueField : 'id',
		store : Ext.create('Ext.data.Store', {
			fields : city_model.config.fields,
			proxy : {
				type : 'ajax',
				url : comedical.util.Config.getService() + "getAllCity",
				reader : 'json'
			}
		}),
		listeners : {
			select : function(combo, records, eOpts) {
				this.up().items.items[1].loadData(records[0].data.id);
			}
		}
	}, {
		xtype : 'combobox',
		fieldLabel : '选择区域',
		displayField : 'name',
		queryMode: 'local',
		valueField : 'id',
		name : 'areaid',
		store : Ext.create('Ext.data.Store', {
			fields : area_model.config.fields
		}),
		loadData : function(id) {
			comedical.util.DataApi.Core.getAreaByCity(function(res, scope) {
				res = Ext.decode(res);
				if (res.code == 200) {
					// scope.store.removeAll();
					scope.store.loadData(res.result);
				}

			}, this, {
				cityid : id
			});
		},
	listeners : {
		select : function(combo, records, eOpts) {				
				this.up().items.items[2].loadData(records[0].data.id);
		}
	}
	}, {
		xtype : 'combobox',
		fieldLabel : '选择医院',
		displayField : 'name',
		queryMode: 'local',
		valueField : 'id',
		name : 'hospitalid',
		store : Ext.create('Ext.data.Store', {
			fields : hospital_model.config.fields
		}),
		loadData : function(id) {
			comedical.util.DataApi.Core.getHospitalByArea(function(res, scope) {
				res = Ext.decode(res);
				if (res.code == 200) {
					// scope.store.removeAll();
					scope.store.loadData(res.result);
				}

			}, this, {
				areaid : id
			});
		},
		listeners : {
			select : function(combo, records, eOpts) {				
					this.up().items.items[3].loadData(records[0].data.id);
			}
		}
	}, {
		xtype : 'combobox',
		fieldLabel : '选择科室',
		displayField : 'name',
		queryMode: 'local',
		valueField : 'id',
		name : 'departmentid',
		store : Ext.create('Ext.data.Store', {
			fields : department_model.config.fields
		}),
		loadData : function(id) {
			comedical.util.DataApi.Core.queryDepartmentByHospital(function(res, scope) {
				res = Ext.decode(res);
				if (res.code == 200) {
					// scope.store.removeAll();
					scope.store.loadData(res.result);
				}

			}, this, {
				hospitalid : id
			});
		}
	}, {
		xtype : 'textfield',
		fieldLabel : 'ID',
		name : 'id',
		hidden : true
	}, {
		xtype : 'textareafield',
		fieldLabel : '标题',
		width : 400,
		height : 50,
		name : 'title'
	}, {
		xtype : 'combobox',
		fieldLabel : '置顶',
		name : 'top',
		displayField : 'name',
		valueField : 'value',

		store : Ext.create('Ext.data.Store', {
			fields : [ 'value', 'name' ],
			data : [ {
				'value' : 0,
				'name' : '否'
			}, {
				'value' : 1,
				'name' : '是'
			} ]
		})
	}, {
		xtype : 'textareafield',
		fieldLabel : '新闻概要',
		width : 400,
		height : 100,
		name : 'subview'
	}, {
		xtype : 'htmleditor',
		fieldLabel : '新闻正文',
		width : 1000,
		height : 800,
		name : 'content'
	},{
		xtype : 'textfield',
		fieldLabel : '图片',
		name : 'pic',
		readOnly:true,
		hidden : false
	} ,{
		xtype : 'textfield',
		fieldLabel : '创建时间',
		name : 'createdate',
		value:Ext.Date.format(new Date(), 'Y-m-d H:i:s'),
		readOnly:true,
		hidden : true
	} ],
	buttons : [ {
		text : '上传图片',
		handler : function(view) {

			win = Ext.create('Ext.window.Window', {
				title : '选择图片',
				id : 'imgselector',
				height : 500,
				width : 600,
				autoScroll:true,
				layout : 'fit',
				trigger : view.up('form').items.items[9]
			}).show();
			win.add(Ext.create('comedical.view.config.form.uploadForm',{url:comedical.util.Config.upload+'news'}));

		}
	} ],
	check:function(){
		var hosid = Ext.util.Cookies.get("hosid");

		 // alert(id);
		if(!hosid || hosid=='undefined'){
			return;
		}else if(hosid=='super'){
		}else{
			this.items.items[0].hidden=true;
			this.items.items[1].hidden=true;
			this.items.items[2].hidden=true;
			this.items.items[2].value=hosid;
			this.items.items[3].loadData(hosid)
		}
	}
});