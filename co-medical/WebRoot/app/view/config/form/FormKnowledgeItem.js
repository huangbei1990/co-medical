Ext.define('comedical.view.config.form.FormKnowledgeItem', {
	extend : 'Ext.form.Panel',
	win : null,
	// title: 'Basic Form',
	// renderTo: Ext.getBody(),
	bodyPadding : 5,
	width : 350,
	items : [{
				xtype : 'textfield',
				fieldLabel : 'ID',
				name : 'id',
				hidden : true
			}, {
				xtype : 'textfield',
				fieldLabel : '父节点ID',
				name : 'parentid',
				hidden : true
			}, {
				xtype : 'textfield',
				fieldLabel : '标题',
				name : 'name'
			}, {
				xtype : 'htmleditor',
				fieldLabel : '内容',
				name : 'info',
				width : 650,
				height : 400,
			}]
		// buttons : [{
		// text : '提交',
		//		
		// }]
	});