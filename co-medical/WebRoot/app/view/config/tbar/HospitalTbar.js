Ext.define('comedical.view.config.tbar.HospitalTbar', {
	// extend : 'Ext.toolbar.Toolbar',
	grid_type : 0,
	config : {},
	key : "",
	constructor : function(config) {
		this.initConfig(config);
	},
	items : [],
	get_tbar : function(type) {
		this.grid_type = type;
		this.items.splice(0, this.items.length);
		this.items.push({
			// id:key+"city",
			xtype : 'combobox',
			fieldLabel : '选择城市',
			displayField : 'name',
			key : type,
			valueField : 'id',
			store : Ext.create('Ext.data.Store', {
				fields : city_model.config.fields,
				proxy : {
					type : 'ajax',
					url : comedical.util.Config.getService() + "getAllCity",
					reader : 'json'
				}
			}),
			listeners : {
				select : function(combo, records, eOpts) {
					if (this.key == GridType.Area) {
						this.up('grid').loadRecords(records[0].data.id);
					} else {
						//this.up().items.items[1].clearValue();
						this.up().items.items[1].loadData(records[0].data.id)

					}

				}
			}
		}, {
			xtype : 'combobox',
			fieldLabel : '选择区域',
			displayField : 'name',
			valueField : 'id',
			queryMode: 'local',
			key : type,
			store : Ext.create('Ext.data.Store', {
				fields : area_model.config.fields,
				data : [ {
					id : ''
				}, {
					name : ''
				} ]
			}),
			loadData : function(id) {
				this.clearValue();
				comedical.util.DataApi.Core.getAreaByCity(function(res, scope) {
					res = Ext.decode(res);
					if (res.code == 200) {
						scope.store.removeAll();
						scope.store.loadData(res.result);
					}


				}, this, {
					cityid : id
				});
			},
			listeners : {
				select : function(combo, records, eOpts) {
					if (this.key == GridType.Hospital) {
						this.up('grid').loadRecords(records[0].data.id);
					} else {
						this.up().items.items[2].loadData(records[0].data.id)

					}

				}
			}
		}, {
			xtype : 'combobox',
			fieldLabel : '选择医院',
			displayField : 'name',
			valueField : 'id',
			queryMode: 'local',
			key : type,
			store : Ext.create('Ext.data.Store', {
				fields : hospital_model.config.fields,
				data : [ {
					id : ''
				}, {
					name : ''
				} ]
			}),
			loadData : function(id) {
				this.clearValue();
				comedical.util.DataApi.Core.getHospitalByArea(function(res,
						scope) {
					res = Ext.decode(res);
					if (res.code == 200) {
						scope.store.removeAll();
						scope.store.loadData(res.result);
					}

				}, this, {
					areaid : id
				});
			},
			listeners : {
				select : function(combo, records, eOpts) {
					if (this.key == GridType.Department) {
						this.up('grid').loadRecords(records[0].data.id);
					} else {
						this.up().items.items[3].loadData(records[0].data.id)

					}

				}
			}
		}, {
			xtype : 'combobox',
			fieldLabel : '选择科室',
			displayField : 'name',
			queryMode: 'local',
			valueField : 'id',
			key : type,
			store : Ext.create('Ext.data.Store', {
				fields : department_model.config.fields,

			}),
			loadData : function(id) {
				this.clearValue();
				comedical.util.DataApi.Core.queryDepartmentByHospital(function(
						res, scope) {
					res = Ext.decode(res);
					if (res.code == 200) {
						scope.store.removeAll();
						scope.store.loadData(res.result);
					}

				}, this, {
					hospitalid : id
				});
			},
			listeners : {
				select : function(combo, records, eOpts) {

					if (this.key == GridType.Doctor
							|| this.key == GridType.News) {
						this.up('grid').loadRecords(records[0].data.id);
					} else {
						// this.up().items.items[4].loadData(records[0].data.id)

					}

				}
			}
		})
		switch (type) {
		case GridType.Area:
			this.items.pop();
			this.items.pop();
			this.items.pop();
			break;
		case GridType.Hospital:
			this.items.pop();
			this.items.pop();
			break;
		case GridType.Department:
			this.items.pop();
			break;
		case GridType.Doctor:
			break;
		case GridType.News:
			break;
		}

		this.items.push({
			xtype : 'button',
			text : '新建',
			icon : 'js/resources/images/icons/fam/add.png',
			handler : function(view, rowIndex, colIndex, actionItem, event,
					record, row) {
				this.up('grid').createRecord();
			}
		}, {
			xtype : 'button',
			text : '编辑',
			icon : 'js/resources/images/edit_task.png',
			handler : function(view, rowIndex, colIndex, actionItem, event,
					record, row) {
				this.up('grid').updateRecord();
			}
		}, {
			xtype : 'button',
			text : '删除',
			icon : 'js/resources/images/icons/fam/delete.png',
			handler : function(view, rowIndex, colIndex, actionItem, event,
					record, row) {
				this.up('grid').removeRecord();
			}
		})
		return this.items;
	},
	get_hospitalTbar:function(hospitalid){
		this.items.splice(0, this.items.length);
		var combox= Ext.create('Ext.form.ComboBox',{
				fieldLabel : '选择科室',
				displayField : 'name',
				queryMode: 'local',
				valueField : 'id',
				store : Ext.create('Ext.data.Store', {
					fields : department_model.config.fields,

				}),
				loadData : function(id) {
					this.clearValue();
					comedical.util.DataApi.Core.queryDepartmentByHospital(function(
							res, scope) {
						res = Ext.decode(res);
						if (res.code == 200) {
							scope.store.removeAll();
							scope.store.loadData(res.result);
						}

					}, this, {
						hospitalid : id
					});
				},
				listeners : {
					select : function(combo, records, eOpts) {

							this.up('grid').loadRecords(records[0].data.id);

					}
				}
			});
			if(hospitalid && hospitalid!="super"){
				combox.loadData(hospitalid);
			}
		this.items.push(combox);
		this.items.push({
			xtype : 'button',
			text : '新建',
			icon : 'js/resources/images/icons/fam/add.png',
			handler : function(view, rowIndex, colIndex, actionItem, event,
					record, row) {
				this.up('grid').createRecord();
			}
		}, {
			xtype : 'button',
			text : '编辑',
			icon : 'js/resources/images/edit_task.png',
			handler : function(view, rowIndex, colIndex, actionItem, event,
					record, row) {
				this.up('grid').updateRecord();
			}
		}, {
			xtype : 'button',
			text : '删除',
			icon : 'js/resources/images/icons/fam/delete.png',
			handler : function(view, rowIndex, colIndex, actionItem, event,
					record, row) {
				this.up('grid').removeRecord();
			}
		});
		return this.items;
	}

});