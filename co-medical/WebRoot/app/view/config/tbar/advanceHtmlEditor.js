Ext.define('comedical.view.config.tbar.advanceHtmlEditor', {
	// extend : 'Ext.toolbar.Toolbar',
	extend : 'Ext.form.HtmlEditor',
	alias : ['widget.ad_htmlfield'],
    xtype: 'ad_htmlfield',
    url:null,
    path:null,
	 addImage : function() {
	        var editor = this;
	        var imgform = Ext.create('Ext.form.Panel',{url:this.url,path:this.path,
	            region : 'center',
	            labelWidth : 55,
	            frame : true,
	            bodyStyle : 'padding:5px 5px 0',
	            autoScroll : true,
	            border : false,
	            fileUpload : true,
	            items : [{
					xtype : 'filefield',
					name : 'file',
					fieldLabel : '文件',
					labelWidth : 50,
					msgTarget : 'side',
					allowBlank : false,
					anchor : '100%',
					buttonText : '选择文件...'
				}],
	            buttons : [{
	                text : '上传',
	                type : 'submit',
	                handler : function() {
	                    if (!imgform.form.isValid()) {return;}
	                    imgform.form.submit({
	                        waitMsg : '正在上传',
	                       
	                        success : function(form, action) {
	                            var element = document.createElement("img");
	                            element.width=200;
	                            element.hight=200;
	                            element.src = form.owner.path+action.result.msg;
	                            if (Ext.isIE) {
	                                editor.insertAtCursor(element.outerHTML);
	                            } else {
	                                var selection = editor.win.getSelection();
	                                if (!selection.isCollapsed) {
	                                    selection.deleteFromDocument();
	                                }
	                                if (!selection.isCollapsed) {
                                        selection.deleteFromDocument();
                                    }
	                                selection.getRangeAt(0).insertNode(element);
	                            }
	                            if(editor.up('panel').setPicValue){
	                            	editor.up('panel').setPicValue(action.result.msg);
	                            }
	                            win.hide();
	                        },
	                        failure : function(form, action) {
	                            form.reset();
	                            if (action.failureType == Ext.form.Action.SERVER_INVALID)
	                                Ext.MessageBox.alert('警告',
	                                        action.result.errors.msg);
	                        }
	                    });
	                }
	            }, {
	                text : '关闭',
	                type : 'submit',
	                handler : function() {
	                    win.close(this);
	                }
	            }]
	        })

	        var win = new Ext.Window({
	                    title : "上传图片",
	                    width : 300,
	                    height : 200,
	                    modal : true,
	                    border : false,
	                    iconCls : "picture.png",
	                    layout : "fit",
	                    items : imgform

	                });
	        win.show();
	    },
	    createToolbar : function(editor) {
	    	var me = this;
	        me.callParent(arguments);
	        me.toolbar.insert(9, {
	            xtype: 'button',
	            icon: 'js/resources/images/icons/fam/image_add.png',
	            handler: this.addImage,
	            scope: this
	        });
	        return me.toolbar;
	       
	    }
});