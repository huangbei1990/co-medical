Ext.override(Ext.menu.Menu, {
			// inherit docs
			getFocusEl : function() {
				return this.focusedItem || this.el;
			}
		});
Ext.define('comedical.view.config.LeftPanel', {
	extend : 'Ext.panel.Panel',
	 xtype:'leftPanel',
	alias: 'widget.leftPanel',
	id : 'left-menu',
	title : '工具栏',
	region : 'west',
	animCollapse : true,
	width : 200,
	minWidth : 150,
	maxWidth : 400,
	split : true,
	collapsible : true,
	layout : {
		type : 'accordion',
		animate : true
	},
	items : [],
	listeners : {
		beforerender : function(pa, eOpts) {
			var id = Ext.util.Cookies.get("uid");

			 // alert(id);
			if(!id || id=='undefined'){
				return;
			}
			comedical.util.DataApi.Core.getMenuList(function(rs, scope) {
				
						res = Ext.decode(rs);
						if(res.code !=200){
							return;
						}
						menulist = res.result;
						var menus = [];
						for (i = 0; i < menulist.length; i++) {
							var menuConfig = {
								title : menulist[i].name,
								xtype : 'menu',
								cls : 'feed-list',
								iconCls : 'nav',
								showSeparator : false,
								floating : false,
								hideHeader : false,
								parent:scope,
								items : [],
								collapsed : i > 0,
								listeners : {
									click:function( menu, item, e, eOpts){
										
										comedical.util.EventHandle.events.leftMenuClick(this.parent,item.data)
									
									}
								}
								
							};
							if (menulist[i].children.length > 0) {
								for (x = 0; x < menulist[i].children.length; x++) {

									menuConfig.items.push({
									id:"menu-"+menulist[i].children[x].id,
									text : menulist[i].children[x].name,
									data:menulist[i].children[x]
											});
								}
							}
							menus.push(menuConfig);
						}
						scope.add(menus);
					},this,{id:id});

		}
	},
	onClick:function(){
		test();
	}

});