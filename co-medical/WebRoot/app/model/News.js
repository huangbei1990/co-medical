Ext.define('comedical.model.News', {
	extend : 'Ext.data.Model',
	config : {
		fields : [ {
			name : 'id',
			type : 'string'
		}, {
			name : 'title',
			type : 'string'
		}, {
			name : 'content',
			type : 'string'
		}, {
			name : 'pic',
			type : 'string'
		} , {
			name : 'top',
			type : 'string'
		}, {
			name : 'createdate',
			type : 'string'
		}, {
			name : 'departmentid',
			type : 'string'
		}, {
			name : 'subview',
			type : 'string'
		}],
		columns : [ {
			text : 'ID',
			dataIndex : 'id'
		}, {
			text : '标题',
			flex : 2,
			sortable : true,
			dataIndex : 'title'
		} , {
			text : '图片文件名',
			flex : 1,
			dataIndex : 'pic',
			sortable : false
		} , {
			text : '置顶顺序',
			flex : 1,
			dataIndex : 'top',
			sortable : false
		} , {
			text : '创建时间',
			flex : 1,
			dataIndex : 'createdate',
			sortable : false
		} , {
			text : '科室ID',
			flex : 1,
			dataIndex : 'departmentid',
			sortable : false
		} , {
			text : '新闻概要',
			flex : 1,
			dataIndex : 'subview',
			sortable : false
		}]
	}
});