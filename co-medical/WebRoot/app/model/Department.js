Ext.define('comedical.model.Department', {
	extend : 'Ext.data.Model',
	config : {
		fields : [ {
			name : 'id',
			type : 'string'
		}, {
			name : 'name',
			type : 'string'
		}, {
			name : 'introduce',
			type : 'string'
		}, {
			name : 'hospitalid',
			type : 'string'
		}],
		columns : [ {
			text : 'ID',
			dataIndex : 'id'
		}, {
			text : '标题',
			flex : 2,
			sortable : true,
			dataIndex : 'name'
		}, {
			text : '介绍',
			flex : 1,
			dataIndex : 'introduce',
			sortable : false
		} ]
	}
});