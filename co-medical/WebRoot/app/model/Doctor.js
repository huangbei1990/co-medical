Ext.define('comedical.model.Doctor', {
	extend : 'Ext.data.Model',
	config : {
		fields : [ {
			name : 'id',
			type : 'string'
		}, {
			name : 'name',
			type : 'string'
		}, {
			name : 'departmentid',
			type : 'string'
		}, {
			name : 'positon',
			type : 'string'
		} , {
			name : 'bookType',
			type : 'string'
		}, {
			name : 'introduce',
			type : 'string'
		}, {
			name : 'pic',
			type : 'string'
		}, {
			name : 'money',
			type : 'string'
		}, {
			name : 'sort',
			type : 'int'
		}],
		columns : [ {
			text : 'ID',
			dataIndex : 'id'
		}, {
			text : '姓名',
			flex : 2,
			sortable : true,
			dataIndex : 'name'
		} , {
			text : '部门ID',
			flex : 1,
			dataIndex : 'departmentid',
			sortable : false,
			hidden : true
		} , {
			text : '职位',
			flex : 1,
			dataIndex : 'positon',
			sortable : false
		} , {
			text : '预定类型',
			flex : 1,
			dataIndex : 'bookType',
			sortable : false,
			hidden : true
		} , {
			text : '介绍',
			flex : 1,
			dataIndex : 'introduce',
			sortable : false
		} , {
			text : '图片',
			flex : 1,
			dataIndex : 'pic',
			sortable : false
		}, {
			text : '排序',
			flex : 1,
			dataIndex : 'sort',
			sortable : false,
			editor : {
				xtype : 'numberfield',
				 maxValue: 99,
			        minValue: 0,
				selectOnFocus : true
			}
			
		}, {
			text : '挂号费',
			flex : 1,
			dataIndex : 'money',
			sortable : false,
			hidden : true
		}]
	}
});