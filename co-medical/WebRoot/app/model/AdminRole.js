Ext.define('comedical.model.AdminRole', {
	extend : 'Ext.data.Model',
	config : {
		fields : [ {
			name : 'id',
			type : 'string'
		}, {
			name : 'name',
			type : 'string'
		} ],
		columns : [
				{
					text : 'ID',
					dataIndex : 'id'
				},
				{
					text : '角色名',
					dataIndex : 'name',
					width : 400
				},
				{
					text : '编辑',
					width : 55,
					menuDisabled : true,
					xtype : 'actioncolumn',
					tooltip : '编辑角色信息',
					align : 'center',
					icon : 'js/resources/images/edit_task.png',
					handler : function(grid, rowIndex, colIndex, actionItem,
							event, record, row) {
						comedical.util.EventHandle.events.ShowForm(true, grid,
								record, '编辑角色信息',
								'comedical.view.config.form.EditUserForm',
								addUserCallback)
					}
				},
				{
					text : '分配权限',
					menuDisabled : true,
					xtype : 'actioncolumn',
					tooltip : '分配权限',
					align : 'center',
					icon : 'js/resources/images/icons/fam/connect.png',
					handler : function(grid, rowIndex, colIndex, actionItem,
							event, record, row) {
						comedical.util.DataApi.Core.getRolePermission(function(res,scope){
							res = Ext.decode(res);
							var permission=null;
							if (res.code == 200) {
								permission=res.result;
							}

							comedical.util.DataApi.Core.getRootPermission(
									function(res, scope,op) {
										
										res = Ext.decode(res);

										store = Ext.create(
														'Ext.data.Store',
														{
															fields : permission_model.config.fields

														});

										grid = Ext.create(
														'comedical.view.config.grid.MutilSelectGrid',
														{
															store : store,
															columns : permission_model.config.columns
														});
										
										if (res.code == 200) {
											store.add(res.result);
										}

										win = Ext.create(
														'Ext.window.Window',
														{
															title : '选择权限',
															// id :
															// 'imgselector',
															height : 500,
															width : 600,
															layout : 'fit',
															trigger : scope,
															setValue : function(
																	id,
																	value) {
																comedical.util.DataApi.Core.addLnkRolePermission(function(res,scope){
																	res = Ext.decode(res);
																	if (res.code == 200) {
																		
																	}else{
																		Ext.Msg.alert('提示','更新失败');
																	}
																},this,{roleid:record.data.id,permissionid:id})
															}
														})
												.show();
										win.add(grid)
										if(op){
											var obj = grid.getSelectionModel();
											for(i=0;i<op.length;i++){
												index=store.find('id',op[i].permissionid)
												if(index>=0){
													obj.select(index,true);
												}
											}
											    	
													
											
										}
									}, scope,null,permission)
						},this.up('panel'),{roleid:record.data.id})
					}
				}, {
					text : '删除角色',
					width : 70,
					menuDisabled : true,
					xtype : 'actioncolumn',
					tooltip : '删除角色',
					align : 'center',
					icon : 'js/resources/images/icons/fam/delete.png'
				} ]
	}
});