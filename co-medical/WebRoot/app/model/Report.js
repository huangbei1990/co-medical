Ext.define('comedical.model.Report', {
	extend : 'Ext.data.Model',
	config : {
		fields : [ {
			name : 'id',
			type : 'string'
		}, {
			name : 'userid',
			type : 'string'
		}, {
			name : 'content',
			type : 'string'
		}, {
			name : 'title',
			type : 'string'
		} , {
			name : 'pic',
			type : 'string'
		}, {
			name : 'createdate',
			type : 'string'
		}, {
			name : 'hospitalid',
			type : 'string'
		}, {
			name : 'hospitalName',
			type : 'string'
		},{
			name : 'doctorid',
			type : 'string'
		}, {
			name : 'doctorname',
			type : 'string'
		}, {
			name : 'answercount',
			type : 'int'
		}],
		columns : [ {
			text : 'ID',
			dataIndex : 'id'
		}, {
			text : '标题',
			flex : 2,
			sortable : true,
			dataIndex : 'title'
		} , {
			text : '已回复',
			flex : 1,
			dataIndex : 'answercount',
			sortable : false
		} , {
			text : '创建时间',
			flex : 1,
			dataIndex : 'createdate',
			sortable : false
		} , {
			text : '医生',
			flex : 1,
			dataIndex : 'doctorname',
			sortable : false
		} , {
			text : '医院',
			flex : 1,
			dataIndex : 'hospitalName',
			sortable : false
		}]
	}
});