Ext.define('comedical.model.Area', {
	extend : 'Ext.data.Model',
	config : {
		fields : [ {
			name : 'id',
			type : 'string'
		}, {
			name : 'name',
			type : 'string'
		}, {
			name : 'cityid',
			type : 'string'
		}],
		columns : [ {
			text : 'ID',
			dataIndex : 'id'
		}, {
			text : '标题',
			flex : 2,
			sortable : true,
			dataIndex : 'name'
		}, {
			text : '城市',
			flex : 1,
			dataIndex : 'cityid',
			sortable : false
		} ]
	}
});