Ext.define('comedical.model.Hospital', {
	extend : 'Ext.data.Model',
	config : {
		fields : [ {
			name : 'id',
			type : 'string'
		}, {
			name : 'name',
			type : 'string'
		}, {
			name : 'level',
			type : 'string'
		}, {
			name : 'characters',
			type : 'string'
		}, {
			name : 'address',
			type : 'string'
		} , {
			name : 'website',
			type : 'string'
		}, {
			name : 'tel',
			type : 'string'
		}, {
			name : 'booktel',
			type : 'string'
		}, {
			name : 'introduce',
			type : 'string'
		}, {
			name : 'areaid',
			type : 'string'
		}
		, {
			name : 'daohang',
			type : 'string'
		}
		, {
			name : 'forenoontime',
			type : 'string'
		}, {
			name : 'afternoontime',
			type : 'string'
		}],
		columns : [ {
			text : 'ID',
			dataIndex : 'id'
		}, {
			text : '名称',
			flex : 2,
			sortable : true,
			dataIndex : 'name'
		}, {
			text : '级别',
			flex : 1,
			dataIndex : 'level',
			sortable : false
		} , {
			text : '类型',
			flex : 1,
			dataIndex : 'characters',
			sortable : false
		} , {
			text : '地址',
			flex : 1,
			dataIndex : 'address',
			sortable : false
		} , {
			text : '网站',
			flex : 1,
			dataIndex : 'website',
			sortable : false
		} , {
			text : '电话',
			flex : 1,
			dataIndex : 'tel',
			sortable : false
		} , {
			text : '预定电话',
			flex : 1,
			dataIndex : 'booktel',
			sortable : false
		} , {
			text : '介绍',
			flex : 1,
			dataIndex : 'introduce',
			sortable : false
		} , {
			text : '区域id',
			flex : 1,
			dataIndex : 'areaid',
			sortable : false,
			hidden:true
		}
		 , {
				text : '上午上班时间',
				flex : 1,
				dataIndex : 'forenoontime',
				sortable : false
			}
		 , {
				text : '下午上班时间',
				flex : 1,
				dataIndex : 'afternoontime',
				sortable : false
			}]
	}
});