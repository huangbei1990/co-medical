Ext.define('comedical.model.Msg', {
	extend : 'Ext.data.Model',
	config : {
		fields : [ {
			name : 'id',
			type : 'string'
		}, {
			name : 'title',
			type : 'string'
		}, {
			name : 'content',
			type : 'string'
		}, {
			name : 'pic',
			type : 'string'
		} , {
			name : 'createdate',
			type : 'string'
		}, {
			name : 'issend',
			type : 'int'
		}],
		columns : [ {
			text : 'ID',
			dataIndex : 'id'
		}, {
			text : '标题',
			flex : 2,
			sortable : true,
			dataIndex : 'title'
		}  , {
			text : '创建时间',
			flex : 1,
			dataIndex : 'createdate',
			sortable : false
		} , {
			text : '已发标识',
			flex : 1,
			dataIndex : 'issend',
			sortable : false
		}]
	}
});