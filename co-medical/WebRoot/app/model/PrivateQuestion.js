Ext.define('comedical.model.PrivateQuestion', {
	extend : 'Ext.data.Model',
	config : {
		fields : [ {
			name : 'id',
			type : 'string'
		}, {
			name : 'userid',
			type : 'string'
		}, {
			name : 'content',
			type : 'string'
		}, {
			name : 'title',
			type : 'string'
		} , {
			name : 'pic',
			type : 'string'
		}, {
			name : 'createdate',
			type : 'string'
		}, {
			name : 'hospitalid',
			type : 'string'
		}, {
			name : 'name',
			type : 'string'
		}, {
			name : 'username',
			type : 'string'
		},{
			name : 'answer_count',
			type : 'int'
		}],
		columns : [ {
			text : 'ID',
			dataIndex : 'id'
		}, {
			text : '标题',
			flex : 2,
			sortable : true,
			dataIndex : 'title'
		} , {
			text : '已回复',
			flex : 1,
			dataIndex : 'answer_count',
			sortable : false
		} , {
			text : '创建时间',
			flex : 1,
			dataIndex : 'createdate',
			sortable : false
		} , {
			text : '用户',
			flex : 1,
			dataIndex : 'username',
			sortable : false
		} , {
			text : '医院',
			flex : 1,
			dataIndex : 'name',
			sortable : false
		}]
	}
});