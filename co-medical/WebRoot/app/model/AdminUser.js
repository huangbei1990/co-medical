Ext
		.define(
				'comedical.model.AdminUser',
				{
					extend : 'Ext.data.Model',
					config : {
						fields : [ {
							name : 'id',
							type : 'string'
						}, {
							name : 'username',
							type : 'string'
						}, {
							name : 'loginname',
							type : 'string'
						}, {
							name : 'password',
							type : 'string'
						}, {
							name : 'hospitalid',
							type : 'string'
						} ],
						columns : [
								{
									text : 'ID',
									dataIndex : 'id'
								},
								{
									text : '用户名',
									dataIndex : 'username',
									width : 400
								},
								{
									text : '登录名',
									dataIndex : 'loginname',
									width : 400
								},
								{
									text : '编辑',
									width : 55,
									menuDisabled : true,
									xtype : 'actioncolumn',
									tooltip : '编辑用户信息',
									align : 'center',
									icon : 'js/resources/images/edit_task.png',
									handler : function(grid, rowIndex,
											colIndex, actionItem, event,
											record, row) {
										comedical.util.EventHandle.events
												.ShowForm(
														true,
														grid,
														record,
														'编辑用户信息',
														'comedical.view.config.form.EditUserForm',
														addUserCallback)
									}
								},
								{
									text : '分配角色',
									menuDisabled : true,
									xtype : 'actioncolumn',
									tooltip : '分配角色',
									align : 'center',
									icon : 'js/resources/images/icons/fam/connect.png',
									handler : function(grid, rowIndex,
											colIndex, actionItem, event,
											record, row) {
										comedical.util.DataApi.Core.getUserRole(function(res,scope){
											res = Ext.decode(res);
											var uid=null;
											if (res.code == 200) {
												uid=res.result[0].roleid;
											}

											comedical.util.DataApi.Core.getAllRole(
													function(res, scope,oid) {
														
														res = Ext.decode(res);

														store = Ext.create(
																		'Ext.data.Store',
																		{
																			fields : role_model.config.fields

																		});

														grid = Ext.create(
																		'comedical.view.config.grid.SingleSelectGrid',
																		{
																			store : store,
																			columns : role_model.config.columns
																		});
														
														if (res.code == 200) {
															store.add(res.result);
														}

														win = Ext.create(
																		'Ext.window.Window',
																		{
																			title : '选择角色',
																			// id :
																			// 'imgselector',
																			height : 500,
																			width : 600,
																			layout : 'fit',
																			trigger : scope,
																			setValue : function(
																					id,
																					value) {
																				comedical.util.DataApi.Core.addLnkUserRole(function(res,scope){
																					res = Ext.decode(res);
																					if (res.code == 200) {
																						
																					}else{
																						Ext.Msg.alert('提示','更新失败');
																					}
																				},this,{userid:record.data.id,roleid:id})
																			}
																		})
																.show();
														win.add(grid)
														if(oid){
															
															    	var obj = grid.getSelectionModel();
																	obj.select(oid-1,true);
															
														}
													}, scope,null,uid)
										},this.up('panel'),{userid:record.data.id})
									
									}
								},
								{
									text : '修改密码',
									menuDisabled : true,
									xtype : 'actioncolumn',
									tooltip : '修改密码',
									align : 'center',
									icon : 'js/resources/images/cog_edit.png',
									handler : function(grid, rowIndex,
											colIndex, actionItem, event,
											record, row) {
										comedical.util.EventHandle.events
												.ShowForm(
														false,
														grid,
														record,
														'修改密码',
														'comedical.view.config.form.NewPasswordForm',
														addUserCallback)
									}
								},
								{
									text : '删除用户',
									width : 70,
									menuDisabled : true,
									xtype : 'actioncolumn',
									tooltip : '删除用户',
									align : 'center',
									icon : 'js/resources/images/icons/fam/delete.png'
								} ]
					}
				});