Ext.define('comedical.model.AdminPermission', {
	extend : 'Ext.data.Model',
	config : {
		fields : [ {
			name : 'id',
			type : 'string'
		}, {
			name : 'name',
			type : 'string'
		}, {
			name : 'leaf',
			type : 'string'
		}, {
			name : 'perid',
			type : 'string'
		}, {
			name : 'panel',
			type : 'string'
		} ],
		columns : [ {
			text : 'ID',
			dataIndex : 'id'
		}, {
			text : '标题',
			flex : 2,
			sortable : true,
			dataIndex : 'name'
		}, {
			text : '指定panel',
			flex : 1,
			dataIndex : 'panel',
			sortable : false
		} ]
	}
});