Ext.define('comedical.model.City', {
	extend : 'Ext.data.Model',
	config : {
		fields : [ {
			name : 'id',
			type : 'string'
		}, {
			name : 'name',
			type : 'string'
		}],
		columns : [ {
			text : 'ID',
			dataIndex : 'id'
		}, {
			text : '名称',
			flex : 2,
			sortable : true,
			dataIndex : 'name'
		}]
	}
});