package com.qianshui.co.medical.notification;

import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.qianshui.co.medical.db.Msg;


public class SendEngine {
	private String appkey = "54b5ff46fd98c57546000a23";
	private String appMasterSecret = "a9qvdgwl8o3osgdutb6eu8octlanjwki";
	private String timestamp = null;
	
	public SendEngine() {
		try {
			timestamp = Integer.toString((int)(System.currentTimeMillis() / 1000));
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	public void sendMsgBroadcast(Msg msg) throws Exception {
		AndroidBroadcast broadcast = new AndroidBroadcast();
		broadcast.setAppMasterSecret(appMasterSecret);
		broadcast.setPredefinedKeyValue("appkey", this.appkey);
		broadcast.setPredefinedKeyValue("timestamp", this.timestamp);
		broadcast.setPredefinedKeyValue("ticker", "重庆妇幼");
		broadcast.setPredefinedKeyValue("title",  "您收到新消息！");
		broadcast.setPredefinedKeyValue("text",   msg.getTitle());
		broadcast.setPredefinedKeyValue("after_open", "go_activity");
		broadcast.setPredefinedKeyValue("activity", "com.bigdata.medical.ui.MsgInfoActivity");
		
		broadcast.setPredefinedKeyValue("display_type", "notification");
		// TODO Set 'production_mode' to 'false' if it's a test device. 
		// For how to register a test device, please see the developer doc.
		broadcast.setPredefinedKeyValue("production_mode", "true");
		// Set customized fields
		broadcast.setExtraField("msg", String.valueOf(msg.getId()));
		broadcast.send();
	}
	
	

	

}
