package com.qianshui.co.medical.db;

/**
 * Classid entity. @author MyEclipse Persistence Tools
 */

public class Classid implements java.io.Serializable {

	// Fields

	private Integer id;
	private String classname;
	private String keyword;
	private Integer record;
	private Integer length;

	// Constructors

	/** default constructor */
	public Classid() {
	}

	/** full constructor */
	public Classid(String classname, String keyword, Integer record,
			Integer length) {
		this.classname = classname;
		this.keyword = keyword;
		this.record = record;
		this.length = length;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClassname() {
		return this.classname;
	}

	public void setClassname(String classname) {
		this.classname = classname;
	}

	public String getKeyword() {
		return this.keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Integer getRecord() {
		return this.record;
	}

	public void setRecord(Integer record) {
		this.record = record;
	}

	public Integer getLength() {
		return this.length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

}