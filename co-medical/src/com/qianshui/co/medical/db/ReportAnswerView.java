package com.qianshui.co.medical.db;

import java.sql.Timestamp;

/**
 * ReportAnswerViewId entity. @author MyEclipse Persistence Tools
 */

public class ReportAnswerView implements java.io.Serializable {

	// Fields

	private String id;
	private String reportid;
	private String content;
	private String adminUserid;
	private Timestamp createdate;
	private String answerPic;
	private String username;

	// Constructors

	/** default constructor */
	public ReportAnswerView() {
	}

	/** minimal constructor */
	public ReportAnswerView(String id) {
		this.id = id;
	}

	/** full constructor */
	public ReportAnswerView(String id, String reportid, String content,
			String adminUserid, Timestamp createdate, String answerPic,
			String username) {
		this.id = id;
		this.reportid = reportid;
		this.content = content;
		this.adminUserid = adminUserid;
		this.createdate = createdate;
		this.answerPic = answerPic;
		this.username = username;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getReportid() {
		return this.reportid;
	}

	public void setReportid(String reportid) {
		this.reportid = reportid;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAdminUserid() {
		return this.adminUserid;
	}

	public void setAdminUserid(String adminUserid) {
		this.adminUserid = adminUserid;
	}

	public Timestamp getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Timestamp createdate) {
		this.createdate = createdate;
	}

	public String getAnswerPic() {
		return this.answerPic;
	}

	public void setAnswerPic(String answerPic) {
		this.answerPic = answerPic;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ReportAnswerView))
			return false;
		ReportAnswerView castOther = (ReportAnswerView) other;

		return ((this.getId() == castOther.getId()) || (this.getId() != null
				&& castOther.getId() != null && this.getId().equals(
				castOther.getId())))
				&& ((this.getReportid() == castOther.getReportid()) || (this
						.getReportid() != null
						&& castOther.getReportid() != null && this
						.getReportid().equals(castOther.getReportid())))
				&& ((this.getContent() == castOther.getContent()) || (this
						.getContent() != null && castOther.getContent() != null && this
						.getContent().equals(castOther.getContent())))
				&& ((this.getAdminUserid() == castOther.getAdminUserid()) || (this
						.getAdminUserid() != null
						&& castOther.getAdminUserid() != null && this
						.getAdminUserid().equals(castOther.getAdminUserid())))
				&& ((this.getCreatedate() == castOther.getCreatedate()) || (this
						.getCreatedate() != null
						&& castOther.getCreatedate() != null && this
						.getCreatedate().equals(castOther.getCreatedate())))
				&& ((this.getAnswerPic() == castOther.getAnswerPic()) || (this
						.getAnswerPic() != null
						&& castOther.getAnswerPic() != null && this
						.getAnswerPic().equals(castOther.getAnswerPic())))
				&& ((this.getUsername() == castOther.getUsername()) || (this
						.getUsername() != null
						&& castOther.getUsername() != null && this
						.getUsername().equals(castOther.getUsername())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (getId() == null ? 0 : this.getId().hashCode());
		result = 37 * result
				+ (getReportid() == null ? 0 : this.getReportid().hashCode());
		result = 37 * result
				+ (getContent() == null ? 0 : this.getContent().hashCode());
		result = 37
				* result
				+ (getAdminUserid() == null ? 0 : this.getAdminUserid()
						.hashCode());
		result = 37
				* result
				+ (getCreatedate() == null ? 0 : this.getCreatedate()
						.hashCode());
		result = 37 * result
				+ (getAnswerPic() == null ? 0 : this.getAnswerPic().hashCode());
		result = 37 * result
				+ (getUsername() == null ? 0 : this.getUsername().hashCode());
		return result;
	}

}