package com.qianshui.co.medical.db;

/**
 * LnkRoleUser entity. @author MyEclipse Persistence Tools
 */

public class LnkRoleUser implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer roleid;
	private Integer userid;

	// Constructors

	/** default constructor */
	public LnkRoleUser() {
	}

	/** full constructor */
	public LnkRoleUser(Integer roleid, Integer userid) {
		this.roleid = roleid;
		this.userid = userid;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRoleid() {
		return this.roleid;
	}

	public void setRoleid(Integer roleid) {
		this.roleid = roleid;
	}

	public Integer getUserid() {
		return this.userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

}