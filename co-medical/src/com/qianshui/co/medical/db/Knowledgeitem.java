package com.qianshui.co.medical.db;

/**
 * Knowledgeitem entity. @author MyEclipse Persistence Tools
 */

public class Knowledgeitem implements java.io.Serializable {

	// Fields

	private String id;
	private String name;
	private String info;
	private String parentid;

	// Constructors

	/** default constructor */
	public Knowledgeitem() {
	}

	/** minimal constructor */
	public Knowledgeitem(String id) {
		this.id = id;
	}

	/** full constructor */
	public Knowledgeitem(String id, String name, String info, String parentid) {
		this.id = id;
		this.name = name;
		this.info = info;
		this.parentid = parentid;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getParentid() {
		return this.parentid;
	}

	public void setParentid(String parentid) {
		this.parentid = parentid;
	}

}