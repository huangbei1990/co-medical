package com.qianshui.co.medical.db;

/**
 * Area entity. @author MyEclipse Persistence Tools
 */

public class Area implements java.io.Serializable {

	// Fields

	private String id;
	private String name;
	private String cityid;

	// Constructors

	/** default constructor */
	public Area() {
	}

	/** full constructor */
	public Area(String name, String cityid) {
		this.name = name;
		this.cityid = cityid;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCityid() {
		return this.cityid;
	}

	public void setCityid(String cityid) {
		this.cityid = cityid;
	}

}