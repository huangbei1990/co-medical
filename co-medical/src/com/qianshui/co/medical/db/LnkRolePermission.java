package com.qianshui.co.medical.db;

/**
 * LnkRolePermission entity. @author MyEclipse Persistence Tools
 */

public class LnkRolePermission implements java.io.Serializable {

	// Fields

	private Integer id;
	private String permissionid;
	private Integer roleid;

	// Constructors

	/** default constructor */
	public LnkRolePermission() {
	}

	/** full constructor */
	public LnkRolePermission(String permissionid, Integer roleid) {
		this.permissionid = permissionid;
		this.roleid = roleid;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPermissionid() {
		return this.permissionid;
	}

	public void setPermissionid(String permissionid) {
		this.permissionid = permissionid;
	}

	public Integer getRoleid() {
		return this.roleid;
	}

	public void setRoleid(Integer roleid) {
		this.roleid = roleid;
	}

}