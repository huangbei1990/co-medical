package com.qianshui.co.medical.db;

import java.sql.Timestamp;

/**
 * Forum entity. @author MyEclipse Persistence Tools
 */

public class Forum implements java.io.Serializable {

	// Fields

	private String id;
	private String userid;
	private String title;
	private String content;
	private String pic;
	private Timestamp createdate;
	private Integer answercount;

	// Constructors

	/** default constructor */
	public Forum() {
	}

	/** minimal constructor */
	public Forum(String id) {
		this.id = id;
	}

	/** full constructor */
	public Forum(String id, String userid, String title, String content,
			String pic, Timestamp createdate, Integer answercount) {
		this.id = id;
		this.userid = userid;
		this.title = title;
		this.content = content;
		this.pic = pic;
		this.createdate = createdate;
		this.answercount = answercount;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserid() {
		return this.userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPic() {
		return this.pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Timestamp getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Timestamp createdate) {
		this.createdate = createdate;
	}

	public Integer getAnswercount() {
		return this.answercount;
	}

	public void setAnswercount(Integer answercount) {
		this.answercount = answercount;
	}

}