package com.qianshui.co.medical.db;

import java.util.List;


public interface IHospitalDAO extends IBaseHibernateDAO<Hospital> {

	public List<Hospital> getHospitalByDengjiCity(String Dengji,String cityid);
	
	public List<Hospital> getHospitalAllByCity(String cityid);
}
