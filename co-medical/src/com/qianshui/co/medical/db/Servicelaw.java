package com.qianshui.co.medical.db;

/**
 * Servicelaw entity. @author MyEclipse Persistence Tools
 */

public class Servicelaw implements java.io.Serializable {

	// Fields

	private Integer idservicelaw;
	private String lawcontent;

	// Constructors

	/** default constructor */
	public Servicelaw() {
	}

	/** full constructor */
	public Servicelaw(String lawcontent) {
		this.lawcontent = lawcontent;
	}

	// Property accessors

	public Integer getIdservicelaw() {
		return this.idservicelaw;
	}

	public void setIdservicelaw(Integer idservicelaw) {
		this.idservicelaw = idservicelaw;
	}

	public String getLawcontent() {
		return this.lawcontent;
	}

	public void setLawcontent(String lawcontent) {
		this.lawcontent = lawcontent;
	}

}