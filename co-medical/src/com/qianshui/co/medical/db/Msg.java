package com.qianshui.co.medical.db;

import java.sql.Timestamp;

/**
 * Msg entity. @author MyEclipse Persistence Tools
 */

public class Msg implements java.io.Serializable {

	// Fields

	private Long id;
	private String title;
	private String content;
	private Timestamp createdate;
	private Integer issend;
	private String pic;

	// Constructors

	/** default constructor */
	public Msg() {
	}

	/** minimal constructor */
	public Msg(Long id) {
		this.id = id;
	}

	/** full constructor */
	public Msg(Long id, String title, String content, Timestamp createdate,
			Integer issend, String pic) {
		this.id = id;
		this.title = title;
		this.content = content;
		this.createdate = createdate;
		this.issend = issend;
		this.pic = pic;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Timestamp getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Timestamp createdate) {
		this.createdate = createdate;
	}

	public Integer getIssend() {
		return this.issend;
	}

	public void setIssend(Integer issend) {
		this.issend = issend;
	}

	public String getPic() {
		return this.pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

}