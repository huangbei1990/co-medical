package com.qianshui.co.medical.db;

import java.util.List;


public interface IArrangeDAO extends IBaseHibernateDAO<Arrange> {

	public List<DocArrange> QueryArrangeByDoctor(String doctorid, String startdate,String enddate);

}
