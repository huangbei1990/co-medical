package com.qianshui.co.medical.db;

import java.sql.Timestamp;

/**
 * News entity. @author MyEclipse Persistence Tools
 */

public class News implements java.io.Serializable {

	// Fields

	private String id;
	private String title;
	private String content;
	private String pic;
	private Integer top;
	private Timestamp createdate;
	private String departmentid;
	private String subview;

	// Constructors

	/** default constructor */
	public News() {
	}

	/** full constructor */
	public News(String id,String title, String content, String pic, Integer top,
			Timestamp createdate, String departmentid, String subview) {
		this.id = id;
		this.title = title;
		this.content = content;
		this.pic = pic;
		this.top = top;
		this.createdate = createdate;
		this.departmentid = departmentid;
		this.subview = subview;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPic() {
		return this.pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Integer getTop() {
		return this.top;
	}

	public void setTop(Integer top) {
		this.top = top;
	}

	public Timestamp getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Timestamp createdate) {
		this.createdate = createdate;
	}

	public String getDepartmentid() {
		return this.departmentid;
	}

	public void setDepartmentid(String departmentid) {
		this.departmentid = departmentid;
	}

	public String getSubview() {
		return this.subview;
	}

	public void setSubview(String subview) {
		this.subview = subview;
	}

}