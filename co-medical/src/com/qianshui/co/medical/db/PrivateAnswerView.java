package com.qianshui.co.medical.db;

import java.sql.Timestamp;

/**
 * PrivateAnswerViewId entity. @author MyEclipse Persistence Tools
 */

public class PrivateAnswerView implements java.io.Serializable {

	// Fields

	private String id;
	private String questionid;
	private String content;
	private String adminUserid;
	private Timestamp createdate;
	private String username;
	private String answer_pic;

	// Constructors

	/** default constructor */
	public PrivateAnswerView() {
	}

	/** minimal constructor */
	public PrivateAnswerView(String id) {
		this.id = id;
	}

	/** full constructor */
	public PrivateAnswerView(String id, String questionid, String content,
			String adminUserid, Timestamp createdate, String username) {
		this.id = id;
		this.questionid = questionid;
		this.content = content;
		this.adminUserid = adminUserid;
		this.createdate = createdate;
		this.username = username;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getQuestionid() {
		return this.questionid;
	}

	public void setQuestionid(String questionid) {
		this.questionid = questionid;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAdminUserid() {
		return this.adminUserid;
	}

	public void setAdminUserid(String adminUserid) {
		this.adminUserid = adminUserid;
	}

	public Timestamp getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Timestamp createdate) {
		this.createdate = createdate;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getAnswer_pic() {
		return answer_pic;
	}

	public void setAnswer_pic(String answer_pic) {
		this.answer_pic = answer_pic;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof PrivateAnswerView))
			return false;
		PrivateAnswerView castOther = (PrivateAnswerView) other;

		return ((this.getId() == castOther.getId()) || (this.getId() != null
				&& castOther.getId() != null && this.getId().equals(
				castOther.getId())))
				&& ((this.getQuestionid() == castOther.getQuestionid()) || (this
						.getQuestionid() != null
						&& castOther.getQuestionid() != null && this
						.getQuestionid().equals(castOther.getQuestionid())))
				&& ((this.getContent() == castOther.getContent()) || (this
						.getContent() != null && castOther.getContent() != null && this
						.getContent().equals(castOther.getContent())))
				&& ((this.getAdminUserid() == castOther.getAdminUserid()) || (this
						.getAdminUserid() != null
						&& castOther.getAdminUserid() != null && this
						.getAdminUserid().equals(castOther.getAdminUserid())))
				&& ((this.getCreatedate() == castOther.getCreatedate()) || (this
						.getCreatedate() != null
						&& castOther.getCreatedate() != null && this
						.getCreatedate().equals(castOther.getCreatedate())))
				&& ((this.getUsername() == castOther.getUsername()) || (this
						.getUsername() != null
						&& castOther.getUsername() != null && this
						.getUsername().equals(castOther.getUsername())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (getId() == null ? 0 : this.getId().hashCode());
		result = 37
				* result
				+ (getQuestionid() == null ? 0 : this.getQuestionid()
						.hashCode());
		result = 37 * result
				+ (getContent() == null ? 0 : this.getContent().hashCode());
		result = 37
				* result
				+ (getAdminUserid() == null ? 0 : this.getAdminUserid()
						.hashCode());
		result = 37
				* result
				+ (getCreatedate() == null ? 0 : this.getCreatedate()
						.hashCode());
		result = 37 * result
				+ (getUsername() == null ? 0 : this.getUsername().hashCode());
		return result;
	}

}