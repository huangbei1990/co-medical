package com.qianshui.co.medical.db;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * Hospital entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.qianshui.co.medical.db.Hospital
 * @author MyEclipse Persistence Tools
 */
public class ArrangeDAO extends BaseHibernateDAO<Arrange> implements IArrangeDAO {
	ArrangeDAO(Class c) {
		super(c);
		// TODO Auto-generated constructor stub
	}

	private static final Logger log = LoggerFactory
			.getLogger(ArrangeDAO.class);

	@Override
	public List<DocArrange> QueryArrangeByDoctor(String doctorid,
			String startdate, String enddate) {
		// TODO Auto-generated method stub
		List<DocArrange> returnlist = new ArrayList<DocArrange>();
			DocArrange docarrange = null;
				List<Arrange> list2 = DaoFactory.getInstance().getArrange_dao()
						.query("select * from Arrange where doctorid=\""+doctorid+"\" and orderdate>='"+startdate+"' and orderdate<='"+enddate+"'");
				if(list2==null){
					docarrange= new DocArrange();
				}else{
					docarrange = getDocArrange(list2);
				}
				docarrange.setDocid(doctorid);
//				docarrange.setName(doc.getName());
//				docarrange.setMoney(doc.getMoney());
				returnlist.add(docarrange);		
				
				return returnlist;	
	}
	
	
	private DocArrange getDocArrange(List<Arrange> list){
		if(list ==null){
			return null;
		}else{
		DocArrange docarrange = new DocArrange();
		Calendar c = Calendar.getInstance();
		for(Arrange arr:list){							
				c.setTime(arr.getOrderdate());
				int week = c.get(Calendar.DAY_OF_WEEK);
				int type = arr.getType();
				String period = arr.getPeriod();
				changeDocArrangeWeek(docarrange,type,week,period);			
		}		
		return docarrange;	 
		}
		
	}
	
	private void changeDocArrangeWeek(DocArrange doc,int type,int week,String period){
		switch(week){		
		case 1:
			if(type==0){
			doc.setSunday(0);
			}else if(type==1){
				doc.setSunday(1);
			}else if(type==2){
				doc.setSunday(2);
			}
			doc.setSunPeriod(period);
			break;
		case 2:
			if(type==0){
				doc.setMonday(0);
				}else if(type==1){
					doc.setMonday(1);
				}else if(type==2){
					doc.setMonday(2);
				}
			doc.setMonPeriod(period);
				break;			
		case 3:
			if(type==0){
				doc.setTuesday(0);
				}else if(type==1){
					doc.setTuesday(1);
				}else if(type==2){
					doc.setTuesday(2);
				}
			doc.setTuePeriod(period);
				break;		
		case 4:
			if(type==0){
				doc.setWednesday(0);
				}else if(type==1){
					doc.setWednesday(1);
				}else if(type==2){
					doc.setWednesday(2);
				}
			doc.setWedPeriod(period);
				break;
		case 5:
			if(type==0){
				doc.setThursday(0);
				}else if(type==1){
					doc.setThursday(1);
				}else if(type==2){
					doc.setThursday(2);
				}
			doc.setThuPeriod(period);
				break;
		case 6:
			if(type==0){
				doc.setFriday(0);
				}else if(type==1){
					doc.setFriday(1);
				}else if(type==2){
					doc.setFriday(2);
				}
			doc.setFriPeriod(period);
				break;
		case 7:
			if(type==0){
				doc.setSaturday(0);
				}else if(type==1){
					doc.setSaturday(1);
				}else if(type==2){
					doc.setSaturday(2);
				}
			doc.setSatPeriod(period);
				break;
		}
	}



}