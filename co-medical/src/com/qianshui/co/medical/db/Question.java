package com.qianshui.co.medical.db;

import java.sql.Timestamp;

/**
 * Question entity. @author MyEclipse Persistence Tools
 */

public class Question implements java.io.Serializable {

	// Fields

	private String id;
	private String userid;
	private String title;
	private String content;
	private String pic;
	private Timestamp createdate;
	private Integer private_;
	private String hospitalid;
	private Integer answercount;
	private String departmentid;
	private String doctorid;

	// Constructors

	/** default constructor */
	public Question() {
	}

	/** minimal constructor */
	public Question(String id) {
		this.id = id;
	}

	/** full constructor */
	public Question(String id, String userid, String title, String content,
			String pic, Timestamp createdate, Integer private_,
			String hospitalid, Integer answercount, String departmentid,
			String doctorid) {
		this.id = id;
		this.userid = userid;
		this.title = title;
		this.content = content;
		this.pic = pic;
		this.createdate = createdate;
		this.private_ = private_;
		this.hospitalid = hospitalid;
		this.answercount = answercount;
		this.departmentid = departmentid;
		this.doctorid = doctorid;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserid() {
		return this.userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPic() {
		return this.pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Timestamp getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Timestamp createdate) {
		this.createdate = createdate;
	}

	public Integer getPrivate_() {
		return this.private_;
	}

	public void setPrivate_(Integer private_) {
		this.private_ = private_;
	}

	public String getHospitalid() {
		return this.hospitalid;
	}

	public void setHospitalid(String hospitalid) {
		this.hospitalid = hospitalid;
	}

	public Integer getAnswercount() {
		return this.answercount;
	}

	public void setAnswercount(Integer answercount) {
		this.answercount = answercount;
	}

	public String getDepartmentid() {
		return this.departmentid;
	}

	public void setDepartmentid(String departmentid) {
		this.departmentid = departmentid;
	}

	public String getDoctorid() {
		return this.doctorid;
	}

	public void setDoctorid(String doctorid) {
		this.doctorid = doctorid;
	}

}