package com.qianshui.co.medical.db;

import java.util.ArrayList;

/**
 * Knowledgeclass entity. @author MyEclipse Persistence Tools
 */

public class Knowledgeclass implements java.io.Serializable {

	// Fields

	private String id;
	private String name;
	private String pic;
	private String parentid;
	private Integer leaf;
	

	private ArrayList<Knowledgeclass> children= new ArrayList<Knowledgeclass>();
	// Constructors

	/** default constructor */
	public Knowledgeclass() {
	}

	/** minimal constructor */
	public Knowledgeclass(String id) {
		this.id = id;
	}

	/** full constructor */
	public Knowledgeclass(String id, String name, String pic, String parentid) {
		this.id = id;
		this.name = name;
		this.pic = pic;
		this.parentid = parentid;
	}

	// Property accessors

	public void setChildren(Knowledgeclass node){
		if(this.id.equals(node.getParentid()) && !this.children.contains(node)){	
			this.children.add(node);
		}	
	}
	public ArrayList<Knowledgeclass> getChildren(){
		return children;
	}
	
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPic() {
		return this.pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getParentid() {
		return this.parentid;
	}

	public void setParentid(String parentid) {
		this.parentid = parentid;
	}
	public Integer getLeaf() {
		return leaf;
	}

	public void setLeaf(Integer leaf) {
		this.leaf = leaf;
	}
}