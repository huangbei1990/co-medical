package com.qianshui.co.medical.db;

/**
 * Userbmi entity. @author MyEclipse Persistence Tools
 */

public class Userbmi implements java.io.Serializable {

	// Fields

	private Integer id;
	private String userid;
	private Double weight;
	private Integer weeknum;

	// Constructors

	/** default constructor */
	public Userbmi() {
	}

	/** minimal constructor */
	public Userbmi(String userid) {
		this.userid = userid;
	}

	/** full constructor */
	public Userbmi(String userid, Double weight, Integer weeknum) {
		this.userid = userid;
		this.weight = weight;
		this.weeknum = weeknum;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserid() {
		return this.userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public Double getWeight() {
		return this.weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Integer getWeeknum() {
		return this.weeknum;
	}

	public void setWeeknum(Integer weeknum) {
		this.weeknum = weeknum;
	}

}