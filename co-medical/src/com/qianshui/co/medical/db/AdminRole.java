package com.qianshui.co.medical.db;

/**
 * AdminRole entity. @author MyEclipse Persistence Tools
 */

public class AdminRole implements java.io.Serializable {

	// Fields

	private Integer id;
	private String name;
	private Integer preid;

	// Constructors

	/** default constructor */
	public AdminRole() {
	}

	/** full constructor */
	public AdminRole(String name, Integer preid) {
		this.name = name;
		this.preid = preid;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getPreid() {
		return this.preid;
	}

	public void setPreid(Integer preid) {
		this.preid = preid;
	}

}