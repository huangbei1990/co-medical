package com.qianshui.co.medical.db;

import java.util.List;



public interface IClassidDAO extends IBaseHibernateDAO<Classid> {

	Classid getClassidByClassName(String ClassName);
}
