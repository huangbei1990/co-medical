package com.qianshui.co.medical.db;

import java.sql.Timestamp;

/**
 * Trace entity. @author MyEclipse Persistence Tools
 */

public class Trace implements java.io.Serializable {

	// Fields

	private String id;
	private String userid;
	private Timestamp date;
	private Double weight;
	private Double temperature;
	private Integer ovulate;

	// Constructors

	/** default constructor */
	public Trace() {
	}

	/** full constructor */
	public Trace(String userid, Timestamp date, Double weight,
			Double temperature, Integer ovulate) {
		this.userid = userid;
		this.date = date;
		this.weight = weight;
		this.temperature = temperature;
		this.ovulate = ovulate;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserid() {
		return this.userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public Timestamp getDate() {
		return this.date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public Double getWeight() {
		return this.weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getTemperature() {
		return this.temperature;
	}

	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}

	public Integer getOvulate() {
		return this.ovulate;
	}

	public void setOvulate(Integer ovulate) {
		this.ovulate = ovulate;
	}

}