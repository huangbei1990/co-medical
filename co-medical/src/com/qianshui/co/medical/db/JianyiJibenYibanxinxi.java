package com.qianshui.co.medical.db;

/**
 * JianyiJibenYibanxinxi entity. @author MyEclipse Persistence Tools
 */

public class JianyiJibenYibanxinxi implements java.io.Serializable {

	// Fields

	private Integer id;
	private String wmMrn;
	private String wmName;
	private String wmAge;
	private String wmIsmarried;
	private String wmJob;
	private String wmAvgscore;
	private String wmEducation;
	private String wmCraft;
	private String wmCompany;
	private String wmMobilePhone;
	private String wmCardno;
	private String wmBlh;
	private String wmPhone;
	private String wmIdentityno;
	private String wmConstant;
	private String wmAddressPro;
	private String wmAddressCity;
	private String wmAddressArea;
	private String wmAddress;
	private String wmNowaddressPro;
	private String wmNowaddressCity;
	private String wmNowaddressArea;
	private String wmNowaddress;
	private String MName;
	private String MAge;
	private String MIsmarried;
	private String MJob;
	private String MEducation;
	private String MCraft;
	private String MCompany;
	private String MPhone;
	private Double wmTemp;
	private Integer wmPulse;
	private Integer wmBreathe;
	private Integer wmBloodHigh;
	private Integer wmBloodLow;
	private Integer wmHigh;
	private Double wmWeight;
	private Double wmBmi;
	private String menstrualNew;
	private String menstrualPeriod;
	private String menstrualCycle;
	private String menstrualIsrule;
	private String menstrualLast;
	private Short marriedAge;
	private Short gravidity;
	private Short production;
	private String isInbreeding;
	private String MCounty;
	private String MNation;
	private String wmHealth;
	private Short birthAge;
	private Byte birthCount;
	private String birthSex;
	private String birthRoute;
	private String perinatal;
	private String abortionAge;
	private String abortionReason;
	private String hpi;
	private String fmhSmoke;
	private String fmhDrink;
	private String fmhDisease;
	private String fmhOperation;
	private String fmhAllergic;
	private String fmhAllergens;
	private String familyhistoryWm;
	private String familyhistoryM;
	private String operatCode;
	private String operatName;
	private String operateTime;
	private String wmRiskfactors;
	private String abortionRemark;
	private String gaoweiRemark;
	private String babyWeight;
	private String isJixing;
	private String birthSex1;
	private String natureFenmian;

	// Constructors

	/** default constructor */
	public JianyiJibenYibanxinxi() {
	}

	/** minimal constructor */
	public JianyiJibenYibanxinxi(Integer id, String wmMrn) {
		this.id = id;
		this.wmMrn = wmMrn;
	}

	/** full constructor */
	public JianyiJibenYibanxinxi(Integer id, String wmMrn, String wmName,
			String wmAge, String wmIsmarried, String wmJob, String wmAvgscore,
			String wmEducation, String wmCraft, String wmCompany,
			String wmMobilePhone, String wmCardno, String wmBlh,
			String wmPhone, String wmIdentityno, String wmConstant,
			String wmAddressPro, String wmAddressCity, String wmAddressArea,
			String wmAddress, String wmNowaddressPro, String wmNowaddressCity,
			String wmNowaddressArea, String wmNowaddress, String MName,
			String MAge, String MIsmarried, String MJob, String MEducation,
			String MCraft, String MCompany, String MPhone, Double wmTemp,
			Integer wmPulse, Integer wmBreathe, Integer wmBloodHigh,
			Integer wmBloodLow, Integer wmHigh, Double wmWeight, Double wmBmi,
			String menstrualNew, String menstrualPeriod, String menstrualCycle,
			String menstrualIsrule, String menstrualLast, Short marriedAge,
			Short gravidity, Short production, String isInbreeding,
			String MCounty, String MNation, String wmHealth, Short birthAge,
			Byte birthCount, String birthSex, String birthRoute,
			String perinatal, String abortionAge, String abortionReason,
			String hpi, String fmhSmoke, String fmhDrink, String fmhDisease,
			String fmhOperation, String fmhAllergic, String fmhAllergens,
			String familyhistoryWm, String familyhistoryM, String operatCode,
			String operatName, String operateTime, String wmRiskfactors,
			String abortionRemark, String gaoweiRemark, String babyWeight,
			String isJixing, String birthSex1, String natureFenmian) {
		this.id = id;
		this.wmMrn = wmMrn;
		this.wmName = wmName;
		this.wmAge = wmAge;
		this.wmIsmarried = wmIsmarried;
		this.wmJob = wmJob;
		this.wmAvgscore = wmAvgscore;
		this.wmEducation = wmEducation;
		this.wmCraft = wmCraft;
		this.wmCompany = wmCompany;
		this.wmMobilePhone = wmMobilePhone;
		this.wmCardno = wmCardno;
		this.wmBlh = wmBlh;
		this.wmPhone = wmPhone;
		this.wmIdentityno = wmIdentityno;
		this.wmConstant = wmConstant;
		this.wmAddressPro = wmAddressPro;
		this.wmAddressCity = wmAddressCity;
		this.wmAddressArea = wmAddressArea;
		this.wmAddress = wmAddress;
		this.wmNowaddressPro = wmNowaddressPro;
		this.wmNowaddressCity = wmNowaddressCity;
		this.wmNowaddressArea = wmNowaddressArea;
		this.wmNowaddress = wmNowaddress;
		this.MName = MName;
		this.MAge = MAge;
		this.MIsmarried = MIsmarried;
		this.MJob = MJob;
		this.MEducation = MEducation;
		this.MCraft = MCraft;
		this.MCompany = MCompany;
		this.MPhone = MPhone;
		this.wmTemp = wmTemp;
		this.wmPulse = wmPulse;
		this.wmBreathe = wmBreathe;
		this.wmBloodHigh = wmBloodHigh;
		this.wmBloodLow = wmBloodLow;
		this.wmHigh = wmHigh;
		this.wmWeight = wmWeight;
		this.wmBmi = wmBmi;
		this.menstrualNew = menstrualNew;
		this.menstrualPeriod = menstrualPeriod;
		this.menstrualCycle = menstrualCycle;
		this.menstrualIsrule = menstrualIsrule;
		this.menstrualLast = menstrualLast;
		this.marriedAge = marriedAge;
		this.gravidity = gravidity;
		this.production = production;
		this.isInbreeding = isInbreeding;
		this.MCounty = MCounty;
		this.MNation = MNation;
		this.wmHealth = wmHealth;
		this.birthAge = birthAge;
		this.birthCount = birthCount;
		this.birthSex = birthSex;
		this.birthRoute = birthRoute;
		this.perinatal = perinatal;
		this.abortionAge = abortionAge;
		this.abortionReason = abortionReason;
		this.hpi = hpi;
		this.fmhSmoke = fmhSmoke;
		this.fmhDrink = fmhDrink;
		this.fmhDisease = fmhDisease;
		this.fmhOperation = fmhOperation;
		this.fmhAllergic = fmhAllergic;
		this.fmhAllergens = fmhAllergens;
		this.familyhistoryWm = familyhistoryWm;
		this.familyhistoryM = familyhistoryM;
		this.operatCode = operatCode;
		this.operatName = operatName;
		this.operateTime = operateTime;
		this.wmRiskfactors = wmRiskfactors;
		this.abortionRemark = abortionRemark;
		this.gaoweiRemark = gaoweiRemark;
		this.babyWeight = babyWeight;
		this.isJixing = isJixing;
		this.birthSex1 = birthSex1;
		this.natureFenmian = natureFenmian;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getWmMrn() {
		return this.wmMrn;
	}

	public void setWmMrn(String wmMrn) {
		this.wmMrn = wmMrn;
	}

	public String getWmName() {
		return this.wmName;
	}

	public void setWmName(String wmName) {
		this.wmName = wmName;
	}

	public String getWmAge() {
		return this.wmAge;
	}

	public void setWmAge(String wmAge) {
		this.wmAge = wmAge;
	}

	public String getWmIsmarried() {
		return this.wmIsmarried;
	}

	public void setWmIsmarried(String wmIsmarried) {
		this.wmIsmarried = wmIsmarried;
	}

	public String getWmJob() {
		return this.wmJob;
	}

	public void setWmJob(String wmJob) {
		this.wmJob = wmJob;
	}

	public String getWmAvgscore() {
		return this.wmAvgscore;
	}

	public void setWmAvgscore(String wmAvgscore) {
		this.wmAvgscore = wmAvgscore;
	}

	public String getWmEducation() {
		return this.wmEducation;
	}

	public void setWmEducation(String wmEducation) {
		this.wmEducation = wmEducation;
	}

	public String getWmCraft() {
		return this.wmCraft;
	}

	public void setWmCraft(String wmCraft) {
		this.wmCraft = wmCraft;
	}

	public String getWmCompany() {
		return this.wmCompany;
	}

	public void setWmCompany(String wmCompany) {
		this.wmCompany = wmCompany;
	}

	public String getWmMobilePhone() {
		return this.wmMobilePhone;
	}

	public void setWmMobilePhone(String wmMobilePhone) {
		this.wmMobilePhone = wmMobilePhone;
	}

	public String getWmCardno() {
		return this.wmCardno;
	}

	public void setWmCardno(String wmCardno) {
		this.wmCardno = wmCardno;
	}

	public String getWmBlh() {
		return this.wmBlh;
	}

	public void setWmBlh(String wmBlh) {
		this.wmBlh = wmBlh;
	}

	public String getWmPhone() {
		return this.wmPhone;
	}

	public void setWmPhone(String wmPhone) {
		this.wmPhone = wmPhone;
	}

	public String getWmIdentityno() {
		return this.wmIdentityno;
	}

	public void setWmIdentityno(String wmIdentityno) {
		this.wmIdentityno = wmIdentityno;
	}

	public String getWmConstant() {
		return this.wmConstant;
	}

	public void setWmConstant(String wmConstant) {
		this.wmConstant = wmConstant;
	}

	public String getWmAddressPro() {
		return this.wmAddressPro;
	}

	public void setWmAddressPro(String wmAddressPro) {
		this.wmAddressPro = wmAddressPro;
	}

	public String getWmAddressCity() {
		return this.wmAddressCity;
	}

	public void setWmAddressCity(String wmAddressCity) {
		this.wmAddressCity = wmAddressCity;
	}

	public String getWmAddressArea() {
		return this.wmAddressArea;
	}

	public void setWmAddressArea(String wmAddressArea) {
		this.wmAddressArea = wmAddressArea;
	}

	public String getWmAddress() {
		return this.wmAddress;
	}

	public void setWmAddress(String wmAddress) {
		this.wmAddress = wmAddress;
	}

	public String getWmNowaddressPro() {
		return this.wmNowaddressPro;
	}

	public void setWmNowaddressPro(String wmNowaddressPro) {
		this.wmNowaddressPro = wmNowaddressPro;
	}

	public String getWmNowaddressCity() {
		return this.wmNowaddressCity;
	}

	public void setWmNowaddressCity(String wmNowaddressCity) {
		this.wmNowaddressCity = wmNowaddressCity;
	}

	public String getWmNowaddressArea() {
		return this.wmNowaddressArea;
	}

	public void setWmNowaddressArea(String wmNowaddressArea) {
		this.wmNowaddressArea = wmNowaddressArea;
	}

	public String getWmNowaddress() {
		return this.wmNowaddress;
	}

	public void setWmNowaddress(String wmNowaddress) {
		this.wmNowaddress = wmNowaddress;
	}

	public String getMName() {
		return this.MName;
	}

	public void setMName(String MName) {
		this.MName = MName;
	}

	public String getMAge() {
		return this.MAge;
	}

	public void setMAge(String MAge) {
		this.MAge = MAge;
	}

	public String getMIsmarried() {
		return this.MIsmarried;
	}

	public void setMIsmarried(String MIsmarried) {
		this.MIsmarried = MIsmarried;
	}

	public String getMJob() {
		return this.MJob;
	}

	public void setMJob(String MJob) {
		this.MJob = MJob;
	}

	public String getMEducation() {
		return this.MEducation;
	}

	public void setMEducation(String MEducation) {
		this.MEducation = MEducation;
	}

	public String getMCraft() {
		return this.MCraft;
	}

	public void setMCraft(String MCraft) {
		this.MCraft = MCraft;
	}

	public String getMCompany() {
		return this.MCompany;
	}

	public void setMCompany(String MCompany) {
		this.MCompany = MCompany;
	}

	public String getMPhone() {
		return this.MPhone;
	}

	public void setMPhone(String MPhone) {
		this.MPhone = MPhone;
	}

	public Double getWmTemp() {
		return this.wmTemp;
	}

	public void setWmTemp(Double wmTemp) {
		this.wmTemp = wmTemp;
	}

	public Integer getWmPulse() {
		return this.wmPulse;
	}

	public void setWmPulse(Integer wmPulse) {
		this.wmPulse = wmPulse;
	}

	public Integer getWmBreathe() {
		return this.wmBreathe;
	}

	public void setWmBreathe(Integer wmBreathe) {
		this.wmBreathe = wmBreathe;
	}

	public Integer getWmBloodHigh() {
		return this.wmBloodHigh;
	}

	public void setWmBloodHigh(Integer wmBloodHigh) {
		this.wmBloodHigh = wmBloodHigh;
	}

	public Integer getWmBloodLow() {
		return this.wmBloodLow;
	}

	public void setWmBloodLow(Integer wmBloodLow) {
		this.wmBloodLow = wmBloodLow;
	}

	public Integer getWmHigh() {
		return this.wmHigh;
	}

	public void setWmHigh(Integer wmHigh) {
		this.wmHigh = wmHigh;
	}

	public Double getWmWeight() {
		return this.wmWeight;
	}

	public void setWmWeight(Double wmWeight) {
		this.wmWeight = wmWeight;
	}

	public Double getWmBmi() {
		return this.wmBmi;
	}

	public void setWmBmi(Double wmBmi) {
		this.wmBmi = wmBmi;
	}

	public String getMenstrualNew() {
		return this.menstrualNew;
	}

	public void setMenstrualNew(String menstrualNew) {
		this.menstrualNew = menstrualNew;
	}

	public String getMenstrualPeriod() {
		return this.menstrualPeriod;
	}

	public void setMenstrualPeriod(String menstrualPeriod) {
		this.menstrualPeriod = menstrualPeriod;
	}

	public String getMenstrualCycle() {
		return this.menstrualCycle;
	}

	public void setMenstrualCycle(String menstrualCycle) {
		this.menstrualCycle = menstrualCycle;
	}

	public String getMenstrualIsrule() {
		return this.menstrualIsrule;
	}

	public void setMenstrualIsrule(String menstrualIsrule) {
		this.menstrualIsrule = menstrualIsrule;
	}

	public String getMenstrualLast() {
		return this.menstrualLast;
	}

	public void setMenstrualLast(String menstrualLast) {
		this.menstrualLast = menstrualLast;
	}

	public Short getMarriedAge() {
		return this.marriedAge;
	}

	public void setMarriedAge(Short marriedAge) {
		this.marriedAge = marriedAge;
	}

	public Short getGravidity() {
		return this.gravidity;
	}

	public void setGravidity(Short gravidity) {
		this.gravidity = gravidity;
	}

	public Short getProduction() {
		return this.production;
	}

	public void setProduction(Short production) {
		this.production = production;
	}

	public String getIsInbreeding() {
		return this.isInbreeding;
	}

	public void setIsInbreeding(String isInbreeding) {
		this.isInbreeding = isInbreeding;
	}

	public String getMCounty() {
		return this.MCounty;
	}

	public void setMCounty(String MCounty) {
		this.MCounty = MCounty;
	}

	public String getMNation() {
		return this.MNation;
	}

	public void setMNation(String MNation) {
		this.MNation = MNation;
	}

	public String getWmHealth() {
		return this.wmHealth;
	}

	public void setWmHealth(String wmHealth) {
		this.wmHealth = wmHealth;
	}

	public Short getBirthAge() {
		return this.birthAge;
	}

	public void setBirthAge(Short birthAge) {
		this.birthAge = birthAge;
	}

	public Byte getBirthCount() {
		return this.birthCount;
	}

	public void setBirthCount(Byte birthCount) {
		this.birthCount = birthCount;
	}

	public String getBirthSex() {
		return this.birthSex;
	}

	public void setBirthSex(String birthSex) {
		this.birthSex = birthSex;
	}

	public String getBirthRoute() {
		return this.birthRoute;
	}

	public void setBirthRoute(String birthRoute) {
		this.birthRoute = birthRoute;
	}

	public String getPerinatal() {
		return this.perinatal;
	}

	public void setPerinatal(String perinatal) {
		this.perinatal = perinatal;
	}

	public String getAbortionAge() {
		return this.abortionAge;
	}

	public void setAbortionAge(String abortionAge) {
		this.abortionAge = abortionAge;
	}

	public String getAbortionReason() {
		return this.abortionReason;
	}

	public void setAbortionReason(String abortionReason) {
		this.abortionReason = abortionReason;
	}

	public String getHpi() {
		return this.hpi;
	}

	public void setHpi(String hpi) {
		this.hpi = hpi;
	}

	public String getFmhSmoke() {
		return this.fmhSmoke;
	}

	public void setFmhSmoke(String fmhSmoke) {
		this.fmhSmoke = fmhSmoke;
	}

	public String getFmhDrink() {
		return this.fmhDrink;
	}

	public void setFmhDrink(String fmhDrink) {
		this.fmhDrink = fmhDrink;
	}

	public String getFmhDisease() {
		return this.fmhDisease;
	}

	public void setFmhDisease(String fmhDisease) {
		this.fmhDisease = fmhDisease;
	}

	public String getFmhOperation() {
		return this.fmhOperation;
	}

	public void setFmhOperation(String fmhOperation) {
		this.fmhOperation = fmhOperation;
	}

	public String getFmhAllergic() {
		return this.fmhAllergic;
	}

	public void setFmhAllergic(String fmhAllergic) {
		this.fmhAllergic = fmhAllergic;
	}

	public String getFmhAllergens() {
		return this.fmhAllergens;
	}

	public void setFmhAllergens(String fmhAllergens) {
		this.fmhAllergens = fmhAllergens;
	}

	public String getFamilyhistoryWm() {
		return this.familyhistoryWm;
	}

	public void setFamilyhistoryWm(String familyhistoryWm) {
		this.familyhistoryWm = familyhistoryWm;
	}

	public String getFamilyhistoryM() {
		return this.familyhistoryM;
	}

	public void setFamilyhistoryM(String familyhistoryM) {
		this.familyhistoryM = familyhistoryM;
	}

	public String getOperatCode() {
		return this.operatCode;
	}

	public void setOperatCode(String operatCode) {
		this.operatCode = operatCode;
	}

	public String getOperatName() {
		return this.operatName;
	}

	public void setOperatName(String operatName) {
		this.operatName = operatName;
	}

	public String getOperateTime() {
		return this.operateTime;
	}

	public void setOperateTime(String operateTime) {
		this.operateTime = operateTime;
	}

	public String getWmRiskfactors() {
		return this.wmRiskfactors;
	}

	public void setWmRiskfactors(String wmRiskfactors) {
		this.wmRiskfactors = wmRiskfactors;
	}

	public String getAbortionRemark() {
		return this.abortionRemark;
	}

	public void setAbortionRemark(String abortionRemark) {
		this.abortionRemark = abortionRemark;
	}

	public String getGaoweiRemark() {
		return this.gaoweiRemark;
	}

	public void setGaoweiRemark(String gaoweiRemark) {
		this.gaoweiRemark = gaoweiRemark;
	}

	public String getBabyWeight() {
		return this.babyWeight;
	}

	public void setBabyWeight(String babyWeight) {
		this.babyWeight = babyWeight;
	}

	public String getIsJixing() {
		return this.isJixing;
	}

	public void setIsJixing(String isJixing) {
		this.isJixing = isJixing;
	}

	public String getBirthSex1() {
		return this.birthSex1;
	}

	public void setBirthSex1(String birthSex1) {
		this.birthSex1 = birthSex1;
	}

	public String getNatureFenmian() {
		return this.natureFenmian;
	}

	public void setNatureFenmian(String natureFenmian) {
		this.natureFenmian = natureFenmian;
	}

}