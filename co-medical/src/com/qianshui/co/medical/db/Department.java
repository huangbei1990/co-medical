package com.qianshui.co.medical.db;

/**
 * Department entity. @author MyEclipse Persistence Tools
 */

public class Department implements java.io.Serializable {

	// Fields

	private String id;
	private String name;
	private String introduce;
	private String hospitalid;
	private String website;
	private String tel;
	private String booktel;
	private String forenoontime;
	private String afternoontime;
	private String departmentcol;

	// Constructors

	/** default constructor */
	public Department() {
	}

	/** minimal constructor */
	public Department(String id) {
		this.id = id;
	}

	/** full constructor */
	public Department(String id, String name, String introduce,
			String hospitalid, String website, String tel, String booktel,
			String forenoontime, String afternoontime, String departmentcol) {
		this.id = id;
		this.name = name;
		this.introduce = introduce;
		this.hospitalid = hospitalid;
		this.website = website;
		this.tel = tel;
		this.booktel = booktel;
		this.forenoontime = forenoontime;
		this.afternoontime = afternoontime;
		this.departmentcol = departmentcol;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIntroduce() {
		return this.introduce;
	}

	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}

	public String getHospitalid() {
		return this.hospitalid;
	}

	public void setHospitalid(String hospitalid) {
		this.hospitalid = hospitalid;
	}

	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getTel() {
		return this.tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getBooktel() {
		return this.booktel;
	}

	public void setBooktel(String booktel) {
		this.booktel = booktel;
	}

	public String getForenoontime() {
		return this.forenoontime;
	}

	public void setForenoontime(String forenoontime) {
		this.forenoontime = forenoontime;
	}

	public String getAfternoontime() {
		return this.afternoontime;
	}

	public void setAfternoontime(String afternoontime) {
		this.afternoontime = afternoontime;
	}

	public String getDepartmentcol() {
		return this.departmentcol;
	}

	public void setDepartmentcol(String departmentcol) {
		this.departmentcol = departmentcol;
	}

}