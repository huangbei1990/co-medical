package com.qianshui.co.medical.db;

import java.sql.Timestamp;

/**
 * NormalAnswerViewId entity. @author MyEclipse Persistence Tools
 */

public class NormalAnswerView implements java.io.Serializable {

	// Fields

	private String id;
	private String questionid;
	private String content;
	private String userid;
	private Timestamp createdate;
	private String answerPic;
	private String username;

	// Constructors

	/** default constructor */
	public NormalAnswerView() {
	}

	/** minimal constructor */
	public NormalAnswerView(String id) {
		this.id = id;
	}

	/** full constructor */
	public NormalAnswerView(String id, String questionid, String content,
			String userid, Timestamp createdate, String answerPic,
			String username) {
		this.id = id;
		this.questionid = questionid;
		this.content = content;
		this.userid = userid;
		this.createdate = createdate;
		this.answerPic = answerPic;
		this.username = username;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getQuestionid() {
		return this.questionid;
	}

	public void setQuestionid(String questionid) {
		this.questionid = questionid;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUserid() {
		return this.userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public Timestamp getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Timestamp createdate) {
		this.createdate = createdate;
	}

	public String getAnswerPic() {
		return this.answerPic;
	}

	public void setAnswerPic(String answerPic) {
		this.answerPic = answerPic;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof NormalAnswerView))
			return false;
		NormalAnswerView castOther = (NormalAnswerView) other;

		return ((this.getId() == castOther.getId()) || (this.getId() != null
				&& castOther.getId() != null && this.getId().equals(
				castOther.getId())))
				&& ((this.getQuestionid() == castOther.getQuestionid()) || (this
						.getQuestionid() != null
						&& castOther.getQuestionid() != null && this
						.getQuestionid().equals(castOther.getQuestionid())))
				&& ((this.getContent() == castOther.getContent()) || (this
						.getContent() != null && castOther.getContent() != null && this
						.getContent().equals(castOther.getContent())))
				&& ((this.getUserid() == castOther.getUserid()) || (this
						.getUserid() != null && castOther.getUserid() != null && this
						.getUserid().equals(castOther.getUserid())))
				&& ((this.getCreatedate() == castOther.getCreatedate()) || (this
						.getCreatedate() != null
						&& castOther.getCreatedate() != null && this
						.getCreatedate().equals(castOther.getCreatedate())))
				&& ((this.getAnswerPic() == castOther.getAnswerPic()) || (this
						.getAnswerPic() != null
						&& castOther.getAnswerPic() != null && this
						.getAnswerPic().equals(castOther.getAnswerPic())))
				&& ((this.getUsername() == castOther.getUsername()) || (this
						.getUsername() != null
						&& castOther.getUsername() != null && this
						.getUsername().equals(castOther.getUsername())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (getId() == null ? 0 : this.getId().hashCode());
		result = 37
				* result
				+ (getQuestionid() == null ? 0 : this.getQuestionid()
						.hashCode());
		result = 37 * result
				+ (getContent() == null ? 0 : this.getContent().hashCode());
		result = 37 * result
				+ (getUserid() == null ? 0 : this.getUserid().hashCode());
		result = 37
				* result
				+ (getCreatedate() == null ? 0 : this.getCreatedate()
						.hashCode());
		result = 37 * result
				+ (getAnswerPic() == null ? 0 : this.getAnswerPic().hashCode());
		result = 37 * result
				+ (getUsername() == null ? 0 : this.getUsername().hashCode());
		return result;
	}

}