package com.qianshui.co.medical.db;



/**
 * Doctor entity. @author MyEclipse Persistence Tools
 */

public class Doctor  implements java.io.Serializable {


    // Fields    

     private String id;
     private String name;
     private String departmentid;
     private String positon;
     private String bookType;
     private String introduce;
     private String pic;
     private Double money;
     private String hospitalid;
     private Integer sort;


    // Constructors

    /** default constructor */
    public Doctor() {
    }

    
    /** full constructor */
    public Doctor(String name, String departmentid, String positon, String bookType, String introduce, String pic, Double money, String hospitalid, Integer sort) {
        this.name = name;
        this.departmentid = departmentid;
        this.positon = positon;
        this.bookType = bookType;
        this.introduce = introduce;
        this.pic = pic;
        this.money = money;
        this.hospitalid = hospitalid;
        this.sort = sort;
    }

   
    // Property accessors

    public String getId() {
        return this.id;
    }
    
    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public String getDepartmentid() {
        return this.departmentid;
    }
    
    public void setDepartmentid(String departmentid) {
        this.departmentid = departmentid;
    }

    public String getPositon() {
        return this.positon;
    }
    
    public void setPositon(String positon) {
        this.positon = positon;
    }

    public String getBookType() {
        return this.bookType;
    }
    
    public void setBookType(String bookType) {
        this.bookType = bookType;
    }

    public String getIntroduce() {
        return this.introduce;
    }
    
    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public String getPic() {
        return this.pic;
    }
    
    public void setPic(String pic) {
        this.pic = pic;
    }

    public Double getMoney() {
        return this.money;
    }
    
    public void setMoney(Double money) {
        this.money = money;
    }

    public String getHospitalid() {
        return this.hospitalid;
    }
    
    public void setHospitalid(String hospitalid) {
        this.hospitalid = hospitalid;
    }

    public Integer getSort() {
        return this.sort;
    }
    
    public void setSort(Integer sort) {
        this.sort = sort;
    }
   








}