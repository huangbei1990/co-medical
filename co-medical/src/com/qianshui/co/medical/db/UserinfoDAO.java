package com.qianshui.co.medical.db;

import java.util.List;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * Userinfo entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.qianshui.co.medical.db.Userinfo
 * @author MyEclipse Persistence Tools
 */
public class UserinfoDAO extends BaseHibernateDAO<Userinfo> implements IUserinfoDAO{
	UserinfoDAO(Class c) {
		super(c);
		// TODO Auto-generated constructor stub
	}

	private static final Logger log = LoggerFactory
			.getLogger(UserinfoDAO.class);
	// property constants
	public static final String REALNAME = "realname";
	public static final String BRITHDAY = "brithday";
	public static final String WEIGHT = "weight";
	public static final String HEIGHT = "height";
	public static final String TYPE = "type";


}