package com.qianshui.co.medical.db;

/**
 * Suggest entity. @author MyEclipse Persistence Tools
 */

public class Suggest implements java.io.Serializable {

	// Fields

	private String id;
	private String title;
	private String imagepath;

	// Constructors

	/** default constructor */
	public Suggest() {
	}

	/** full constructor */
	public Suggest(String title, String imagepath) {
		this.title = title;
		this.imagepath = imagepath;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImagepath() {
		return this.imagepath;
	}

	public void setImagepath(String imagepath) {
		this.imagepath = imagepath;
	}

}