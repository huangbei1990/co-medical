package com.qianshui.co.medical.db;

import java.sql.Timestamp;

/**
 * PrivateQuestionViewId entity. @author MyEclipse Persistence Tools
 */

public class PrivateQuestionView implements java.io.Serializable {

	// Fields

	private String id;
	private String userid;
	private String title;
	private String content;
	private String pic;
	private Timestamp createdate;
	private Integer private_;
	private String hospitalid;
	private Integer answercount;
	private String username;
	private String name;
	private String departmentid;
	private String departmentname;

	// Constructors

	/** default constructor */
	public PrivateQuestionView() {
	}

	/** minimal constructor */
	public PrivateQuestionView(String id) {
		this.id = id;
	}

	/** full constructor */
	public PrivateQuestionView(String id, String userid, String title,
			String content, String pic, Timestamp createdate, Integer private_,
			String hospitalid, Integer answercount, String username,
			String name, String departmentid, String departmentname) {
		this.id = id;
		this.userid = userid;
		this.title = title;
		this.content = content;
		this.pic = pic;
		this.createdate = createdate;
		this.private_ = private_;
		this.hospitalid = hospitalid;
		this.answercount = answercount;
		this.username = username;
		this.name = name;
		this.departmentid = departmentid;
		this.departmentname = departmentname;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserid() {
		return this.userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPic() {
		return this.pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Timestamp getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Timestamp createdate) {
		this.createdate = createdate;
	}

	public Integer getPrivate_() {
		return this.private_;
	}

	public void setPrivate_(Integer private_) {
		this.private_ = private_;
	}

	public String getHospitalid() {
		return this.hospitalid;
	}

	public void setHospitalid(String hospitalid) {
		this.hospitalid = hospitalid;
	}

	public Integer getAnswercount() {
		return this.answercount;
	}

	public void setAnswercount(Integer answercount) {
		this.answercount = answercount;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepartmentid() {
		return this.departmentid;
	}

	public void setDepartmentid(String departmentid) {
		this.departmentid = departmentid;
	}

	public String getDepartmentname() {
		return this.departmentname;
	}

	public void setDepartmentname(String departmentname) {
		this.departmentname = departmentname;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof PrivateQuestionView))
			return false;
		PrivateQuestionView castOther = (PrivateQuestionView) other;

		return ((this.getId() == castOther.getId()) || (this.getId() != null
				&& castOther.getId() != null && this.getId().equals(
				castOther.getId())))
				&& ((this.getUserid() == castOther.getUserid()) || (this
						.getUserid() != null && castOther.getUserid() != null && this
						.getUserid().equals(castOther.getUserid())))
				&& ((this.getTitle() == castOther.getTitle()) || (this
						.getTitle() != null && castOther.getTitle() != null && this
						.getTitle().equals(castOther.getTitle())))
				&& ((this.getContent() == castOther.getContent()) || (this
						.getContent() != null && castOther.getContent() != null && this
						.getContent().equals(castOther.getContent())))
				&& ((this.getPic() == castOther.getPic()) || (this.getPic() != null
						&& castOther.getPic() != null && this.getPic().equals(
						castOther.getPic())))
				&& ((this.getCreatedate() == castOther.getCreatedate()) || (this
						.getCreatedate() != null
						&& castOther.getCreatedate() != null && this
						.getCreatedate().equals(castOther.getCreatedate())))
				&& ((this.getPrivate_() == castOther.getPrivate_()) || (this
						.getPrivate_() != null
						&& castOther.getPrivate_() != null && this
						.getPrivate_().equals(castOther.getPrivate_())))
				&& ((this.getHospitalid() == castOther.getHospitalid()) || (this
						.getHospitalid() != null
						&& castOther.getHospitalid() != null && this
						.getHospitalid().equals(castOther.getHospitalid())))
				&& ((this.getAnswercount() == castOther.getAnswercount()) || (this
						.getAnswercount() != null
						&& castOther.getAnswercount() != null && this
						.getAnswercount().equals(castOther.getAnswercount())))
				&& ((this.getUsername() == castOther.getUsername()) || (this
						.getUsername() != null
						&& castOther.getUsername() != null && this
						.getUsername().equals(castOther.getUsername())))
				&& ((this.getName() == castOther.getName()) || (this.getName() != null
						&& castOther.getName() != null && this.getName()
						.equals(castOther.getName())))
				&& ((this.getDepartmentid() == castOther.getDepartmentid()) || (this
						.getDepartmentid() != null
						&& castOther.getDepartmentid() != null && this
						.getDepartmentid().equals(castOther.getDepartmentid())))
				&& ((this.getDepartmentname() == castOther.getDepartmentname()) || (this
						.getDepartmentname() != null
						&& castOther.getDepartmentname() != null && this
						.getDepartmentname().equals(
								castOther.getDepartmentname())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (getId() == null ? 0 : this.getId().hashCode());
		result = 37 * result
				+ (getUserid() == null ? 0 : this.getUserid().hashCode());
		result = 37 * result
				+ (getTitle() == null ? 0 : this.getTitle().hashCode());
		result = 37 * result
				+ (getContent() == null ? 0 : this.getContent().hashCode());
		result = 37 * result
				+ (getPic() == null ? 0 : this.getPic().hashCode());
		result = 37
				* result
				+ (getCreatedate() == null ? 0 : this.getCreatedate()
						.hashCode());
		result = 37 * result
				+ (getPrivate_() == null ? 0 : this.getPrivate_().hashCode());
		result = 37
				* result
				+ (getHospitalid() == null ? 0 : this.getHospitalid()
						.hashCode());
		result = 37
				* result
				+ (getAnswercount() == null ? 0 : this.getAnswercount()
						.hashCode());
		result = 37 * result
				+ (getUsername() == null ? 0 : this.getUsername().hashCode());
		result = 37 * result
				+ (getName() == null ? 0 : this.getName().hashCode());
		result = 37
				* result
				+ (getDepartmentid() == null ? 0 : this.getDepartmentid()
						.hashCode());
		result = 37
				* result
				+ (getDepartmentname() == null ? 0 : this.getDepartmentname()
						.hashCode());
		return result;
	}

}