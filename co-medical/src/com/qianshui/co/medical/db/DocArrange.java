package com.qianshui.co.medical.db;

public class DocArrange {
	
	private String docid;
	private String name;
	private Double money;
	private int sunday =3;
	private int monday =3;   // 0表示上午,1表示下午,2表示全天.
	private int tuesday =3;
	private int wednesday=3;
	private int thursday=3;
	private int friday=3;   
	private int saturday=3;
	private String sunPeriod;
	private String monPeriod;
	private String tuePeriod;
	private String wedPeriod;
	private String thuPeriod;
	private String friPeriod;
	private String satPeriod;
	
	
	
	public String getWedPeriod() {
		return wedPeriod;
	}

	public void setWedPeriod(String wedPeriod) {
		this.wedPeriod = wedPeriod;
	}

	public String getSunPeriod() {
		return sunPeriod;
	}

	public void setSunPeriod(String sunPeriod) {
		this.sunPeriod = sunPeriod;
	}

	public String getMonPeriod() {
		return monPeriod;
	}

	public void setMonPeriod(String monPeriod) {
		this.monPeriod = monPeriod;
	}

	public String getTuePeriod() {
		return tuePeriod;
	}

	public void setTuePeriod(String tuePeriod) {
		this.tuePeriod = tuePeriod;
	}

	public String getThuPeriod() {
		return thuPeriod;
	}

	public void setThuPeriod(String thuPeriod) {
		this.thuPeriod = thuPeriod;
	}

	public String getFriPeriod() {
		return friPeriod;
	}

	public void setFriPeriod(String friPeriod) {
		this.friPeriod = friPeriod;
	}

	public String getSatPeriod() {
		return satPeriod;
	}

	public void setSatPeriod(String satPeriod) {
		this.satPeriod = satPeriod;
	}

	public String getDocid() {
		return docid;
	}
	
	public void setDocid(String docid) {
		this.docid = docid;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getMoney() {
		return money;
	}
	public void setMoney(Double money) {
		this.money = money;
	}
	public int getSunday() {
		return sunday;
	}
	public void setSunday(int sunday) {
		this.sunday = sunday;
	}
	public int getMonday() {
		return monday;
	}
	public void setMonday(int monday) {
		this.monday = monday;
	}
	public int getTuesday() {
		return tuesday;
	}
	public void setTuesday(int tuesday) {
		this.tuesday = tuesday;
	}
	public int getWednesday() {
		return wednesday;
	}
	public void setWednesday(int wednesday) {
		this.wednesday = wednesday;
	}
	public int getThursday() {
		return thursday;
	}
	public void setThursday(int thursday) {
		this.thursday = thursday;
	}
	public int getFriday() {
		return friday;
	}
	public void setFriday(int friday) {
		this.friday = friday;
	}
	public int getSaturday() {
		return saturday;
	}
	public void setSaturday(int saturday) {
		this.saturday = saturday;
	}
	
	
	
}
