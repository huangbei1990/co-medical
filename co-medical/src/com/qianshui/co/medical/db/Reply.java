package com.qianshui.co.medical.db;

import java.sql.Timestamp;

/**
 * Reply entity. @author MyEclipse Persistence Tools
 */

public class Reply implements java.io.Serializable {

	// Fields

	private String id;
	private String threadid;
	private Timestamp createdate;
	private String userid;
	private String content;
	private String pic;

	// Constructors

	/** default constructor */
	public Reply() {
	}

	/** full constructor */
	public Reply(String threadid, Timestamp createdate, String userid,
			String content, String pic) {
		this.threadid = threadid;
		this.createdate = createdate;
		this.userid = userid;
		this.content = content;
		this.pic = pic;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getThreadid() {
		return this.threadid;
	}

	public void setThreadid(String threadid) {
		this.threadid = threadid;
	}

	public Timestamp getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Timestamp createdate) {
		this.createdate = createdate;
	}

	public String getUserid() {
		return this.userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPic() {
		return this.pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

}