package com.qianshui.co.medical.db;

import java.sql.Timestamp;

/**
 * BodySign entity. @author MyEclipse Persistence Tools
 */

public class BodySign implements java.io.Serializable {

	// Fields

	private String id;
	private String userid;
	private Float temperature;
	private Float pulse;
	private Float brething;
	private Float SBloodPressure;
	private Integer fetalMovement;
	private Float fhr;
	private Timestamp createdate;
	private Float DBloodPressure;

	// Constructors

	/** default constructor */
	public BodySign() {
	}

	/** minimal constructor */
	public BodySign(String userid, Timestamp createdate) {
		this.userid = userid;
		this.createdate = createdate;
	}

	/** full constructor */
	public BodySign(String userid, Float temperature, Float pulse,
			Float brething, Float SBloodPressure, Integer fetalMovement,
			Float fhr, Timestamp createdate, Float DBloodPressure) {
		this.userid = userid;
		this.temperature = temperature;
		this.pulse = pulse;
		this.brething = brething;
		this.SBloodPressure = SBloodPressure;
		this.fetalMovement = fetalMovement;
		this.fhr = fhr;
		this.createdate = createdate;
		this.DBloodPressure = DBloodPressure;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserid() {
		return this.userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public Float getTemperature() {
		return this.temperature;
	}

	public void setTemperature(Float temperature) {
		this.temperature = temperature;
	}

	public Float getPulse() {
		return this.pulse;
	}

	public void setPulse(Float pulse) {
		this.pulse = pulse;
	}

	public Float getBrething() {
		return this.brething;
	}

	public void setBrething(Float brething) {
		this.brething = brething;
	}

	public Float getSBloodPressure() {
		return this.SBloodPressure;
	}

	public void setSBloodPressure(Float SBloodPressure) {
		this.SBloodPressure = SBloodPressure;
	}

	public Integer getFetalMovement() {
		return this.fetalMovement;
	}

	public void setFetalMovement(Integer fetalMovement) {
		this.fetalMovement = fetalMovement;
	}

	public Float getFhr() {
		return this.fhr;
	}

	public void setFhr(Float fhr) {
		this.fhr = fhr;
	}

	public Timestamp getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Timestamp createdate) {
		this.createdate = createdate;
	}

	public Float getDBloodPressure() {
		return this.DBloodPressure;
	}

	public void setDBloodPressure(Float DBloodPressure) {
		this.DBloodPressure = DBloodPressure;
	}

}