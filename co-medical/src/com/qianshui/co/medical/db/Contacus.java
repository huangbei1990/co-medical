package com.qianshui.co.medical.db;

/**
 * Contacus entity. @author MyEclipse Persistence Tools
 */

public class Contacus implements java.io.Serializable {

	// Fields

	private Integer idcontacus;
	private String contactcon;

	// Constructors

	/** default constructor */
	public Contacus() {
	}

	/** full constructor */
	public Contacus(String contactcon) {
		this.contactcon = contactcon;
	}

	// Property accessors

	public Integer getIdcontacus() {
		return this.idcontacus;
	}

	public void setIdcontacus(Integer idcontacus) {
		this.idcontacus = idcontacus;
	}

	public String getContactcon() {
		return this.contactcon;
	}

	public void setContactcon(String contactcon) {
		this.contactcon = contactcon;
	}

}