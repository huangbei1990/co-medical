package com.qianshui.co.medical.db;

import java.sql.Timestamp;

/**
 * ReportAnswer entity. @author MyEclipse Persistence Tools
 */

public class ReportAnswer implements java.io.Serializable {

	// Fields

	private String id;
	private String reportid;
	private String content;
	private String adminUserid;
	private Timestamp createdate;
	private String answerPic;

	// Constructors

	/** default constructor */
	public ReportAnswer() {
	}

	/** minimal constructor */
	public ReportAnswer(String id) {
		this.id = id;
	}

	/** full constructor */
	public ReportAnswer(String id, String reportid, String content,
			String adminUserid, Timestamp createdate, String answerPic) {
		this.id = id;
		this.reportid = reportid;
		this.content = content;
		this.adminUserid = adminUserid;
		this.createdate = createdate;
		this.answerPic = answerPic;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getReportid() {
		return this.reportid;
	}

	public void setReportid(String reportid) {
		this.reportid = reportid;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAdminUserid() {
		return this.adminUserid;
	}

	public void setAdminUserid(String adminUserid) {
		this.adminUserid = adminUserid;
	}

	public Timestamp getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Timestamp createdate) {
		this.createdate = createdate;
	}

	public String getAnswerPic() {
		return this.answerPic;
	}

	public void setAnswerPic(String answerPic) {
		this.answerPic = answerPic;
	}

}