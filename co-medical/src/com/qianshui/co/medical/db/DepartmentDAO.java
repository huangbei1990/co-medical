package com.qianshui.co.medical.db;

import java.util.List;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * Department entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.qianshui.co.medical.db.Department
 * @author MyEclipse Persistence Tools
 */
public class DepartmentDAO extends BaseHibernateDAO<Department> implements IDepartmentDAO {
	DepartmentDAO(Class c) {
		super(c);
		// TODO Auto-generated constructor stub
	}

	private static final Logger log = LoggerFactory
			.getLogger(DepartmentDAO.class);
	// property constants
	public static final String NAME = "name";

	
}