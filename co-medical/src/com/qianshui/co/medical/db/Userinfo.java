package com.qianshui.co.medical.db;

import java.util.Date;

/**
 * Userinfo entity. @author MyEclipse Persistence Tools
 */

public class Userinfo implements java.io.Serializable {

	// Fields

	private String userid;
	private String realname;
	private String birthday;
	private Double weight;
	private Double height;
	private Integer type;
	private Date firstMenarch;
	private Date lastMenarch;
	private Integer menarchDays;
	private Integer cycleDays;
	private String visitCard;
	private String medicalCard;
	private Integer age;
	private String identityCard;
	private Date birthdayBaby;
	private Date pregnancyDay;
	private Integer status;
	private String city;

	// Constructors

	/** default constructor */
	public Userinfo() {
	}

	/** minimal constructor */
	public Userinfo(String userid) {
		this.userid = userid;
	}

	/** full constructor */
	public Userinfo(String userid, String realname, String birthday,
			Double weight, Double height, Integer type, Date firstMenarch,
			Date lastMenarch, Integer menarchDays, Integer cycleDays,
			String visitCard, String medicalCard, Integer age,
			String identityCard, Date birthdayBaby, Date pregnancyDay,
			Integer status, String city) {
		this.userid = userid;
		this.realname = realname;
		this.birthday = birthday;
		this.weight = weight;
		this.height = height;
		this.type = type;
		this.firstMenarch = firstMenarch;
		this.lastMenarch = lastMenarch;
		this.menarchDays = menarchDays;
		this.cycleDays = cycleDays;
		this.visitCard = visitCard;
		this.medicalCard = medicalCard;
		this.age = age;
		this.identityCard = identityCard;
		this.birthdayBaby = birthdayBaby;
		this.pregnancyDay = pregnancyDay;
		this.status = status;
		this.city = city;
	}

	// Property accessors

	public String getUserid() {
		return this.userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getRealname() {
		return this.realname;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public String getBirthday() {
		return this.birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public Double getWeight() {
		return this.weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getHeight() {
		return this.height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Date getFirstMenarch() {
		return this.firstMenarch;
	}

	public void setFirstMenarch(Date firstMenarch) {
		this.firstMenarch = firstMenarch;
	}

	public Date getLastMenarch() {
		return this.lastMenarch;
	}

	public void setLastMenarch(Date lastMenarch) {
		this.lastMenarch = lastMenarch;
	}

	public Integer getMenarchDays() {
		return this.menarchDays;
	}

	public void setMenarchDays(Integer menarchDays) {
		this.menarchDays = menarchDays;
	}

	public Integer getCycleDays() {
		return this.cycleDays;
	}

	public void setCycleDays(Integer cycleDays) {
		this.cycleDays = cycleDays;
	}

	public String getVisitCard() {
		return this.visitCard;
	}

	public void setVisitCard(String visitCard) {
		this.visitCard = visitCard;
	}

	public String getMedicalCard() {
		return this.medicalCard;
	}

	public void setMedicalCard(String medicalCard) {
		this.medicalCard = medicalCard;
	}

	public Integer getAge() {
		return this.age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getIdentityCard() {
		return this.identityCard;
	}

	public void setIdentityCard(String identityCard) {
		this.identityCard = identityCard;
	}

	public Date getBirthdayBaby() {
		return this.birthdayBaby;
	}

	public void setBirthdayBaby(Date birthdayBaby) {
		this.birthdayBaby = birthdayBaby;
	}

	public Date getPregnancyDay() {
		return this.pregnancyDay;
	}

	public void setPregnancyDay(Date pregnancyDay) {
		this.pregnancyDay = pregnancyDay;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

}