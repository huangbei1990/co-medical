package com.qianshui.co.medical.db;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * Hospital entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.qianshui.co.medical.db.Hospital
 * @author MyEclipse Persistence Tools
 */
public class HospitalDAO extends BaseHibernateDAO<Hospital> implements IHospitalDAO {
	HospitalDAO(Class c) {
		super(c);
		// TODO Auto-generated constructor stub
	}

	private static final Logger log = LoggerFactory
			.getLogger(HospitalDAO.class);
	// property constants
	public static final String NAME = "name";
	public static final String LEVEL = "level";
	public static final String ADDRESS = "address";
	public static final String AREAID = "areaid";
	public static final String LONGITUDE = "longitude";
	public static final String LATITUDE = "latitude";
	public static final String TEL = "tel";
	public static final String INTRODUCE = "introduce";
	@Override
	public List<Hospital> getHospitalByDengjiCity(String level, String cityid) {
		// TODO Auto-generated method stub
		List<Hospital> hospitalList =new ArrayList<Hospital>();
		List<Area> list = DaoFactory.getInstance().getArea_dao().findByProperty("cityid", cityid);
		for(int i=0;i<list.size();i++){
			String[] propertyNames={"areaid","level"};
			String[] values = {list.get(i).getId(),level};
			List<Hospital> list2 = DaoFactory.getInstance().getHospital_dao().findByProperty(propertyNames, values);
			hospitalList.addAll(list2);
		}
		
		return hospitalList;
	}
	@Override
	public List<Hospital> getHospitalAllByCity(String cityid) {
		// TODO Auto-generated method stub
		List<Hospital> hospitalList =new ArrayList<Hospital>();
		List<Area> list = DaoFactory.getInstance().getArea_dao().findByProperty("cityid", cityid);
		for(int i=0;i<list.size();i++){
			List<Hospital> list2 = DaoFactory.getInstance().getHospital_dao().findByProperty("areaid", list.get(i).getId());
			hospitalList.addAll(list2);
		}
//		for(int i =0;i<hospitalList.size();i++){
//			System.out.println(hospitalList.get(i).getName());
//		}
		return hospitalList;
	}


}