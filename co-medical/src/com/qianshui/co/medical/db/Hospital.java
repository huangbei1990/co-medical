package com.qianshui.co.medical.db;

/**
 * Hospital entity. @author MyEclipse Persistence Tools
 */

public class Hospital implements java.io.Serializable {

	// Fields

	private String id;
	private String name;
	private String level;
	private String characters;
	private String address;
	private String website;
	private String tel;
	private String booktel;
	private String introduce;
	private String areaid;
	private String daohang;
	private String forenoontime;
	private String afternoontime;

	// Constructors

	/** default constructor */
	public Hospital() {
	}

	/** minimal constructor */
	public Hospital(String id) {
		this.id = id;
	}

	/** full constructor */
	public Hospital(String id, String name, String level, String characters,
			String address, String website, String tel, String booktel,
			String introduce, String areaid, String daohang,
			String forenoontime, String afternoontime) {
		this.id = id;
		this.name = name;
		this.level = level;
		this.characters = characters;
		this.address = address;
		this.website = website;
		this.tel = tel;
		this.booktel = booktel;
		this.introduce = introduce;
		this.areaid = areaid;
		this.daohang = daohang;
		this.forenoontime = forenoontime;
		this.afternoontime = afternoontime;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLevel() {
		return this.level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getCharacters() {
		return this.characters;
	}

	public void setCharacters(String characters) {
		this.characters = characters;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getTel() {
		return this.tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getBooktel() {
		return this.booktel;
	}

	public void setBooktel(String booktel) {
		this.booktel = booktel;
	}

	public String getIntroduce() {
		return this.introduce;
	}

	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}

	public String getAreaid() {
		return this.areaid;
	}

	public void setAreaid(String areaid) {
		this.areaid = areaid;
	}

	public String getDaohang() {
		return this.daohang;
	}

	public void setDaohang(String daohang) {
		this.daohang = daohang;
	}

	public String getForenoontime() {
		return this.forenoontime;
	}

	public void setForenoontime(String forenoontime) {
		this.forenoontime = forenoontime;
	}

	public String getAfternoontime() {
		return this.afternoontime;
	}

	public void setAfternoontime(String afternoontime) {
		this.afternoontime = afternoontime;
	}

}