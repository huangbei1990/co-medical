package com.qianshui.co.medical.db;

import java.util.Date;

/**
 * Userdayinfo entity. @author MyEclipse Persistence Tools
 */

public class Userdayinfo implements java.io.Serializable {

	// Fields

	private Integer id;
	private String userid;
	private Double temperature;
	private Double weight;
	private Integer ovulation;
	private Integer acyeterion;
	private Integer love;
	private Integer yuejing;
	private Date inputdate;

	// Constructors

	/** default constructor */
	public Userdayinfo() {
	}

	/** full constructor */
	public Userdayinfo(String userid, Double temperature, Double weight,
			Integer ovulation, Integer acyeterion, Integer love,
			Integer yuejing, Date inputdate) {
		this.userid = userid;
		this.temperature = temperature;
		this.weight = weight;
		this.ovulation = ovulation;
		this.acyeterion = acyeterion;
		this.love = love;
		this.yuejing = yuejing;
		this.inputdate = inputdate;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserid() {
		return this.userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public Double getTemperature() {
		return this.temperature;
	}

	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}

	public Double getWeight() {
		return this.weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Integer getOvulation() {
		return this.ovulation;
	}

	public void setOvulation(Integer ovulation) {
		this.ovulation = ovulation;
	}

	public Integer getAcyeterion() {
		return this.acyeterion;
	}

	public void setAcyeterion(Integer acyeterion) {
		this.acyeterion = acyeterion;
	}

	public Integer getLove() {
		return this.love;
	}

	public void setLove(Integer love) {
		this.love = love;
	}

	public Integer getYuejing() {
		return this.yuejing;
	}

	public void setYuejing(Integer yuejing) {
		this.yuejing = yuejing;
	}

	public Date getInputdate() {
		return this.inputdate;
	}

	public void setInputdate(Date inputdate) {
		this.inputdate = inputdate;
	}

}