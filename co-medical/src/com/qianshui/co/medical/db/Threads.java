package com.qianshui.co.medical.db;

import java.sql.Timestamp;

/**
 * Threads entity. @author MyEclipse Persistence Tools
 */

public class Threads implements java.io.Serializable {

	// Fields

	private String id;
	private String pic;
	private String content;
	private String title;
	private String userid;
	private Timestamp createdate;

	// Constructors

	/** default constructor */
	public Threads() {
	}

	/** full constructor */
	public Threads(String pic, String content, String title, String userid,
			Timestamp createdate) {
		this.pic = pic;
		this.content = content;
		this.title = title;
		this.userid = userid;
		this.createdate = createdate;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPic() {
		return this.pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUserid() {
		return this.userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public Timestamp getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Timestamp createdate) {
		this.createdate = createdate;
	}

}