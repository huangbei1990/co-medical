package com.qianshui.co.medical.db;

import java.util.Date;

/**
 * Arrange entity. @author MyEclipse Persistence Tools
 */

public class Arrange implements java.io.Serializable {

	// Fields

	private String id;
	private String doctorid;
	private Date orderdate;
	private Integer type;
	private String period;

	// Constructors

	/** default constructor */
	public Arrange() {
	}

	/** full constructor */
	public Arrange(String doctorid, Date orderDate, Integer type, String period) {
		this.doctorid = doctorid;
		this.orderdate = orderDate;
		this.type = type;
		this.period = period;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDoctorid() {
		return this.doctorid;
	}

	public void setDoctorid(String doctorid) {
		this.doctorid = doctorid;
	}

	public Date getOrderdate() {
		return this.orderdate;
	}

	public void setOrderdate(Date orderDate) {
		this.orderdate = orderDate;
	}

	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getPeriod() {
		return this.period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

}