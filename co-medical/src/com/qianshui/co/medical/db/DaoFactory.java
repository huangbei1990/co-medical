package com.qianshui.co.medical.db;

import java.util.List;



public class DaoFactory {
	private static DaoFactory m_Instance = null;

	public static enum TableName{
		AdminPermission,
		AdminRole,
		AdminUser,
		Area,
		Arrange,
		Category,
		City,
		Department,
		Doctor,
		Hospital,
		Replay,
		Score,
		Threads,
		Trace,
		User,
		Userinfo,
		Suggest,
		News,
		Knowledgeclass,
		Knowledgeitem,
		Msg,
		PregnantKnowledge,
	}
	
	IUserDAO user_dao;
	IUserinfoDAO user_info_dao;

	IHospitalDAO hospital_dao;
	ICityDAO city_dao;
	IAreaDAO area_dao;
	
	IClassidDAO classid_dao;
	

	INewsDAO news_dao;
	IDoctorDAO doctor_dao;
	IDepartmentDAO department_dao;
	IArrangeDAO arrange_dao;
	IUserdayinfoDAO userdayinfo_dao;
	IKnowledgeclassDAO knowledgeclass_dao;
	IKnowledgeitemDAO knowledgeitem_dao;
	IUserbmiDAO userbmi_dao;
	IServicelawDAO servicelaw_dao;
	IContacusDAO contacus_dao;
	

	private DaoFactory() {

	}

	public static DaoFactory getInstance() {
		if (m_Instance == null) {
			m_Instance = new DaoFactory();
		}

		return m_Instance;
	}

	public IUserinfoDAO getUser_info_dao() {
		if (user_info_dao == null) {
			user_info_dao = new UserinfoDAO(Userinfo.class);
		}
		return user_info_dao;
	}

	public IUserDAO getUser_dao() {
		if (user_dao == null) {
			user_dao = new UserDAO(User.class);
		}
		return user_dao;
	}

	public IHospitalDAO getHospital_dao() {
		if(hospital_dao==null){
			hospital_dao= new HospitalDAO(Hospital.class);
    	}
    		return hospital_dao;
	}
	
	public ICityDAO getCity_dao(){
		if(city_dao == null){
			city_dao = new CityDAO(City.class);
		}
		return city_dao;
	}
	
	public IAreaDAO getArea_dao(){
		if(area_dao == null){
			area_dao = new AreaDAO(Area.class);
		}
		return area_dao;
	}

	public INewsDAO getNews_dao() {
		if(news_dao==null){
			news_dao= new NewsDAO(News.class);
    	}
    		return news_dao;
	}

	public IDoctorDAO getDoctor_dao() {
		if(doctor_dao==null){
			doctor_dao= new DoctorDAO(Doctor.class);
    	}
    		return doctor_dao;
	}

	public IDepartmentDAO getDepartment_dao() {
		if(department_dao==null){
			department_dao= new DepartmentDAO(Department.class);
    	}
    		return department_dao;
	}

	public IClassidDAO getClassid_dao() {
		if(classid_dao==null){
			classid_dao= new ClassidDAO(Classid.class);
    	}
    		return classid_dao;
	}
	
	
	public IArrangeDAO getArrange_dao() {
		if(arrange_dao==null)
			arrange_dao=new ArrangeDAO(Arrange.class);
		return arrange_dao;
	}
	
	public IUserdayinfoDAO getUserdayinfo_dao(){
		if(userdayinfo_dao==null){
			userdayinfo_dao=new UserdayinfoDAO(Userdayinfo.class);
		}
		return userdayinfo_dao;
	}
	
	public IKnowledgeclassDAO getKnowledgeclass_dao(){
		if(knowledgeclass_dao==null){
			knowledgeclass_dao=new KnowledgeclassDAO(Knowledgeclass.class);
		}
		return knowledgeclass_dao;
	}
	
	public IKnowledgeitemDAO getKnowledgeitem_dao(){
		if(knowledgeitem_dao==null){
			knowledgeitem_dao=new KnowledgeitemDAO(Knowledgeitem.class);
		}
		return knowledgeitem_dao;
	}
	
	public IUserbmiDAO getUserbmi_dao(){
		if(userbmi_dao==null){
			userbmi_dao=new UserbmiDAO(Userbmi.class);
		}
		return userbmi_dao;
	}
	
	public IServicelawDAO getServicelaw_dao(){
		if(servicelaw_dao==null){
			servicelaw_dao=new ServicelawDAO(Servicelaw.class);
		}
		return servicelaw_dao;
	}
	
	public IContacusDAO getContacus_dao(){
		if(contacus_dao==null){
			contacus_dao=new ContacusDAO(Contacus.class);
		}
		return contacus_dao;
	}
	
	
	public IBaseHibernateDAO<Trace> getTrace_dao() {
		return new BaseHibernateDAO<Trace>(Trace.class);
	}
	
	public IBaseHibernateDAO<Score> getScore_dao() {
		return new BaseHibernateDAO<Score>(Score.class);
	}
	
	public IBaseHibernateDAO<ScoreView> getScoreView_dao() {
		return new BaseHibernateDAO<ScoreView>(ScoreView.class);
	}
	
	public IBaseHibernateDAO<Threads> getThreads_dao() {
		return new BaseHibernateDAO<Threads>(Threads.class);
	}
	
	public IBaseHibernateDAO<Reply> getReply_dao() {
		return new BaseHibernateDAO<Reply>(Reply.class);
	}
	
	public IBaseHibernateDAO<Question> getQuestion_dao() {
		return new BaseHibernateDAO<Question>(Question.class);
	}
	
	public IBaseHibernateDAO<PrivateQuestionView> getPrivateQuestionView_dao() {
		return new BaseHibernateDAO<PrivateQuestionView>(PrivateQuestionView.class);
	}
	
	public IBaseHibernateDAO<PublicQuestionView> getPublicQuestionView_dao() {
		return new BaseHibernateDAO<PublicQuestionView>(PublicQuestionView.class);
	}
	
	public IBaseHibernateDAO<NormalAnswer> getNormalAnswer_dao() {
		return new BaseHibernateDAO<NormalAnswer>(NormalAnswer.class);
	}
	
	public IBaseHibernateDAO<NormalAnswerView> getNormalAnswerView_dao() {
		return new BaseHibernateDAO<NormalAnswerView>(NormalAnswerView.class);
	}
	
	public IBaseHibernateDAO<PrivateAnswer> getPrivateAnswer_dao() {
		return new BaseHibernateDAO<PrivateAnswer>(PrivateAnswer.class);
	}
	
	public IBaseHibernateDAO<PrivateAnswerView> getPrivateAnswerView_dao() {
		return new BaseHibernateDAO<PrivateAnswerView>(PrivateAnswerView.class);
	}
	
	public IBaseHibernateDAO<AdminUser> getAdminUser_dao() {
		return new BaseHibernateDAO<AdminUser>(AdminUser.class);
	}
	
	public IBaseHibernateDAO<AdminRole> getAdminRole_dao() {
		return new BaseHibernateDAO<AdminRole>(AdminRole.class);
	}
	
	public IBaseHibernateDAO<AdminPermission> getAdminPermission_dao() {
		return new BaseHibernateDAO<AdminPermission>(AdminPermission.class);
	}
	
	public IBaseHibernateDAO<LnkRolePermission> getLnkRolePermission_dao() {
		return new BaseHibernateDAO<LnkRolePermission>(LnkRolePermission.class);
	}
	
	public IBaseHibernateDAO<LnkRoleUser> getLnkRoleUser_dao() {
		return new BaseHibernateDAO<LnkRoleUser>(LnkRoleUser.class);
	}
	
	//孕妇知识
	public IBaseHibernateDAO<PregnantKnowledge> getPregnantKnowledge_dao() {
		return new BaseHibernateDAO<PregnantKnowledge>(PregnantKnowledge.class);
	}
	
	//孕妇知识,view
	public IBaseHibernateDAO<PregView> getPregnantKnowledgeView_dao() {
		return new BaseHibernateDAO<PregView>(PregView.class);
	}
	
	//报告
	public IBaseHibernateDAO<Report> getReport_dao() {
		return new BaseHibernateDAO<Report>(Report.class);
	}
	
	//报告回复
	public IBaseHibernateDAO<ReportAnswer> getReportAnswer_dao() {
		return new BaseHibernateDAO<ReportAnswer>(ReportAnswer.class);
	}
	
	//报告回复view
	public IBaseHibernateDAO<ReportAnswerView> getReportAnswerView_dao() {
		return new BaseHibernateDAO<ReportAnswerView>(ReportAnswerView.class);
	}
	
	//论坛,view
	public IBaseHibernateDAO<ForumView> getForumView_dao() {
		return new BaseHibernateDAO<ForumView>(ForumView.class);
	}
	
	//论坛
	public IBaseHibernateDAO<Forum> getForum_dao() {
		return new BaseHibernateDAO<Forum>(Forum.class);
	}
	//生命体征管理
	public IBaseHibernateDAO<BodySign> getBodySign_dao() {
		return new BaseHibernateDAO<BodySign>(BodySign.class);
	}
	
	public IBaseHibernateDAO<Suggest> getSuggest_dao() {
		return new BaseHibernateDAO<Suggest>(Suggest.class);
	}
	
	//建档记录
	public IBaseHibernateDAO<JianyiJibenYibanxinxi> getJianyiJibenYibanxinxi_dao() {
		return new BaseHibernateDAO<JianyiJibenYibanxinxi>(JianyiJibenYibanxinxi.class);
	}
	
	//首次产检
	public IBaseHibernateDAO<FirstInspect> getFirstInspect_dao() {
		return new BaseHibernateDAO<FirstInspect>(FirstInspect.class);
	}
	
	//复查记录
	public IBaseHibernateDAO<SecondInspect> getSecondInspect_dao() {
		return new BaseHibernateDAO<SecondInspect>(SecondInspect.class);
	}
	
	//消息
		public IBaseHibernateDAO<Msg> getMsg_dao() {
			return new BaseHibernateDAO<Msg>(Msg.class);
		}
	// public IBaseHibernateDAO<Sortconfig> getSortconfigDao(){
	// return new BaseHibernateDAO<Sortconfig>(Sortconfig.class);
	// }
}
