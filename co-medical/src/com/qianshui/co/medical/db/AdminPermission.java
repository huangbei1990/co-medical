package com.qianshui.co.medical.db;

import java.util.ArrayList;

/**
 * AdminPermission entity. @author MyEclipse Persistence Tools
 */

public class AdminPermission implements java.io.Serializable {

	// Fields

	private String id;
	private String name;
	private String description;
	private Integer status;
	private String perid;
	private Integer leaf;
	private String panel;
	private ArrayList<AdminPermission> children= new ArrayList<AdminPermission>();
	// Constructors

	/** default constructor */
	public AdminPermission() {
	}

	/** full constructor */
	public AdminPermission(String name, String description, Integer status,
			String perid, Integer leaf, String panel) {
		this.name = name;
		this.description = description;
		this.status = status;
		this.perid = perid;
		this.leaf = leaf;
		this.panel = panel;
	}

	// Property accessors
	public void setChildren(AdminPermission node){
		if(this.id.equals(node.getPerid()) && !this.children.contains(node)){	
			this.children.add(node);
		}	
	}
	public ArrayList<AdminPermission> getChildren(){
		return children;
	}
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getPerid() {
		return this.perid;
	}

	public void setPerid(String perid) {
		this.perid = perid;
	}

	public Integer getLeaf() {
		return this.leaf;
	}

	public void setLeaf(Integer leaf) {
		this.leaf = leaf;
	}

	public String getPanel() {
		return this.panel;
	}

	public void setPanel(String panel) {
		this.panel = panel;
	}

}