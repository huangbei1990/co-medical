package com.qianshui.co.medical.db;

import java.sql.Timestamp;

/**
 * PrivateAnswer entity. @author MyEclipse Persistence Tools
 */

public class PrivateAnswer implements java.io.Serializable {

	// Fields

	private String id;
	private String questionid;
	private String content;
	private String adminUserid;
	private Timestamp createdate;
	private String answer_pic;

	// Constructors

	/** default constructor */
	public PrivateAnswer() {
	}

	/** full constructor */
	public PrivateAnswer(String questionid, String content, String adminUserid,
			Timestamp createdate) {
		this.questionid = questionid;
		this.content = content;
		this.adminUserid = adminUserid;
		this.createdate = createdate;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getQuestionid() {
		return this.questionid;
	}

	public void setQuestionid(String questionid) {
		this.questionid = questionid;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAdminUserid() {
		return this.adminUserid;
	}

	public void setAdminUserid(String adminUserid) {
		this.adminUserid = adminUserid;
	}

	public Timestamp getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Timestamp createdate) {
		this.createdate = createdate;
	}

	public String getAnswer_pic() {
		return answer_pic;
	}

	public void setAnswer_pic(String answer_pic) {
		this.answer_pic = answer_pic;
	}
	
	

}