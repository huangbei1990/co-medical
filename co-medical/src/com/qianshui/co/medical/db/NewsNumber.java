package com.qianshui.co.medical.db;

public class NewsNumber {
	int chankeNum = 0;
	int shengzhikeNum = 0;
	int erkeNum =0;
	
	
	public NewsNumber(int chankeNum, int shengzhikeNum, int erkeNum) {
		this.chankeNum = chankeNum;
		this.shengzhikeNum = shengzhikeNum;
		this.erkeNum = erkeNum;
	}


	public NewsNumber() {
	}


	public int getChankeNum() {
		return chankeNum;
	}


	public void setChankeNum(int chankeNum) {
		this.chankeNum = chankeNum;
	}


	public int getShengzhikeNum() {
		return shengzhikeNum;
	}


	public void setShengzhikeNum(int shengzhikeNum) {
		this.shengzhikeNum = shengzhikeNum;
	}


	public int getErkeNum() {
		return erkeNum;
	}


	public void setErkeNum(int erkeNum) {
		this.erkeNum = erkeNum;
	}
	
	
	
}
