package com.qianshui.co.medical.db;

import java.sql.Timestamp;

/**
 * NormalAnswer entity. @author MyEclipse Persistence Tools
 */

public class NormalAnswer implements java.io.Serializable {

	// Fields

	private String id;
	private String questionid;
	private String content;
	private String userid;
	private Timestamp createdate;
	private String answerPic;

	// Constructors

	/** default constructor */
	public NormalAnswer() {
	}

	/** minimal constructor */
	public NormalAnswer(String id) {
		this.id = id;
	}

	/** full constructor */
	public NormalAnswer(String id, String questionid, String content,
			String userid, Timestamp createdate, String answerPic) {
		this.id = id;
		this.questionid = questionid;
		this.content = content;
		this.userid = userid;
		this.createdate = createdate;
		this.answerPic = answerPic;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getQuestionid() {
		return this.questionid;
	}

	public void setQuestionid(String questionid) {
		this.questionid = questionid;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUserid() {
		return this.userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public Timestamp getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Timestamp createdate) {
		this.createdate = createdate;
	}

	public String getAnswerPic() {
		return this.answerPic;
	}

	public void setAnswerPic(String answerPic) {
		this.answerPic = answerPic;
	}

}