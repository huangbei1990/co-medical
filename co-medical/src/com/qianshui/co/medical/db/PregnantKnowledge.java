package com.qianshui.co.medical.db;

import java.sql.Timestamp;

/**
 * PregnantKnowledge entity. @author MyEclipse Persistence Tools
 */

public class PregnantKnowledge implements java.io.Serializable {

	// Fields

	private String id;
	private String title;
	private String content;
	private String pic;
	private Timestamp createdate;
	private String adminid;
	private Integer top;
	private String departmentid;

	// Constructors

	/** default constructor */
	public PregnantKnowledge() {
	}

	/** full constructor */
	public PregnantKnowledge(String title, String content, String pic,
			Timestamp createdate, String adminid, Integer top,
			String departmentid) {
		this.title = title;
		this.content = content;
		this.pic = pic;
		this.createdate = createdate;
		this.adminid = adminid;
		this.top = top;
		this.departmentid = departmentid;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPic() {
		return this.pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Timestamp getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Timestamp createdate) {
		this.createdate = createdate;
	}

	public String getAdminid() {
		return this.adminid;
	}

	public void setAdminid(String adminid) {
		this.adminid = adminid;
	}

	public Integer getTop() {
		return this.top;
	}

	public void setTop(Integer top) {
		this.top = top;
	}

	public String getDepartmentid() {
		return this.departmentid;
	}

	public void setDepartmentid(String departmentid) {
		this.departmentid = departmentid;
	}

}