package com.qianshui.co.medical.db;

/**
 * SecondInspect entity. @author MyEclipse Persistence Tools
 */

public class SecondInspect implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer wmId;
	private String wmChecktime;
	private String wmYunzhouW;
	private String wmYunzhouD;
	private String wmYuyueriqi;
	private String wmDiagnose;
	private String wmTaidong;
	private Float wmHigh;
	private Float wmWeight;
	private Float wmBloodHigh;
	private Float wmBloodLow;
	private String wmTaixin;
	private String wmTaiwei;
	private String wmGonggao;
	private String wmFuwei;
	private String wmXianlou;
	private String wmXianjie;
	private String wmFuzhong;
	private String wmXueshu;
	private String wmDanbai;
	private String wmZhengzhuang;
	private String wmTreat;
	private String wmCheckhos;
	private String wmCheckcode;
	private String wmCheckname;
	private String isInsert;
	private String visitNum;

	// Constructors

	/** default constructor */
	public SecondInspect() {
	}

	/** minimal constructor */
	public SecondInspect(Integer id, Integer wmId) {
		this.id = id;
		this.wmId = wmId;
	}

	/** full constructor */
	public SecondInspect(Integer id, Integer wmId, String wmChecktime,
			String wmYunzhouW, String wmYunzhouD, String wmYuyueriqi,
			String wmDiagnose, String wmTaidong, Float wmHigh, Float wmWeight,
			Float wmBloodHigh, Float wmBloodLow, String wmTaixin,
			String wmTaiwei, String wmGonggao, String wmFuwei,
			String wmXianlou, String wmXianjie, String wmFuzhong,
			String wmXueshu, String wmDanbai, String wmZhengzhuang,
			String wmTreat, String wmCheckhos, String wmCheckcode,
			String wmCheckname, String isInsert, String visitNum) {
		this.id = id;
		this.wmId = wmId;
		this.wmChecktime = wmChecktime;
		this.wmYunzhouW = wmYunzhouW;
		this.wmYunzhouD = wmYunzhouD;
		this.wmYuyueriqi = wmYuyueriqi;
		this.wmDiagnose = wmDiagnose;
		this.wmTaidong = wmTaidong;
		this.wmHigh = wmHigh;
		this.wmWeight = wmWeight;
		this.wmBloodHigh = wmBloodHigh;
		this.wmBloodLow = wmBloodLow;
		this.wmTaixin = wmTaixin;
		this.wmTaiwei = wmTaiwei;
		this.wmGonggao = wmGonggao;
		this.wmFuwei = wmFuwei;
		this.wmXianlou = wmXianlou;
		this.wmXianjie = wmXianjie;
		this.wmFuzhong = wmFuzhong;
		this.wmXueshu = wmXueshu;
		this.wmDanbai = wmDanbai;
		this.wmZhengzhuang = wmZhengzhuang;
		this.wmTreat = wmTreat;
		this.wmCheckhos = wmCheckhos;
		this.wmCheckcode = wmCheckcode;
		this.wmCheckname = wmCheckname;
		this.isInsert = isInsert;
		this.visitNum = visitNum;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getWmId() {
		return this.wmId;
	}

	public void setWmId(Integer wmId) {
		this.wmId = wmId;
	}

	public String getWmChecktime() {
		return this.wmChecktime;
	}

	public void setWmChecktime(String wmChecktime) {
		this.wmChecktime = wmChecktime;
	}

	public String getWmYunzhouW() {
		return this.wmYunzhouW;
	}

	public void setWmYunzhouW(String wmYunzhouW) {
		this.wmYunzhouW = wmYunzhouW;
	}

	public String getWmYunzhouD() {
		return this.wmYunzhouD;
	}

	public void setWmYunzhouD(String wmYunzhouD) {
		this.wmYunzhouD = wmYunzhouD;
	}

	public String getWmYuyueriqi() {
		return this.wmYuyueriqi;
	}

	public void setWmYuyueriqi(String wmYuyueriqi) {
		this.wmYuyueriqi = wmYuyueriqi;
	}

	public String getWmDiagnose() {
		return this.wmDiagnose;
	}

	public void setWmDiagnose(String wmDiagnose) {
		this.wmDiagnose = wmDiagnose;
	}

	public String getWmTaidong() {
		return this.wmTaidong;
	}

	public void setWmTaidong(String wmTaidong) {
		this.wmTaidong = wmTaidong;
	}

	public Float getWmHigh() {
		return this.wmHigh;
	}

	public void setWmHigh(Float wmHigh) {
		this.wmHigh = wmHigh;
	}

	public Float getWmWeight() {
		return this.wmWeight;
	}

	public void setWmWeight(Float wmWeight) {
		this.wmWeight = wmWeight;
	}

	public Float getWmBloodHigh() {
		return this.wmBloodHigh;
	}

	public void setWmBloodHigh(Float wmBloodHigh) {
		this.wmBloodHigh = wmBloodHigh;
	}

	public Float getWmBloodLow() {
		return this.wmBloodLow;
	}

	public void setWmBloodLow(Float wmBloodLow) {
		this.wmBloodLow = wmBloodLow;
	}

	public String getWmTaixin() {
		return this.wmTaixin;
	}

	public void setWmTaixin(String wmTaixin) {
		this.wmTaixin = wmTaixin;
	}

	public String getWmTaiwei() {
		return this.wmTaiwei;
	}

	public void setWmTaiwei(String wmTaiwei) {
		this.wmTaiwei = wmTaiwei;
	}

	public String getWmGonggao() {
		return this.wmGonggao;
	}

	public void setWmGonggao(String wmGonggao) {
		this.wmGonggao = wmGonggao;
	}

	public String getWmFuwei() {
		return this.wmFuwei;
	}

	public void setWmFuwei(String wmFuwei) {
		this.wmFuwei = wmFuwei;
	}

	public String getWmXianlou() {
		return this.wmXianlou;
	}

	public void setWmXianlou(String wmXianlou) {
		this.wmXianlou = wmXianlou;
	}

	public String getWmXianjie() {
		return this.wmXianjie;
	}

	public void setWmXianjie(String wmXianjie) {
		this.wmXianjie = wmXianjie;
	}

	public String getWmFuzhong() {
		return this.wmFuzhong;
	}

	public void setWmFuzhong(String wmFuzhong) {
		this.wmFuzhong = wmFuzhong;
	}

	public String getWmXueshu() {
		return this.wmXueshu;
	}

	public void setWmXueshu(String wmXueshu) {
		this.wmXueshu = wmXueshu;
	}

	public String getWmDanbai() {
		return this.wmDanbai;
	}

	public void setWmDanbai(String wmDanbai) {
		this.wmDanbai = wmDanbai;
	}

	public String getWmZhengzhuang() {
		return this.wmZhengzhuang;
	}

	public void setWmZhengzhuang(String wmZhengzhuang) {
		this.wmZhengzhuang = wmZhengzhuang;
	}

	public String getWmTreat() {
		return this.wmTreat;
	}

	public void setWmTreat(String wmTreat) {
		this.wmTreat = wmTreat;
	}

	public String getWmCheckhos() {
		return this.wmCheckhos;
	}

	public void setWmCheckhos(String wmCheckhos) {
		this.wmCheckhos = wmCheckhos;
	}

	public String getWmCheckcode() {
		return this.wmCheckcode;
	}

	public void setWmCheckcode(String wmCheckcode) {
		this.wmCheckcode = wmCheckcode;
	}

	public String getWmCheckname() {
		return this.wmCheckname;
	}

	public void setWmCheckname(String wmCheckname) {
		this.wmCheckname = wmCheckname;
	}

	public String getIsInsert() {
		return this.isInsert;
	}

	public void setIsInsert(String isInsert) {
		this.isInsert = isInsert;
	}

	public String getVisitNum() {
		return this.visitNum;
	}

	public void setVisitNum(String visitNum) {
		this.visitNum = visitNum;
	}

}