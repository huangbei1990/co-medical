package com.qianshui.co.medical.db;

/**
 * FirstInspect entity. @author MyEclipse Persistence Tools
 */

public class FirstInspect implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer wmId;
	private String wmWaiyin;
	private String wmYindao;
	private String wmGongjin;
	private String wmZigong;
	private String wmFujian;
	private String wmHengjin;
	private String wmGonggao;
	private String wmFuwei;
	private String wmToutun;
	private String wmByunzhouW;
	private String wmByunzhouD;
	private String wmBdate;
	private String wmShuangding;
	private String wmDuotai;
	private String wmXueshu;
	private String wmXuexing;
	private String wmHbsag;
	private String wmGangong;
	private String wmDanbai;
	private String wmHiv;
	private String wmMeidu;
	private String MXuexing;
	private String wmDiag;
	private String operateCode;
	private String operateName;
	private String operateTime;

	// Constructors

	/** default constructor */
	public FirstInspect() {
	}

	/** minimal constructor */
	public FirstInspect(Integer id, Integer wmId) {
		this.id = id;
		this.wmId = wmId;
	}

	/** full constructor */
	public FirstInspect(Integer id, Integer wmId, String wmWaiyin,
			String wmYindao, String wmGongjin, String wmZigong,
			String wmFujian, String wmHengjin, String wmGonggao,
			String wmFuwei, String wmToutun, String wmByunzhouW,
			String wmByunzhouD, String wmBdate, String wmShuangding,
			String wmDuotai, String wmXueshu, String wmXuexing, String wmHbsag,
			String wmGangong, String wmDanbai, String wmHiv, String wmMeidu,
			String MXuexing, String wmDiag, String operateCode,
			String operateName, String operateTime) {
		this.id = id;
		this.wmId = wmId;
		this.wmWaiyin = wmWaiyin;
		this.wmYindao = wmYindao;
		this.wmGongjin = wmGongjin;
		this.wmZigong = wmZigong;
		this.wmFujian = wmFujian;
		this.wmHengjin = wmHengjin;
		this.wmGonggao = wmGonggao;
		this.wmFuwei = wmFuwei;
		this.wmToutun = wmToutun;
		this.wmByunzhouW = wmByunzhouW;
		this.wmByunzhouD = wmByunzhouD;
		this.wmBdate = wmBdate;
		this.wmShuangding = wmShuangding;
		this.wmDuotai = wmDuotai;
		this.wmXueshu = wmXueshu;
		this.wmXuexing = wmXuexing;
		this.wmHbsag = wmHbsag;
		this.wmGangong = wmGangong;
		this.wmDanbai = wmDanbai;
		this.wmHiv = wmHiv;
		this.wmMeidu = wmMeidu;
		this.MXuexing = MXuexing;
		this.wmDiag = wmDiag;
		this.operateCode = operateCode;
		this.operateName = operateName;
		this.operateTime = operateTime;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getWmId() {
		return this.wmId;
	}

	public void setWmId(Integer wmId) {
		this.wmId = wmId;
	}

	public String getWmWaiyin() {
		return this.wmWaiyin;
	}

	public void setWmWaiyin(String wmWaiyin) {
		this.wmWaiyin = wmWaiyin;
	}

	public String getWmYindao() {
		return this.wmYindao;
	}

	public void setWmYindao(String wmYindao) {
		this.wmYindao = wmYindao;
	}

	public String getWmGongjin() {
		return this.wmGongjin;
	}

	public void setWmGongjin(String wmGongjin) {
		this.wmGongjin = wmGongjin;
	}

	public String getWmZigong() {
		return this.wmZigong;
	}

	public void setWmZigong(String wmZigong) {
		this.wmZigong = wmZigong;
	}

	public String getWmFujian() {
		return this.wmFujian;
	}

	public void setWmFujian(String wmFujian) {
		this.wmFujian = wmFujian;
	}

	public String getWmHengjin() {
		return this.wmHengjin;
	}

	public void setWmHengjin(String wmHengjin) {
		this.wmHengjin = wmHengjin;
	}

	public String getWmGonggao() {
		return this.wmGonggao;
	}

	public void setWmGonggao(String wmGonggao) {
		this.wmGonggao = wmGonggao;
	}

	public String getWmFuwei() {
		return this.wmFuwei;
	}

	public void setWmFuwei(String wmFuwei) {
		this.wmFuwei = wmFuwei;
	}

	public String getWmToutun() {
		return this.wmToutun;
	}

	public void setWmToutun(String wmToutun) {
		this.wmToutun = wmToutun;
	}

	public String getWmByunzhouW() {
		return this.wmByunzhouW;
	}

	public void setWmByunzhouW(String wmByunzhouW) {
		this.wmByunzhouW = wmByunzhouW;
	}

	public String getWmByunzhouD() {
		return this.wmByunzhouD;
	}

	public void setWmByunzhouD(String wmByunzhouD) {
		this.wmByunzhouD = wmByunzhouD;
	}

	public String getWmBdate() {
		return this.wmBdate;
	}

	public void setWmBdate(String wmBdate) {
		this.wmBdate = wmBdate;
	}

	public String getWmShuangding() {
		return this.wmShuangding;
	}

	public void setWmShuangding(String wmShuangding) {
		this.wmShuangding = wmShuangding;
	}

	public String getWmDuotai() {
		return this.wmDuotai;
	}

	public void setWmDuotai(String wmDuotai) {
		this.wmDuotai = wmDuotai;
	}

	public String getWmXueshu() {
		return this.wmXueshu;
	}

	public void setWmXueshu(String wmXueshu) {
		this.wmXueshu = wmXueshu;
	}

	public String getWmXuexing() {
		return this.wmXuexing;
	}

	public void setWmXuexing(String wmXuexing) {
		this.wmXuexing = wmXuexing;
	}

	public String getWmHbsag() {
		return this.wmHbsag;
	}

	public void setWmHbsag(String wmHbsag) {
		this.wmHbsag = wmHbsag;
	}

	public String getWmGangong() {
		return this.wmGangong;
	}

	public void setWmGangong(String wmGangong) {
		this.wmGangong = wmGangong;
	}

	public String getWmDanbai() {
		return this.wmDanbai;
	}

	public void setWmDanbai(String wmDanbai) {
		this.wmDanbai = wmDanbai;
	}

	public String getWmHiv() {
		return this.wmHiv;
	}

	public void setWmHiv(String wmHiv) {
		this.wmHiv = wmHiv;
	}

	public String getWmMeidu() {
		return this.wmMeidu;
	}

	public void setWmMeidu(String wmMeidu) {
		this.wmMeidu = wmMeidu;
	}

	public String getMXuexing() {
		return this.MXuexing;
	}

	public void setMXuexing(String MXuexing) {
		this.MXuexing = MXuexing;
	}

	public String getWmDiag() {
		return this.wmDiag;
	}

	public void setWmDiag(String wmDiag) {
		this.wmDiag = wmDiag;
	}

	public String getOperateCode() {
		return this.operateCode;
	}

	public void setOperateCode(String operateCode) {
		this.operateCode = operateCode;
	}

	public String getOperateName() {
		return this.operateName;
	}

	public void setOperateName(String operateName) {
		this.operateName = operateName;
	}

	public String getOperateTime() {
		return this.operateTime;
	}

	public void setOperateTime(String operateTime) {
		this.operateTime = operateTime;
	}

}