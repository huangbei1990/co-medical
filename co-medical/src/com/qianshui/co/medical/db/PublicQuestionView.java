package com.qianshui.co.medical.db;

import java.sql.Timestamp;

/**
 * PublicQuestionViewId entity. @author MyEclipse Persistence Tools
 */

public class PublicQuestionView implements java.io.Serializable {

	// Fields

	private String id;
	private String userid;
	private String title;
	private String content;
	private String pic;
	private Timestamp createdate;
	private Integer private_;
	private String hospitalid;
	private String username;
	private int answer_count;

	// Constructors

	/** default constructor */
	public PublicQuestionView() {
	}

	/** minimal constructor */
	public PublicQuestionView(String id) {
		this.id = id;
	}

	/** full constructor */
	public PublicQuestionView(String id, String userid, String title,
			String content, String pic, Timestamp createdate, Integer private_,
			String hospitalid, String username) {
		this.id = id;
		this.userid = userid;
		this.title = title;
		this.content = content;
		this.pic = pic;
		this.createdate = createdate;
		this.private_ = private_;
		this.hospitalid = hospitalid;
		this.username = username;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserid() {
		return this.userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPic() {
		return this.pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Timestamp getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Timestamp createdate) {
		this.createdate = createdate;
	}

	public Integer getPrivate_() {
		return this.private_;
	}

	public void setPrivate_(Integer private_) {
		this.private_ = private_;
	}

	public String getHospitalid() {
		return this.hospitalid;
	}

	public void setHospitalid(String hospitalid) {
		this.hospitalid = hospitalid;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	

	public int getAnswer_count() {
		return answer_count;
	}

	public void setAnswer_count(int answer_count) {
		this.answer_count = answer_count;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof PublicQuestionView))
			return false;
		PublicQuestionView castOther = (PublicQuestionView) other;

		return ((this.getId() == castOther.getId()) || (this.getId() != null
				&& castOther.getId() != null && this.getId().equals(
				castOther.getId())))
				&& ((this.getUserid() == castOther.getUserid()) || (this
						.getUserid() != null && castOther.getUserid() != null && this
						.getUserid().equals(castOther.getUserid())))
				&& ((this.getTitle() == castOther.getTitle()) || (this
						.getTitle() != null && castOther.getTitle() != null && this
						.getTitle().equals(castOther.getTitle())))
				&& ((this.getContent() == castOther.getContent()) || (this
						.getContent() != null && castOther.getContent() != null && this
						.getContent().equals(castOther.getContent())))
				&& ((this.getPic() == castOther.getPic()) || (this.getPic() != null
						&& castOther.getPic() != null && this.getPic().equals(
						castOther.getPic())))
				&& ((this.getCreatedate() == castOther.getCreatedate()) || (this
						.getCreatedate() != null
						&& castOther.getCreatedate() != null && this
						.getCreatedate().equals(castOther.getCreatedate())))
				&& ((this.getPrivate_() == castOther.getPrivate_()) || (this
						.getPrivate_() != null
						&& castOther.getPrivate_() != null && this
						.getPrivate_().equals(castOther.getPrivate_())))
				&& ((this.getHospitalid() == castOther.getHospitalid()) || (this
						.getHospitalid() != null
						&& castOther.getHospitalid() != null && this
						.getHospitalid().equals(castOther.getHospitalid())))
				&& ((this.getUsername() == castOther.getUsername()) || (this
						.getUsername() != null
						&& castOther.getUsername() != null && this
						.getUsername().equals(castOther.getUsername())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (getId() == null ? 0 : this.getId().hashCode());
		result = 37 * result
				+ (getUserid() == null ? 0 : this.getUserid().hashCode());
		result = 37 * result
				+ (getTitle() == null ? 0 : this.getTitle().hashCode());
		result = 37 * result
				+ (getContent() == null ? 0 : this.getContent().hashCode());
		result = 37 * result
				+ (getPic() == null ? 0 : this.getPic().hashCode());
		result = 37
				* result
				+ (getCreatedate() == null ? 0 : this.getCreatedate()
						.hashCode());
		result = 37 * result
				+ (getPrivate_() == null ? 0 : this.getPrivate_().hashCode());
		result = 37
				* result
				+ (getHospitalid() == null ? 0 : this.getHospitalid()
						.hashCode());
		result = 37 * result
				+ (getUsername() == null ? 0 : this.getUsername().hashCode());
		return result;
	}

}