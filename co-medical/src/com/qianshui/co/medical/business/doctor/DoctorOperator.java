package com.qianshui.co.medical.business.doctor;

import java.util.List;

import com.qianshui.co.medical.db.Arrange;
import com.qianshui.co.medical.db.City;
import com.qianshui.co.medical.db.DaoFactory;
import com.qianshui.co.medical.db.Hospital;
import com.qianshui.co.medical.db.User;

public class DoctorOperator {
	private static DoctorOperator m_Instance = null;

	private DoctorOperator() {

	}

	public static DoctorOperator getInstance() {
		if (m_Instance == null) {
			m_Instance = new DoctorOperator();
		}

		return m_Instance;
	}

	public void EditHospital(String action, String json) {

	}

	public void EditDepartment(String action, String json) {

	}
	
	public void EditDoctor(String action, String json) {

	}
	
	public List<City> getAllCity(){
    	return DaoFactory.getInstance().getCity_dao().findAll("id",true);
    }
	
	


}
