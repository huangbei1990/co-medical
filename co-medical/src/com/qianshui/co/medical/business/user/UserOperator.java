package com.qianshui.co.medical.business.user;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.qianshui.co.medical.common.IDOperation;
import com.qianshui.co.medical.db.DaoFactory;
import com.qianshui.co.medical.db.User;


public class UserOperator {
	private static UserOperator m_Instance = null;
	 
    private UserOperator() {
    	
    }
    
    public static UserOperator getInstance() {
        if (m_Instance == null) {
        	m_Instance = new UserOperator();
        }
        
        return m_Instance;
    }
    
    public User SignIn(String loginname,String password){
    	return null;
    	
    }
    
    public User SignUp(String loginname,String name,String password){
    
    	return null;
    }
    
   
    
    public List<User> getAllUser(){
    	return DaoFactory.getInstance().getUser_dao().findAll("id",true);
    }
    
    /**
     * 注册
     * @param phone
     * @param pass
     * @param gender
     * @return
     * @throws Exception 
     */
    public User Register(String username, String loginname, String phone,String pass) throws Exception{
    	
    	List<User> user_list=DaoFactory.getInstance().getUser_dao().findByProperty("loginname", loginname);
    	if(user_list.size()>0)
    	{
    		throw new Exception("此登录名已注册！");
    	}

    	
    	User user= new User();
    	user.setId(IDOperation.getClassID(User.class.getName()));
    	user.setCreatedate(new Timestamp(System.currentTimeMillis()));
    	user.setLoginname(loginname);
    	user.setUsername(username);
    	user.setPhone(phone);
    	user.setPassword(pass);
    	if(DaoFactory.getInstance().getUser_dao().save(user))
    	{
    		return user;
    	}
    	return null;

    }
	/**
	 * 登录
	 * @param phone
	 * @param pass
	 * @return
	 */
    public User Login(String loginname,String pass){
    	String[] fields = new String[]{"loginname","password"};
    	List<Object> list= new ArrayList<Object>();
    	list.add(loginname);
    	list.add(pass);
    	List<User> user_list=DaoFactory.getInstance().getUser_dao().findByProperty(fields, list.toArray());
    	if(user_list.size()>0)
    	{
    		return user_list.get(0);
    	}
    	return null;

    }
    
    /**
     * 确认电话号码已存在
     * @param phone
     * @return
     */
    public boolean CheckPhone(String phone){

    	List<User> user_list=DaoFactory.getInstance().getUser_dao().findByProperty("phone", phone);
    	if(user_list.size()>0)
    	{
    		return false;
    	}
    	return true;

    }
    
    /**
     * 确认用户名已存在
     * @param phone
     * @return
     */
    public boolean CheckName(String username){

    	List<User> user_list=DaoFactory.getInstance().getUser_dao().findByProperty("loginname", username);
    	if(user_list.size()>0)
    	{
    		return false;
    	}
    	return true;

    }
    
    /**
     * 修改密码
     * @param loginname
     * @param oldPassword
     * @param newPassword
     * @return
     */
    public boolean ChangePassword(String loginname,String oldPassword,String newPassword){
    	
    	String[] propertyNames=new String[]{"loginname","password"};
		String[] values=new String[]{loginname,oldPassword};
    	List<User> u=DaoFactory.getInstance().getUser_dao().findByProperty(propertyNames, values);
    	if(u.size()==0){
    		return false;
    	}
    	u.get(0).setPassword(newPassword);
    	DaoFactory.getInstance().getUser_dao().saveOrUpdate(u.get(0));
    	return true;
    }
    
    
}
