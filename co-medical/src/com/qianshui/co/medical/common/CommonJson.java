/**
 * *************************************************************
 * @FileName:CommonJson.java
 * @ClassName:CommonJson
 * @Author: wanghong
 * @Create date: 2014.02.27
 * @Description: CommonJson类,处理对象等与JSON的相互转换
 * *************************************************************
 */
package com.qianshui.co.medical.common;
import net.sf.ezmorph.object.DateMorpher;
import net.sf.json.*;
import net.sf.json.util.JSONUtils;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;


/**
 * *************************************************************
 * CommonJson类提供对JSON的通用操作，全部为静态方法，对外提供统一接口；
 * 处理list，Object到JSON的转换。
 * @Author: wanghong
 * @Create date: 2014.02.27
 * @Description: CommonJson类,处理对象等与JSON的相互转换
 * *************************************************************
 */

public class CommonJson {
	
	/**
	 * *************************************************************
	 * FunName : list2Json
     * Description： 将list对象转化为JSON
     * Input: @param list
     * Output:String
     * *************************************************************
	 */
	public static String list2Json(List<?> list) {
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(java.sql.Date.class,new JsonDateValueProcessor());
		jsonConfig.registerJsonValueProcessor(Timestamp.class,new JsonDateValueProcessor());
		JSONArray json = JSONArray.fromObject(list, jsonConfig);
//		Gson gson= new Gson();
		return json.toString();
	}
	
	/**
	 * *************************************************************
	 * FunName : object2Json
     * Description： 将对象转化为JSON
     * Input: @param obj
     * Output:String
     * *************************************************************
	 */
	public static <Object> String object2Json(Object obj) {
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(java.sql.Date.class,new JsonDateValueProcessor());
		jsonConfig.registerJsonValueProcessor(Timestamp.class,new JsonDateValueProcessor());
		jsonConfig.registerJsonValueProcessor(java.util.Date.class,new JsonDateValueProcessor());
	    JSONObject json = JSONObject.fromObject(obj,jsonConfig);
	    return json.toString();
	}
	
	/**
	 * *************************************************************
	 * FunName : Json2Object
     * Description： 将Json转化为对象
     * Input: @param String
     * Input: @param Class
     * Output:Object
     * *************************************************************
	 */
	public static  Object Json2ObjByGson(String json, Class objClass) {
					
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();   
		return gson.fromJson(json, objClass);
	}
	public static  Object Json2Obj(String json, Class objClass) {
		String[] formats={"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd"};  
	    JSONUtils.getMorpherRegistry().registerMorpher(new TimestampMorpher(formats));  
		JSONObject jsonObject= JSONObject.fromObject(json);
		return JSONObject.toBean(jsonObject, objClass);
	}
	public static List<?> Json2List(String json, Class objClass){
		JSONArray jsonArray = JSONArray.fromObject(json);  
        List<?> list = (List) JSONArray.toCollection(jsonArray,  
        		objClass);  
		  return list;
	}
	
	public static Map getMapFromJson(String jsonString) { 
		        JSONObject jsonObject = JSONObject.fromObject(jsonString); 
		        Map map = new HashMap(); 
		        for(Iterator iter = jsonObject.keys(); iter.hasNext();){ 
		            String key = (String)iter.next(); 
		            map.put(key, jsonObject.get(key)); 
		        } 
		        return map; 
		    } 
}
