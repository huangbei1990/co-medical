package com.qianshui.co.medical.common;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class BuildResult {
	public static int Success = 200;
	public static int failed = 201;

	public static String getListResult(int code, List<?> res) throws UnsupportedEncodingException {
		TempResult temp = new TempResult();
		temp.SetListResult(code, res);
		return URLEncoder.encode(CommonJson.object2Json(temp), "utf-8");
	}
	
	public static String getResult(int code, Object res) throws UnsupportedEncodingException {
		TempResult temp = new TempResult();
		temp.SetResult(code, res);
		return URLEncoder.encode(CommonJson.object2Json(temp), "utf-8");
	}
	
	public static String getListResultNoEncode(int code, List<?> res) throws UnsupportedEncodingException {
		TempResult temp = new TempResult();
		temp.SetListResult(code, res);
		return CommonJson.object2Json(temp);
	}
	
	
	public static String getResultNoEncode(int code, Object res) throws UnsupportedEncodingException {
		TempResult temp = new TempResult();
		temp.SetResult(code, res);
		return CommonJson.object2Json(temp);
	}

	public static class TempResult {
		int Code;

		List Result = new ArrayList<Object>();

		
		public void SetResult(int code, Object res) {
			this.Code = code;
			this.Result.add(res);
		}

		public void SetListResult(int code, List<?> res) {
			this.Code = code;
			this.Result = res;
		}

		public int getCode() {
			return Code;
		}

		public List getResult() {
			return Result;
		}
	}
}
