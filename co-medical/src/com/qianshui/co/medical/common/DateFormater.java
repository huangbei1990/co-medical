package com.qianshui.co.medical.common;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateFormater {

	public static String formatShortDateTime(Date times) {
		return new SimpleDateFormat("yyyyMMddHHmmss").format(times);
	}

	public static String formatZHShortDate(Date times) {
		return new SimpleDateFormat("yyyy-MM-dd").format(times);
	}
	
	public static String formatLongDateTime(Date times) {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(times);
	}

	public static String formatZHTime(long times) {
		SimpleDateFormat SDF = new SimpleDateFormat("yyyy年MM月dd日 ");
		SDF.setTimeZone(TimeZone.getTimeZone("GMT+08:00"));
		return SDF.format(new Date(times));

	}

	
	public static Date getDateByShortZHString(String dateStr) {
		Date date = new Date();

		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			date = sdf.parse(dateStr);
			System.out.println(date.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;

	}
	
	public static Date getDateByZHString(String dateStr) {
		Date date = new Date();

		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			date = sdf.parse(dateStr);
			System.out.println(date.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;

	}
	
	// 注意format的格式要与日期String的格式相匹配
	// 此处使用yyyy/MM/dd HH:mm:ss
	public static Date getDateByString(String dateStr) {
		Date date = new Date();

		DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		try {
			date = sdf.parse(dateStr);
			System.out.println(date.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;

	}

	// 注：String的类型必须形如： yyyy-mm-dd hh:mm:ss[.f...] 这样的格式，中括号表示可选，否则报错！！！
	public static Timestamp getTimestampByString(String dateStr) {
		Timestamp ts = new Timestamp(System.currentTimeMillis());  

        try {  
            ts = Timestamp.valueOf(dateStr);  
            System.out.println(ts);  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
		return ts;

	}
	
	public static Date getDateByTimestamp(Timestamp ts) {
        Date date = new Date();  
        try {  
            date = ts;  
            System.out.println(date);  
        } catch (Exception e) {  
            e.printStackTrace();  
        } 
        
        return date;

	}
	
	public static String getCurrentDate(){
		String temp_str="";   
	    Date dt = new Date();   
	    //最后的aa表示“上午”或“下午”    HH表示24小时制    如果换成hh表示12小时制   
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");   
	    temp_str=sdf.format(dt);   
	    return temp_str;   
	}
	
	//得到一个月的第一天日期
	public static String getCurrentMonthFirstDay(String mdate) {
		try{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
		Date date=sdf.parse(mdate);
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR,date.getYear()+1900);
		cal.set(Calendar.MONTH, date.getMonth());
		cal.set(Calendar.DAY_OF_MONTH, 1);
		Date firstDate = cal.getTime();
		  
		  return sdf.format(firstDate);
		}catch(Exception e){
			e.printStackTrace();
			return mdate;
		}
	}
	
	//得到一个月的最后一天日期
	public static String getCurrentMonthLastDay(String mdate){
		try{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
		Date date=sdf.parse(mdate);
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR,date.getYear()+1900);
		cal.set(Calendar.MONTH, date.getMonth());
		cal.add(Calendar.MONTH, 1);
	    cal.set(Calendar.DAY_OF_MONTH, 1);
	    cal.add(Calendar.DAY_OF_MONTH, -1);
	    Date lastDate = cal.getTime();
	    
		  return sdf.format(lastDate);
		}catch(Exception e){
			e.printStackTrace();
			return mdate;
		}
	}
}
