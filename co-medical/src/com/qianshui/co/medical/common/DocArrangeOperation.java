package com.qianshui.co.medical.common;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.qianshui.co.medical.db.Arrange;
import com.qianshui.co.medical.db.DaoFactory;
import com.qianshui.co.medical.db.DocArrange;
import com.qianshui.co.medical.db.Doctor;

public class DocArrangeOperation {

	public static List<DocArrange> getArrangeByDepartment(String departmentid,String start,String end){
		
		List<DocArrange> returnlist = new ArrayList<DocArrange>();
		List<Doctor> list = DaoFactory.getInstance().getDoctor_dao()
				.findByProperty("departmentid", departmentid);
		if(list!=null){
		for(Doctor doc:list){
			DocArrange docarrange = null;
				List<Arrange> list2 = DaoFactory.getInstance().getArrange_dao()
						.query("select * from Arrange where doctorid=\""+doc.getId()+"\" and orderdate>='"+start+"' and orderdate<='"+end+"'");
				if(list2==null){
					docarrange= new DocArrange();
				}else{
					docarrange = getDocArrange(list2);
				}
				docarrange.setDocid(doc.getId());
				docarrange.setName(doc.getName());
				docarrange.setMoney(doc.getMoney());
				returnlist.add(docarrange);
		}
		}		
		return returnlist;
	}
	
	
	private  static DocArrange getDocArrange(List<Arrange> list){
		if(list ==null){
			return null;
		}else{
		DocArrange docarrange = new DocArrange();
		Calendar c = Calendar.getInstance();
		for(Arrange arr:list){							
				c.setTime(arr.getOrderdate());
				int week = c.get(Calendar.DAY_OF_WEEK);
				int type = arr.getType();
				String period = arr.getPeriod();
				changeDocArrangeWeek(docarrange,type,week,period);			
		}		
		return docarrange;	 
		}
		
	}
	
	private static void changeDocArrangeWeek(DocArrange doc,int type,int week,String period){
		switch(week){		
		case 1:
			if(type==0){
			doc.setSunday(0);
			}else if(type==1){
				doc.setSunday(1);
			}else if(type==2){
				doc.setSunday(2);
			}
			doc.setSunPeriod(period);
			break;
		case 2:
			if(type==0){
				doc.setMonday(0);
				}else if(type==1){
					doc.setMonday(1);
				}else if(type==2){
					doc.setMonday(2);
				}
			doc.setMonPeriod(period);
				break;			
		case 3:
			if(type==0){
				doc.setTuesday(0);
				}else if(type==1){
					doc.setTuesday(1);
				}else if(type==2){
					doc.setTuesday(2);
				}
			doc.setTuePeriod(period);
				break;		
		case 4:
			if(type==0){
				doc.setWednesday(0);
				}else if(type==1){
					doc.setWednesday(1);
				}else if(type==2){
					doc.setWednesday(2);
				}
			doc.setWedPeriod(period);
				break;
		case 5:
			if(type==0){
				doc.setThursday(0);
				}else if(type==1){
					doc.setThursday(1);
				}else if(type==2){
					doc.setThursday(2);
				}
			doc.setThuPeriod(period);
				break;
		case 6:
			if(type==0){
				doc.setFriday(0);
				}else if(type==1){
					doc.setFriday(1);
				}else if(type==2){
					doc.setFriday(2);
				}
			doc.setFriPeriod(period);
				break;
		case 7:
			if(type==0){
				doc.setSaturday(0);
				}else if(type==1){
					doc.setSaturday(1);
				}else if(type==2){
					doc.setSaturday(2);
				}
			doc.setSatPeriod(period);
				break;
		}
	}
}
