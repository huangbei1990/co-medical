/** 
 * *************************************************************
 * @FileName:IDOperation.java
 * @Description: ID操作文件
 * @Author: wanghong
 * @Create date: 2014.03.28
 * *************************************************************
 */
package com.qianshui.co.medical.common;

import com.qianshui.co.medical.db.Classid;
import com.qianshui.co.medical.db.DaoFactory;


/** 
 * *************************************************************
 * IDOperation为ID操作类，用于生成各数据表的ID字段值
 * @ClassName:IDOperation
 * @Author: wanghong
 * @Create date: 2014.03.28
 * *************************************************************
 */
public class IDOperation {
	
	 /**
     * *************************************************************
	 * FunName : getClassID
     * Description： 获取对应表的ID字段值
     * Input: @param className(数据表名，如“subject”)
     * Output:@param ID值(如“SJ0006”)
     * *************************************************************
	 */
	public static String getClassID(String className) {
		
		String baseValue = "00000";//数值基准部分
		
        try{
//        	String[] arr = className.slastIndexOf(".").split(".");
//        	if(arr.length>0){
//        	className=arr[arr.length-1];
//        	}
        	
        	className=className.substring(className.lastIndexOf(".")+1,className.length());
        	Classid obj = DaoFactory.getInstance().getClassid_dao().getClassidByClassName(className);
	      
			if (obj != null) {
				/* ID值加1后更改数据库 */
				String value = String.valueOf(obj.getRecord() + 1);
				DaoFactory.getInstance().getClassid_dao().excuteSql
				("update classid set record="+value+" where classname='"+className+"'");
				
				for(int i=value.length();i<obj.getLength();i++){
					value="0"+value;
				}
				String classID = obj.getKeyword() + value;
				return classID;
			}
			
        }catch(Exception e){
        	e.printStackTrace();  
        }
        
		return null;
	}
}