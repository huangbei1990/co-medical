package com.qianshui.co.medical.server;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.qianshui.co.medical.common.BuildResult;
import com.qianshui.co.medical.common.CommonJson;
import com.qianshui.co.medical.common.DateFormater;
import com.qianshui.co.medical.common.IDOperation;
import com.qianshui.co.medical.db.DaoFactory;
import com.qianshui.co.medical.db.Department;
import com.qianshui.co.medical.db.Forum;
import com.qianshui.co.medical.db.ForumView;
import com.qianshui.co.medical.db.NormalAnswer;
import com.qianshui.co.medical.db.NormalAnswerView;
import com.qianshui.co.medical.db.Report;
import com.qianshui.co.medical.db.ReportAnswer;
import com.qianshui.co.medical.db.ReportAnswerView;

@Path("ReportUtilService")
public class ReportUtilService {

	//读取科室列表
	@GET
	@Path("queryDepartmentByHospital")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryDepartmentByHospital(
			@QueryParam("hospitalid") String hospitalid)
			throws UnsupportedEncodingException {
		List<Department> list = DaoFactory.getInstance().getDepartment_dao()
				.findByProperty("hospitalid", hospitalid);
		if (list != null && list.size() >= 0) {
			return BuildResult.getListResult(BuildResult.Success, list);
		}
		return BuildResult.getResult(BuildResult.failed, "没有科室信息");
	}
	
	//分页读取报告列表
	@GET
	@Path("queryReport")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryReport(@QueryParam("Page") String page, 
			                  @QueryParam("Number") String number,
			                  @QueryParam("userID") String userID)
			throws UnsupportedEncodingException {
		try {
			int start=Integer.parseInt(page)*Integer.parseInt(number);
//			String sql="select * from report where hospitalid='"+hospitalid+"' and userid='"+userID+"' ORDER BY createdate DESC LIMIT "+ start +"," + number;
			String sql="select * from report where  userid='"+userID+"' ORDER BY createdate DESC LIMIT "+ start +"," + number;
			List<Report> report=DaoFactory.getInstance().getReport_dao().query(sql);
            if (report != null && report.size() >= 0) {
    			return BuildResult.getListResult(BuildResult.Success, report);
    		}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return BuildResult.getResult(BuildResult.failed, "查询失败");
	}
	
	//增加新的报告，返回id
	@GET
	@Path("addReport")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String addReport(@QueryParam("json") String json)
			throws UnsupportedEncodingException {
		try {
			if (json != null && !json.equals("")) {
				json = URLDecoder.decode(json);
				Report f = (Report) CommonJson.Json2Obj(json, Report.class);
				if (f != null) {
					f.setId(IDOperation.getClassID(Report.class.getName()));
					f.setAnswercount(0);
					f.setCreatedate(DateFormater.getTimestampByString(DateFormater.formatLongDateTime(new Date())) );
				}

				if (DaoFactory.getInstance().getReport_dao().save(f)) {
					return BuildResult.getResult(BuildResult.Success, f.getId());
				}
    		}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return BuildResult.getResult(BuildResult.failed, "提交失败");
	}
	
	//新增报告的图片
	public String addReportImage(String reportID,String imagePath)
			throws UnsupportedEncodingException {
		try {
			  List<Report> r=DaoFactory.getInstance().getReport_dao().findByProperty("id", reportID);
			  if(r.get(0).getPic()!=null&&!r.get(0).equals("")){
				  r.get(0).setPic(r.get(0).getPic()+","+imagePath);
			  }else{
                  r.get(0).setPic(imagePath);
			  }
				if (DaoFactory.getInstance().getReport_dao().update(r.get(0))) {
					return BuildResult.getResult(BuildResult.Success,"添加图片成功");
				}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return BuildResult.getResult(BuildResult.failed, "添加图片失败");
	}
	
	//新增回复
	@GET
	@Path("addReportAnswer")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String addReportAnswer(@QueryParam("ReportAnswer") String json)
			throws UnsupportedEncodingException {
		try {
			if(json!=null&&!json.equals("")){
				json = URLDecoder.decode(json);
				ReportAnswer answer = (ReportAnswer) CommonJson.Json2Obj(json, ReportAnswer.class);
				if (answer != null) {
					answer.setId(IDOperation.getClassID(ReportAnswer.class.getName()));
					answer.setCreatedate(DateFormater.getTimestampByString(DateFormater.formatLongDateTime(new Date())) );
				}

				if (DaoFactory.getInstance().getReportAnswer_dao().save(answer)) {
					return BuildResult.getResult(BuildResult.Success, answer.getId());
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return BuildResult.getResult(BuildResult.failed, "回复失败");
	}
	
	//新增报告回复的图片
	public String addReportAnswerImage(String reportAnswerID,String imagePath)
			throws UnsupportedEncodingException {
		try {
			  List<ReportAnswer> r=DaoFactory.getInstance().getReportAnswer_dao().findByProperty("id", reportAnswerID);
			  if(r.get(0).getAnswerPic()!=null&&!r.get(0).equals("")){
				  r.get(0).setAnswerPic(r.get(0).getAnswerPic()+","+imagePath);
			  }else{
                  r.get(0).setAnswerPic(imagePath);
			  }
				if (DaoFactory.getInstance().getReportAnswer_dao().update(r.get(0))) {
					return BuildResult.getResult(BuildResult.Success,"添加图片成功");
				}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return BuildResult.getResult(BuildResult.failed, "添加图片失败");
	}
	
	//查询所有的回复
	@GET
	@Path("queryReportAnswerView")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryReportAnswerView(@QueryParam("ReportID") String ReportID)
			throws UnsupportedEncodingException {
		try {
			String sql="select * from report_answer_view where reportid='"+ReportID+"' ORDER BY createdate ASC";
            List<ReportAnswerView> reportAnswer=DaoFactory.getInstance().getReportAnswerView_dao().query(sql);
            if (reportAnswer != null && reportAnswer.size() >= 0) {
    			return BuildResult.getListResult(BuildResult.Success, reportAnswer);
    		}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return BuildResult.getResult(BuildResult.failed, "查询失败");
	}

}
