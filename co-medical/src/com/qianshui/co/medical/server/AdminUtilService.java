package com.qianshui.co.medical.server;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.qianshui.co.medical.admin.user.AdminUserOperater;
import com.qianshui.co.medical.business.user.UserOperator;
import com.qianshui.co.medical.common.BuildResult;
import com.qianshui.co.medical.common.CommonJson;
import com.qianshui.co.medical.common.DateFormater;
import com.qianshui.co.medical.common.IDOperation;
import com.qianshui.co.medical.db.AdminPermission;
import com.qianshui.co.medical.db.AdminRole;
import com.qianshui.co.medical.db.AdminUser;
import com.qianshui.co.medical.db.Area;
import com.qianshui.co.medical.db.Arrange;
import com.qianshui.co.medical.db.City;
import com.qianshui.co.medical.db.DaoFactory;
import com.qianshui.co.medical.db.Department;
import com.qianshui.co.medical.db.Doctor;
import com.qianshui.co.medical.db.FirstInspect;
import com.qianshui.co.medical.db.Hospital;
import com.qianshui.co.medical.db.JianyiJibenYibanxinxi;
import com.qianshui.co.medical.db.Knowledgeclass;
import com.qianshui.co.medical.db.Knowledgeitem;
import com.qianshui.co.medical.db.LnkRolePermission;
import com.qianshui.co.medical.db.LnkRoleUser;
import com.qianshui.co.medical.db.Msg;
import com.qianshui.co.medical.db.News;
import com.qianshui.co.medical.db.PregnantKnowledge;
import com.qianshui.co.medical.db.PrivateAnswer;
import com.qianshui.co.medical.db.PrivateQuestionView;
import com.qianshui.co.medical.db.Report;
import com.qianshui.co.medical.db.ReportAnswer;
import com.qianshui.co.medical.db.Score;
import com.qianshui.co.medical.db.SecondInspect;
import com.qianshui.co.medical.db.User;
import com.qianshui.co.medical.db.Userinfo;
import com.qianshui.co.medical.notification.SendEngine;

@Path("AdminUtilService")
public class AdminUtilService {

	@GET
	@Path("login")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String login(@QueryParam("loginname") String loginname,
			@QueryParam("password") String password)
			throws UnsupportedEncodingException {

		AdminUser admin = AdminUserOperater.getInstance().login(loginname,
				password);
		if (admin != null) {
			return BuildResult.getResultNoEncode(BuildResult.Success, admin);
		}

		return BuildResult.getResultNoEncode(BuildResult.failed, "登录错误");
	}

	@GET
	@Path("addNewUser")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String addNewUser(@QueryParam("username") String username,
			@QueryParam("loginname") String loginname,
			@QueryParam("password") String password,
			@QueryParam("hospitalid") String hospitalid)
			throws UnsupportedEncodingException {

		boolean res = AdminUserOperater.getInstance().addUser(username,
				loginname, password, hospitalid);
		if (res) {
			return BuildResult.getResultNoEncode(BuildResult.Success, "操作已成功");
		}

		return BuildResult.getResultNoEncode(BuildResult.failed, "操作失败");
	}

	@GET
	@Path("getMenuList")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getMenuList(@QueryParam("id") String id)
			throws UnsupportedEncodingException {
		List<AdminPermission> MenuList = AdminUserOperater.getInstance()
				.getMenuList(id);
		if (MenuList != null && MenuList.size() > 0) {
			return BuildResult.getListResultNoEncode(BuildResult.Success,
					MenuList);
		}

		return BuildResult.getResultNoEncode(BuildResult.failed, "登录错误");
	}

	@GET
	@Path("getAllAdminUser")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getAllAdminUser() throws UnsupportedEncodingException {
		List<AdminUser> userlist = AdminUserOperater.getInstance()
				.findAllAdmin();
		if (userlist != null && userlist.size() > 0) {
			return BuildResult.getListResultNoEncode(BuildResult.Success,
					userlist);
		}

		return BuildResult.getResultNoEncode(BuildResult.failed, "获取失败");
	}

	@GET
	@Path("getAllPermission")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getAllPermission() throws UnsupportedEncodingException {
		List<AdminPermission> list = AdminUserOperater.getInstance()
				.findAllPermission();
		if (list != null && list.size() > 0) {
			return CommonJson.list2Json(list);
		}

		return BuildResult.getResultNoEncode(BuildResult.failed, "获取失败");
	}

	@GET
	@Path("getAllRole")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getAllRole() throws UnsupportedEncodingException {
		List<AdminRole> list = AdminUserOperater.getInstance().findAllRole();
		if (list != null && list.size() > 0) {
			return BuildResult.getListResultNoEncode(BuildResult.Success, list);
		}

		return BuildResult.getResultNoEncode(BuildResult.failed, "获取失败");
	}

	@GET
	@Path("getRootPermission")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getRootPermission() throws UnsupportedEncodingException {
		List<AdminPermission> list = DaoFactory.getInstance()
				.getAdminPermission_dao()
				.query("select p.* from admin_permission p where p.leaf=0");
		if (list != null && list.size() > 0) {
			return BuildResult.getListResultNoEncode(BuildResult.Success, list);
		}

		return BuildResult.getResultNoEncode(BuildResult.failed, "获取失败");
	}

	@GET
	@Path("getUserRole")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getUserRole(@QueryParam("userid") int userid)
			throws UnsupportedEncodingException {
		List<LnkRoleUser> list = DaoFactory.getInstance().getLnkRoleUser_dao()
				.findByProperty("userid", userid);
		if (list != null && list.size() > 0) {
			return BuildResult.getListResultNoEncode(BuildResult.Success, list);
		}

		return BuildResult.getResultNoEncode(BuildResult.failed, "获取失败");
	}

	@GET
	@Path("getRolePermission")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getRolePermission(@QueryParam("roleid") int roleid)
			throws UnsupportedEncodingException {
		List<LnkRolePermission> list = DaoFactory.getInstance()
				.getLnkRolePermission_dao().findByProperty("roleid", roleid);
		if (list != null && list.size() > 0) {
			return BuildResult.getListResultNoEncode(BuildResult.Success, list);
		}

		return BuildResult.getResultNoEncode(BuildResult.failed, "获取失败");
	}

	@POST
	@Path("addNewRecord")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String addNewRecord(String json)
			throws UnsupportedEncodingException, JSONException {
		JSONObject rs = new JSONObject(json);

		Object obj = AdminUserOperater.getInstance().addOrUpdateRecord(
				rs.get("table").toString(), rs.get("json").toString());
		if (obj != null) {
			return BuildResult.getResultNoEncode(BuildResult.Success, obj);
		}

		return BuildResult.getResultNoEncode(BuildResult.failed, "获取失败");
	}

	@GET
	@Path("deleteRecord")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String deleteRecord(@QueryParam("table") String table,
			@QueryParam("json") String json)
			throws UnsupportedEncodingException {
		Object obj = AdminUserOperater.getInstance().deleteRecord(table, json);
		if (obj != null) {
			return BuildResult.getResultNoEncode(BuildResult.Success, obj);
		}

		return BuildResult.getResultNoEncode(BuildResult.failed, "获取失败");
	}

	@GET
	@Path("addLnkUserRole")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String addLnkUserRole(@QueryParam("userid") int userid,
			@QueryParam("roleid") int roleid)
			throws UnsupportedEncodingException {
		DaoFactory.getInstance().getLnkRoleUser_dao()
				.excuteSql("delete from lnk_role_user where userid=" + userid);
		LnkRoleUser lnk = new LnkRoleUser(roleid, userid);
		if (DaoFactory.getInstance().getLnkRoleUser_dao().save(lnk)) {
			return BuildResult.getResultNoEncode(BuildResult.Success, "true");
		}

		return BuildResult.getResultNoEncode(BuildResult.failed, "更新失败");
	}

	@GET
	@Path("addLnkRolePermission")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String addLnkRolePermission(
			@QueryParam("permissionid") String permissionid,
			@QueryParam("roleid") int roleid)
			throws UnsupportedEncodingException {
		DaoFactory
				.getInstance()
				.getLnkRolePermission_dao()
				.excuteSql(
						"delete from lnk_role_permission where roleid="
								+ roleid);
		boolean res = true;
		if (permissionid != null && permissionid.length() > 0) {
			String[] ids = permissionid.split(";");
			for (int i = 0; i < ids.length; i++) {
				LnkRolePermission lnk = new LnkRolePermission(ids[i], roleid);
				if (!DaoFactory.getInstance().getLnkRolePermission_dao()
						.save(lnk)) {
					res = false;
					break;
				}
			}

		}

		if (res) {
			return BuildResult.getResultNoEncode(BuildResult.Success, "true");
		}

		return BuildResult.getResultNoEncode(BuildResult.failed, "更新失败");
	}

	@GET
	@Path("getAllArea")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getAllArea() throws UnsupportedEncodingException {
		List<Area> list = DaoFactory.getInstance().getArea_dao()
				.findAll("id", true);
		if (list != null && list.size() > 0) {
			return BuildResult.getListResultNoEncode(BuildResult.Success, list);
		}
		return BuildResult.getResultNoEncode(BuildResult.failed, "没有区域信息");
		// return CommonJson.list2Json(product);
	}

	@GET
	@Path("getAllAreaJson")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getAllAreaJson() throws UnsupportedEncodingException {
		List<Area> list = DaoFactory.getInstance().getArea_dao()
				.findAll("id", true);
		if (list != null && list.size() > 0) {
			return CommonJson.list2Json(list);
		}
		return BuildResult.getResultNoEncode(BuildResult.failed, "没有区域信息");
		// return CommonJson.list2Json(product);
	}

	@GET
	@Path("getAllCity")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getAllCity() throws UnsupportedEncodingException {
		List<City> list = DaoFactory.getInstance().getCity_dao()
				.findAll("id", true);
		if (list != null && list.size() > 0) {
			return CommonJson.list2Json(list);
		}
		return BuildResult.getResultNoEncode(BuildResult.failed, "没有区域信息");
		// return CommonJson.list2Json(product);
	}

	@GET
	@Path("getAreaByCity")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getAreaByCity(@QueryParam("cityid") String id)
			throws UnsupportedEncodingException {
		List<Area> list = DaoFactory.getInstance().getArea_dao()
				.findByProperty("cityid", id); // DoctorOperator.getInstance().getAllCity();
		if (list != null && list.size() > 0) {
			return BuildResult.getListResultNoEncode(BuildResult.Success, list);
		}
		return BuildResult.getResultNoEncode(BuildResult.failed, "该城市没有区域信息");
		// return CommonJson.list2Json(product);
	}

	@GET
	@Path("getHospitalByCityAndLevel")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getHospitalByCityAndLevel(
			@QueryParam("cityid") String cityid,
			@QueryParam("level") String level)
			throws UnsupportedEncodingException {
		List<Hospital> list = DaoFactory.getInstance().getHospital_dao()
				.getHospitalByDengjiCity(level, cityid); // DoctorOperator.getInstance().getAllCity();
		if (list != null && list.size() > 0) {
			return BuildResult.getListResultNoEncode(BuildResult.Success, list);
		}
		return BuildResult.getResultNoEncode(BuildResult.failed, "没有该等级的医院");
		// return CommonJson.list2Json(product);
	}

	@GET
	@Path("getHospitalByArea")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getHospitalByArea(@QueryParam("areaid") String areaid)
			throws UnsupportedEncodingException {
		List<Hospital> list = DaoFactory.getInstance().getHospital_dao()
				.findByProperty("areaid", areaid); // DoctorOperator.getInstance().getAllCity();
		if (list != null && list.size() > 0) {
			return BuildResult.getListResultNoEncode(BuildResult.Success, list);
		}
		return BuildResult.getResultNoEncode(BuildResult.failed, "没有该区域的医院");
		// return CommonJson.list2Json(product);
	}

	@GET
	@Path("getHospitalJSon")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getHospitalJSon() throws UnsupportedEncodingException {
		List<Hospital> list = DaoFactory.getInstance().getHospital_dao()
				.findAll("id", true); // DoctorOperator.getInstance().getAllCity();
		if (list != null && list.size() > 0) {
			return CommonJson.list2Json(list);
		}
		return BuildResult.getResultNoEncode(BuildResult.failed, "没有该区域的医院");
		// return CommonJson.list2Json(product);
	}

	@GET
	@Path("queryDepartmentByHospital")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryDepartmentByHospital(
			@QueryParam("hospitalid") String hospitalid)
			throws UnsupportedEncodingException {
		List<Department> list = DaoFactory.getInstance().getDepartment_dao()
				.findByProperty("hospitalid", hospitalid);
		if (list != null && list.size() > 0) {
			return BuildResult.getListResultNoEncode(BuildResult.Success, list);
		}
		return BuildResult.getResultNoEncode(BuildResult.failed, "没有科室信息");
	}

	@GET
	@Path("queryDoctorByDepartment")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryDoctorByDepartment(
			@QueryParam("departmentid") String deparmentid)
			throws UnsupportedEncodingException {
		List<Doctor> list = DaoFactory.getInstance().getDoctor_dao()
				.findByProperty("departmentid", deparmentid);
		if (list != null && list.size() > 0) {
			return BuildResult.getListResultNoEncode(BuildResult.Success, list);
		}
		return BuildResult.getResultNoEncode(BuildResult.failed, "没有医生信息");
	}

	@GET
	@Path("getNewsByDepartment")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getNewsByDepartment(
			@QueryParam("departmentid") String departmentid)
			throws UnsupportedEncodingException {

		List<News> returnList = DaoFactory
				.getInstance()
				.getNews_dao()
				.query("select * from News where " + " departmentid=\""
						+ departmentid + "\" order by createdate desc");
		if (returnList != null && returnList.size() > 0) {
			return BuildResult.getListResultNoEncode(BuildResult.Success,
					returnList);
		}

		return BuildResult
				.getListResultNoEncode(BuildResult.failed, returnList);
		// return BuildResult.getResult(BuildResult.failed, "没有任何新闻");
		// return CommonJson.list2Json(product);
	}

	@GET
	@Path("getKnowledge")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getKnowledge() throws UnsupportedEncodingException {
		List<Knowledgeclass> list = AdminUserOperater.getInstance()
				.buildKnowledgeTree();
		if (list != null && list.size() > 0) {
			return CommonJson.list2Json(list);
		}

		return BuildResult.getResultNoEncode(BuildResult.failed, "获取失败");
	}

	@GET
	@Path("getKnowledgeItem")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getKnowledgeItem(@QueryParam("classid") String classid)
			throws UnsupportedEncodingException {
		List<Knowledgeitem> list = DaoFactory.getInstance()
				.getKnowledgeitem_dao().findByProperty("id", classid);
		if (list != null && list.size() > 0) {
			return BuildResult.getResultNoEncode(BuildResult.Success,
					list.get(0));
		}

		return BuildResult.getResultNoEncode(BuildResult.failed, "获取失败");
	}

	@POST
	@Path("saveKnowledgeItem")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String saveKnowledgeItemw0(String json)
			throws UnsupportedEncodingException {
		Knowledgeitem item = (Knowledgeitem) CommonJson.Json2Obj(json,
				Knowledgeitem.class);
		try {
			DaoFactory.getInstance().getKnowledgeitem_dao().saveOrUpdate(item);
			List<Knowledgeclass> class_list = DaoFactory.getInstance()
					.getKnowledgeclass_dao().findByProperty("id", item.getId());
			if (class_list != null && class_list.size() > 0) {
				Knowledgeclass temp = class_list.get(0);
				temp.setLeaf(1);
				DaoFactory.getInstance().getKnowledgeclass_dao().update(temp);
			}

			return BuildResult
					.getResultNoEncode(BuildResult.Success, "success");
		} catch (Exception ex) {
			return BuildResult.getResultNoEncode(BuildResult.failed, "failed");
		}

	}

	@GET
	@Path("queryArrange")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryArrange(@QueryParam("departmentid") String departmentid,
			@QueryParam("startday") String startday,
			@QueryParam("endday") String endday)
			throws UnsupportedEncodingException {

		List<Map<String, Object>> map_list = AdminUserOperater.getInstance()
				.queryArrange(departmentid, startday, endday);
		if (map_list != null && map_list.size() > 0) {
			return BuildResult.getListResultNoEncode(BuildResult.Success,
					map_list);
		}

		return BuildResult.getResultNoEncode(BuildResult.failed, "操作失败");
	}

	@POST
	@Path("saveArrange")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String saveArrange(String json) throws UnsupportedEncodingException {
		try {
			AdminUserOperater.getInstance().saveArrange(json);

			return BuildResult.getResultNoEncode(BuildResult.Success,
					"当前月数据保存成功！");
		} catch (Exception e) {
			e.printStackTrace();
			return BuildResult.getResultNoEncode(BuildResult.failed, "操作失败！");
		}
	}

	// 分页查询用户所提的私有问题,view
	@GET
	@Path("queryPrivateAdvisoryQuestion")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryPrivateAdvisoryQuestion(
			@QueryParam("start") String page,
			@QueryParam("limit") String number,
			@QueryParam("departmentid") String departmentid)
			throws UnsupportedEncodingException {
		try {
			Map<String, Object> jsonObj = new HashMap<String, Object>();
			int start = Integer.parseInt(page);
			String sql = "select * from private_question_view where departmentid='"
					+ departmentid
					+ "' ORDER BY createdate DESC LIMIT "
					+ start + "," + number;
			List<PrivateQuestionView> question = DaoFactory.getInstance()
					.getPrivateQuestionView_dao().query(sql);
			sql = "select count(*) from private_question_view where departmentid='"
					+ departmentid + "'";
			int count = DaoFactory.getInstance().getPrivateQuestionView_dao()
					.queryCount(sql);
			if (question != null && question.size() >= 0) {
				String json = "{\"total\":" + count + ",\"data\":"
						+ CommonJson.list2Json(question) + "}";
				return json;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return BuildResult.getResult(BuildResult.failed, "查询失败");
	}

	// 查询私有问题的回复
	@GET
	@Path("queryPrivateQuestionAnswer")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryPrivateQuestionAnswer(
			@QueryParam("QuestionID") String questionID)
			throws UnsupportedEncodingException {
		try {
			String sql = "select * from private_answer where questionid='"
					+ questionID + "' ORDER BY createdate ASC";
			List<PrivateAnswer> privateAnswer = DaoFactory.getInstance()
					.getPrivateAnswer_dao().query(sql);

			if (privateAnswer != null && privateAnswer.size() >= 0) {
				return BuildResult.getListResultNoEncode(BuildResult.Success,
						privateAnswer);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return BuildResult.getResult(BuildResult.failed, "查询失败");
	}

	// 提交私有问题的回复
	@GET
	@Path("addPrivateQuestionAnswer")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String addPrivateQuestionAnswer(
			@QueryParam("QuestionID") String questionID,
			@QueryParam("uid") String uid, @QueryParam("content") String content)
			throws UnsupportedEncodingException {
		try {
			PrivateAnswer answer = new PrivateAnswer();
			answer.setId(IDOperation.getClassID(answer.getClass().getName()));
			answer.setCreatedate(new Timestamp(System.currentTimeMillis()));
			answer.setAdminUserid(uid);
			answer.setContent(content);
			answer.setQuestionid(questionID);
			if (DaoFactory.getInstance().getPrivateAnswer_dao().save(answer)) {
				return BuildResult.getResultNoEncode(BuildResult.Success,
						answer);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return BuildResult.getResult(BuildResult.failed, "添加失败");
	}

	@GET
	@Path("queryMsg")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryMsg(@QueryParam("start") String start,
			@QueryParam("limit") String limit)
			throws UnsupportedEncodingException {

		List<Msg> list = DaoFactory
				.getInstance()
				.getMsg_dao()
				.query("select * from msg  ORDER BY createdate DESC LIMIT "
						+ start + "," + limit);

		int count = DaoFactory.getInstance().getMsg_dao()
				.queryCount("select count(*) from msg");
		if (list != null && list.size() > 0) {
			String json = "{\"total\":" + count + ",\"data\":"
					+ CommonJson.list2Json(list) + "}";
			return json;

		}

		return BuildResult.getResultNoEncode(BuildResult.failed, "操作失败");
	}

	@GET
	@Path("sendMsg")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String sendMsg(@QueryParam("id") String id)
			throws UnsupportedEncodingException {

		if (id != null && !id.equals("")) {
			try {
				List<Msg> list = DaoFactory.getInstance().getMsg_dao()
						.findByProperty("id", Long.parseLong(id));
				if (list != null && list.size() > 0) {
					new SendEngine().sendMsgBroadcast(list.get(0));
					Msg msg = list.get(0);
					msg.setIssend(1);
					DaoFactory.getInstance().getMsg_dao().update(msg);
					return BuildResult.getResultNoEncode(BuildResult.Success,
							"发送成功");
				}
			} catch (Exception ex) {
				return BuildResult
						.getResultNoEncode(BuildResult.failed, "操作失败");
			}
		}

		return BuildResult.getResultNoEncode(BuildResult.failed, "操作失败");
	}

	// 分页查询用户所提的私有问题,view
	@GET
	@Path("queryPregKnowledgeView")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryPregKnowledgeView(@QueryParam("start") String page,
			@QueryParam("limit") String number,
			@QueryParam("departmentid") String departmentid)
			throws UnsupportedEncodingException {
		try {
			Map<String, Object> jsonObj = new HashMap<String, Object>();
			int start = Integer.parseInt(page);
			String sql = "select * from private_question_view where departmentid='"
					+ departmentid
					+ "' ORDER BY createdate DESC LIMIT "
					+ start + "," + number;
			List<PrivateQuestionView> question = DaoFactory.getInstance()
					.getPrivateQuestionView_dao().query(sql);
			sql = "select count(*) from private_question_view where departmentid='"
					+ departmentid + "'";
			int count = DaoFactory.getInstance().getPrivateQuestionView_dao()
					.queryCount(sql);
			if (question != null && question.size() >= 0) {
				String json = "{\"total\":" + count + ",\"data\":"
						+ CommonJson.list2Json(question) + "}";
				return json;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return BuildResult.getResult(BuildResult.failed, "查询失败");
	}

	@POST
	@Path("saveJianyi")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String saveJianyi(String json) throws UnsupportedEncodingException {
		try {
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
					.create();

			@SuppressWarnings("unchecked")
			List<JianyiJibenYibanxinxi> temp = (List<JianyiJibenYibanxinxi>) gson
					.fromJson(json,
							new TypeToken<List<JianyiJibenYibanxinxi>>() {
							}.getType());
			if (temp != null && temp.size() > 0) {
				for (JianyiJibenYibanxinxi j : temp) {
					DaoFactory.getInstance().getJianyiJibenYibanxinxi_dao()
							.saveOrUpdate(j);
				}
			}
			return BuildResult
					.getResultNoEncode(BuildResult.Success, "数据保存成功！");
		} catch (Exception e) {
			e.printStackTrace();
			return BuildResult.getResultNoEncode(BuildResult.failed, "操作失败！");
		}
	}

	@POST
	@Path("saveFirstInspect")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String saveFirstInspect(String json)
			throws UnsupportedEncodingException {
		try {
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
					.create();

			@SuppressWarnings("unchecked")
			List<FirstInspect> temp = (List<FirstInspect>) gson.fromJson(json,
					new TypeToken<List<FirstInspect>>() {
					}.getType());

			if (temp != null && temp.size() > 0) {
				for (FirstInspect j : temp) {
					DaoFactory.getInstance().getFirstInspect_dao()
							.saveOrUpdate(j);
				}
			}
			return BuildResult
					.getResultNoEncode(BuildResult.Success, "数据保存成功！");
		} catch (Exception e) {
			e.printStackTrace();
			return BuildResult.getResultNoEncode(BuildResult.failed, "操作失败！");
		}
	}

	@POST
	@Path("saveSecondInspect")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String saveSecondInspect(String json)
			throws UnsupportedEncodingException {
		try {
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
					.create();

			@SuppressWarnings("unchecked")
			List<SecondInspect> temp = (List<SecondInspect>) gson.fromJson(
					json, new TypeToken<List<SecondInspect>>() {
					}.getType());
			if (temp != null && temp.size() > 0) {
				for (SecondInspect j : temp) {
					DaoFactory.getInstance().getSecondInspect_dao()
							.saveOrUpdate(j);
				}
			}
			return BuildResult
					.getResultNoEncode(BuildResult.Success, "数据保存成功！");
		} catch (Exception e) {
			e.printStackTrace();
			return BuildResult.getResultNoEncode(BuildResult.failed, "操作失败！");
		}
	}

	// 分页查询用户所提的私有问题,view
	@GET
	@Path("queryReport")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryReport(@QueryParam("start") String page,
			@QueryParam("limit") String number,
			@QueryParam("departmentid") String departmentid)
			throws UnsupportedEncodingException {
		try {
			Map<String, Object> jsonObj = new HashMap<String, Object>();
			int start = Integer.parseInt(page);
			String sql = "select * from report where departmentid='"
					+ departmentid + "' ORDER BY createdate DESC LIMIT "
					+ start + "," + number;
			List<Report> question = DaoFactory.getInstance().getReport_dao()
					.query(sql);
			sql = "select count(*) from Report where departmentid='"
					+ departmentid + "'";
			int count = DaoFactory.getInstance().getReport_dao()
					.queryCount(sql);
			if (question != null && question.size() >= 0) {
				String json = "{\"total\":" + count + ",\"data\":"
						+ CommonJson.list2Json(question) + "}";
				return json;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return BuildResult.getResult(BuildResult.failed, "查询失败");
	}

	// 查询私有问题的回复
	@GET
	@Path("queryReportAnswer")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryReportAnswer(@QueryParam("reportid") String questionID)
			throws UnsupportedEncodingException {
		try {
			String sql = "select * from report_answer where questionid='"
					+ questionID + "' ORDER BY createdate ASC";
			List<Report> privateAnswer = DaoFactory.getInstance()
					.getReport_dao().query(sql);

			if (privateAnswer != null && privateAnswer.size() >= 0) {
				return BuildResult.getListResultNoEncode(BuildResult.Success,
						privateAnswer);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return BuildResult.getResult(BuildResult.failed, "查询失败");
	}

	// 提交私有问题的回复
	@GET
	@Path("addReportAnswer")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String addReportAnswer(@QueryParam("reportid") String questionID,
			@QueryParam("uid") String uid, @QueryParam("content") String content)
			throws UnsupportedEncodingException {
		try {
			ReportAnswer answer = new ReportAnswer();
			answer.setId(IDOperation.getClassID(answer.getClass().getName()));
			answer.setCreatedate(new Timestamp(System.currentTimeMillis()));
			answer.setAdminUserid(uid);
			answer.setContent(content);
			answer.setReportid(questionID);
			if (DaoFactory.getInstance().getReportAnswer_dao().save(answer)) {
				return BuildResult.getResultNoEncode(BuildResult.Success,
						answer);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return BuildResult.getResult(BuildResult.failed, "添加失败");
	}

	// 分页读取报告列表
	@GET
	@Path("queryPregKnowledge")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryPregKnowledge(@QueryParam("start") String start,
			@QueryParam("limit") String number,
			@QueryParam("departmentid") String departmentid)
			throws UnsupportedEncodingException {
		try {
			String sql = "select * from pregnant_knowledge where departmentid='"
					+ departmentid
					+ "' ORDER BY createdate DESC LIMIT "
					+ start + "," + number;
			List<PregnantKnowledge> pk = DaoFactory.getInstance()
					.getPregnantKnowledge_dao().query(sql);
			
			sql = "select count(*) from pregnant_knowledge where departmentid='"
					+ departmentid + "'";
			int count = DaoFactory.getInstance().getPregnantKnowledge_dao()
					.queryCount(sql);

			if (pk != null && pk.size() >= 0) {
				String json = "{\"total\":" + count + ",\"data\":"
						+ CommonJson.list2Json(pk) + "}";
				return json;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return BuildResult.getResultNoEncode(BuildResult.failed, "查询失败");
	}
}
