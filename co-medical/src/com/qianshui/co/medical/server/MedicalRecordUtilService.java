package com.qianshui.co.medical.server;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.qianshui.co.medical.common.BuildResult;
import com.qianshui.co.medical.db.DaoFactory;
import com.qianshui.co.medical.db.FirstInspect;
import com.qianshui.co.medical.db.ForumView;
import com.qianshui.co.medical.db.JianyiJibenYibanxinxi;
import com.qianshui.co.medical.db.SecondInspect;

@Path("MedicalRecordUtilService")
public class MedicalRecordUtilService {
	
	//查询就诊记录
	@GET
	@Path("queryMedicalRecord")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryMedicalRecord(@QueryParam("Tag") String tag,@QueryParam("Value") String value)
			throws UnsupportedEncodingException {
		try {
			List<JianyiJibenYibanxinxi> medicalRecord=null;
			if(tag.equals("phone")){
			   medicalRecord=DaoFactory.getInstance().getJianyiJibenYibanxinxi_dao().findByProperty("wmMobilePhone", value);
			}else if(tag.equals("identiyNo")){
			   medicalRecord=DaoFactory.getInstance().getJianyiJibenYibanxinxi_dao().findByProperty("wmIdentityno", value);
			}else if(tag.equals("recordNo")){
			   medicalRecord=DaoFactory.getInstance().getJianyiJibenYibanxinxi_dao().findByProperty("wmMrn", value);
			}
            if (medicalRecord != null && medicalRecord.size() > 0) {
    			return BuildResult.getListResult(BuildResult.Success, medicalRecord);
    		}else if(medicalRecord != null && medicalRecord.size() == 0) {
    			return BuildResult.getResult(BuildResult.Success, "false");
    		}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return BuildResult.getResult(BuildResult.failed, "查询失败");
	}
	
	//查询首次产检
	@GET
	@Path("queryFirstInspect")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryFirstInspect(@QueryParam("wmID") String wmID)
			throws UnsupportedEncodingException {
		try {
			List<FirstInspect> firstInspect=
					DaoFactory.getInstance().getFirstInspect_dao().findByProperty("wmId", Integer.parseInt(wmID));
            if (firstInspect != null && firstInspect.size() >= 0) {
    			return BuildResult.getListResult(BuildResult.Success, firstInspect);
    		}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return BuildResult.getResult(BuildResult.failed, "查询失败");
	}

	//查询 复查记录
	@GET
	@Path("querySecondInspect")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String querySecondInspect(@QueryParam("wmID") String wmID)
			throws UnsupportedEncodingException {
		try {
			String sql="select * from second_inspect where wm_id="+wmID+" ORDER BY wm_checktime DESC";
			List<SecondInspect> secondInspect=
					DaoFactory.getInstance().getSecondInspect_dao().query(sql);
            if (secondInspect != null && secondInspect.size() >= 0) {
    			return BuildResult.getListResult(BuildResult.Success, secondInspect);
    		}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return BuildResult.getResult(BuildResult.failed, "查询失败");
	}
}
