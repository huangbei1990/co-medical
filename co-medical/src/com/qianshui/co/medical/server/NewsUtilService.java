package com.qianshui.co.medical.server;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.qianshui.co.medical.business.doctor.DoctorOperator;
import com.qianshui.co.medical.common.BuildResult;
import com.qianshui.co.medical.common.CommonJson;
import com.qianshui.co.medical.db.City;
import com.qianshui.co.medical.db.DaoFactory;
import com.qianshui.co.medical.db.Department;
import com.qianshui.co.medical.db.Msg;
import com.qianshui.co.medical.db.News;
import com.qianshui.co.medical.db.NewsNumber;
import com.qianshui.co.medical.db.Suggest;

@Path("NewsUtilService")
public class NewsUtilService {

	@GET
	@Path("getTopNewsByDepartment")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getTopNewsByDepartment(
			@QueryParam("departmentid") String departmentid)
			throws UnsupportedEncodingException {
		List<News> list = DaoFactory
				.getInstance()
				.getNews_dao()
				.query("select * from News where top=1 "
						+ "and departmentid=\"" + departmentid
						+ "\" order by createdate desc");
		if (list != null && list.size() > 0) {
			return BuildResult.getResult(BuildResult.Success, list.get(0));
		}
		return BuildResult.getResult(BuildResult.failed, "无置顶新闻");
		// return CommonJson.list2Json(product);
	}

	@GET
	@Path("getNewsByDepartmentName")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getNewsByDepartmentName(@QueryParam("name") String name,
			@QueryParam("start") int start) throws UnsupportedEncodingException {
		List<News> returnList = new ArrayList<News>();
		List<Department> dList = DaoFactory.getInstance().getDepartment_dao()
				.findByProperty("name", name);
		if (dList != null && dList.size() > 0) {
			for (Department de : dList) {
				List<News> list = DaoFactory
						.getInstance()
						.getNews_dao()
						.query("select * from News where " + "departmentid=\""
								+ de.getId()
								+ "\" order by createdate desc limit " + start
								+ ",5");
				if (list != null && list.size() > 0) {
					returnList.addAll(list);
				}
			}
		}

		if (returnList != null && returnList.size() > 0) {
			return BuildResult.getListResult(BuildResult.Success, returnList);
		}
		return BuildResult.getResult(BuildResult.failed, "无置顶新闻");
		// return CommonJson.list2Json(product);
	}

	@GET
	@Path("getNewsByHospitalid")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getNewsByHospitalid(@QueryParam("id") String id,
			@QueryParam("start") int start) throws UnsupportedEncodingException {
		List<News> returnList = new ArrayList<News>();
		List<Department> dList = DaoFactory.getInstance().getDepartment_dao()
				.findByProperty("hospitalid", id);
		if (dList != null && dList.size() > 0) {
			for (Department de : dList) {
				List<News> list = DaoFactory
						.getInstance()
						.getNews_dao()
						.query("select * from News where " + "departmentid=\""
								+ de.getId()
								+ "\" order by createdate desc limit " + start
								+ ",5");
				if (list != null && list.size() > 0) {
					returnList.addAll(list);
				}
			}
		}

		if (returnList != null && returnList.size() > 0) {
			return BuildResult.getListResult(BuildResult.Success, returnList);
		}
		return BuildResult.getResult(BuildResult.failed, "无置顶新闻");
		// return CommonJson.list2Json(product);
	}

	@GET
	@Path("getAllNews")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getAllNews(@QueryParam("start") int start)
			throws UnsupportedEncodingException {
		List<News> list = DaoFactory
				.getInstance()
				.getNews_dao()
				.query("select * from News order by createdate desc limit "
						+ start + ",4");
		if (list != null && list.size() > 0) {
			return BuildResult.getListResult(BuildResult.Success, list);
		}
		return BuildResult.getResult(BuildResult.failed, "无置顶新闻");
		// return CommonJson.list2Json(product);
	}

	@GET
	@Path("getNewsNumByName")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getNewsNumByName() throws UnsupportedEncodingException {
		NewsNumber newsNum = new NewsNumber();
		int[] returnNum = new int[3];
		String[] NameStr = { "产科", "生殖医学科", "儿科" };
		for (int i = 0; i < 3; i++) {
			List<Department> dList = DaoFactory.getInstance()
					.getDepartment_dao().findByProperty("name", NameStr[i]);
			if (dList != null && dList.size() > 0) {
				for (Department de : dList) {
					List<News> list = DaoFactory
							.getInstance()
							.getNews_dao()
							.query("select * from News where "
									+ "departmentid=\"" + de.getId()
									+ "\" order by createdate desc");
					if (list != null && list.size() > 0) {
						returnNum[i] += list.size();
					} else {
						returnNum[i] += 0;
					}
				}
			} else {
				returnNum[i] = 0;
			}
		}

		newsNum.setChankeNum(returnNum[0]);
		newsNum.setShengzhikeNum(returnNum[1]);
		newsNum.setErkeNum(returnNum[2]);
		return BuildResult.getResult(BuildResult.Success, newsNum);

		// return BuildResult.getResult(BuildResult.failed, "无置顶新闻");
		// return CommonJson.list2Json(product);
	}

	@GET
	@Path("getNewsByDepartment")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getNewsByDepartment(
			@QueryParam("departmentid") String departmentid,
			@QueryParam("start") int start) throws UnsupportedEncodingException {
		List<News> returnList = new ArrayList<News>();
			News top1=new News("-","无置顶新闻", "无置顶新闻", "", 1, new Timestamp(
					1), "", "");
			List<News> list = DaoFactory
					.getInstance()
					.getNews_dao()
					.query("select * from News where top=1 "
							+ "and departmentid=\"" + departmentid
							+ "\" order by createdate desc");
			if (list != null && list.size() > 0) {
				top1 = list.get(0);
			} 
			if(start == 0){
				returnList.add(top1);
			}
		List<News> list2 = DaoFactory
				.getInstance()
				.getNews_dao()
				.query("select * from News where id<>\""+top1.getId()
						+ "\" and departmentid=\"" + departmentid
						+ "\" order by createdate desc limit " + start + ",4");
		if (list2 != null && list2.size() > 0) {
			returnList.addAll(list2);
			return BuildResult.getListResult(BuildResult.Success, returnList);
		}
		return BuildResult.getListResult(BuildResult.Success, returnList);
		// return BuildResult.getResult(BuildResult.failed, "没有任何新闻");
		// return CommonJson.list2Json(product);
	}

	@GET
	@Path("getAllSuggest")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getAllSuggest() throws UnsupportedEncodingException {
		List<Suggest> list = DaoFactory.getInstance().getSuggest_dao()
				.findAll("id", false);
		if (list != null && list.size() > 0) {

			return BuildResult.getListResult(BuildResult.Success, list);
		}
		return BuildResult.getResult(BuildResult.failed, "推荐新闻为空");
	}
	
	@GET
	@Path("getHospitalSuggest")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getHospitalSuggest(@QueryParam("hospitalid") String hospitalid) throws UnsupportedEncodingException {
		
		List<News> returnList = new ArrayList<News>();
		List<Department> dList = DaoFactory.getInstance().getDepartment_dao()
				.findByProperty("hospitalid", hospitalid);
		if (dList != null && dList.size() > 0) {
			for (Department de : dList) {
				List<News> list = DaoFactory
						.getInstance()
						.getNews_dao()
						.query("select * from News where " + "departmentid=\""
								+ de.getId()
								+ "\" order by createdate desc limit " + 0
								+ ",3");
				if (list != null && list.size() > 0) {
					returnList.addAll(list);
				}
			}
		}
		
		List<Suggest> list=new ArrayList<Suggest>();
		int count=(returnList.size()>3)?3:returnList.size();
		for(int i=0;i<count;i++){
			Suggest s=new Suggest();
			s.setId(returnList.get(i).getId());
			s.setTitle(returnList.get(i).getTitle());
			s.setImagepath(returnList.get(i).getPic());
			list.add(s);
		}
		
		if (list != null && list.size() > 0) {

			return BuildResult.getListResult(BuildResult.Success, list);
		}
		return BuildResult.getResult(BuildResult.failed, "推荐新闻为空");
	}

	@GET
	@Path("queryMsg")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryMsg(@QueryParam("start") String start,
			@QueryParam("limit") String limit)
			throws UnsupportedEncodingException {

		List<Msg> list = DaoFactory
				.getInstance()
				.getMsg_dao()
				.query("select * from msg  ORDER BY createdate DESC LIMIT "
						+ start + "," + limit);
		if (list != null && list.size() > 0) {
			return BuildResult.getListResult(BuildResult.Success, list);

		}

		return BuildResult.getResult(BuildResult.failed, "操作失败");
	}

	@GET
	@Path("queryMsgById")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryMsgById(@QueryParam("id") String id)
			throws UnsupportedEncodingException {

		if (id != null && !id.equals("")) {
			try {
				List<Msg> list = DaoFactory.getInstance().getMsg_dao()
						.findByProperty("id", Long.parseLong(id));
				if (list != null && list.size() > 0) {
					return BuildResult.getResult(BuildResult.Success,
							list.get(0));

				}
			} catch (Exception ex) {
				return BuildResult.getResult(BuildResult.failed, "操作失败");
			}
		}

		return BuildResult.getResult(BuildResult.failed, "操作失败");
	}

}
