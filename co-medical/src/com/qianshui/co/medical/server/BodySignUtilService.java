package com.qianshui.co.medical.server;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.qianshui.co.medical.common.BuildResult;
import com.qianshui.co.medical.common.CommonJson;
import com.qianshui.co.medical.common.DateFormater;
import com.qianshui.co.medical.common.IDOperation;
import com.qianshui.co.medical.db.BodySign;
import com.qianshui.co.medical.db.DaoFactory;
import com.qianshui.co.medical.db.Forum;

@Path("BodySignUtilService")
public class BodySignUtilService {
	
	//得到所有的生命体征信息
	@GET
	@Path("getAllBodySign")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getAllBodySign(@QueryParam("userid") String userid) throws UnsupportedEncodingException {
		List<BodySign> list = DaoFactory.getInstance().getBodySign_dao().findByProperty("userid", userid);
		if (list != null && list.size() >= 0) {
			return BuildResult.getListResult(BuildResult.Success, list);
		}
		return BuildResult.getResult(BuildResult.failed, "没有生命体征信息");
	}
	
	//根据时间和userid得到生命体征信息
	@GET
	@Path("getBodySignByDate")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getBodySignByDate(@QueryParam("userid") String userid,@QueryParam("date") String date) 
			throws UnsupportedEncodingException {
		String[] propertyNames=new String[]{"userid",""};
		Object[] values=new Object[]{userid,DateFormater.getTimestampByString(date)};
		List<BodySign> list = DaoFactory.getInstance().getBodySign_dao().findByProperty(propertyNames, values);
		if (list != null && list.size() >= 0) {
			return BuildResult.getListResult(BuildResult.Success, list);
		}
		return BuildResult.getResult(BuildResult.failed, "没有生命体征信息");
	}
	
	//根据时间和userid得到一个月的生命体征信息
	@GET
	@Path("getBodySignByDateMonth")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getBodySignByDateMonth(@QueryParam("userid") String userid,@QueryParam("date") String date) 
			throws UnsupportedEncodingException {
		String sql="select * from body_sign where userid='"+userid+
				"' and createdate>='"+DateFormater.getCurrentMonthFirstDay(date)+
				"' and createdate<='"+DateFormater.getCurrentMonthLastDay(date)+"'";
		List<BodySign> list = DaoFactory.getInstance().getBodySign_dao().query(sql);
		if (list != null && list.size() >= 0) {
			return BuildResult.getListResult(BuildResult.Success, list);
		}
		return BuildResult.getResult(BuildResult.failed, "没有生命体征信息");
	}
	
	//添加生命体征信息
	@GET
	@Path("saveOrUpdateBodySign")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String saveOrUpdateBodySign(@QueryParam("userid") String userid,
			                  @QueryParam("date") String date,
			                  @QueryParam("json") String json)
			throws UnsupportedEncodingException {
		try {
			if (json != null && !json.equals("")) {
				json = URLDecoder.decode(json);
				BodySign bs = (BodySign) CommonJson.Json2Obj(json, BodySign.class);
				bs.setCreatedate(DateFormater.getTimestampByString(date+" 00:00:00"));
				if (bs != null) {
					//update
					if(bs.getId()!=null&&!bs.getId().equals("")){
						
					   DaoFactory.getInstance().getBodySign_dao().update(bs);
					   if (DaoFactory.getInstance().getBodySign_dao().update(bs)) {
							return BuildResult.getResult(BuildResult.Success, bs.getId());
						}
					   
					}else{//save
						
						bs.setId(IDOperation.getClassID(BodySign.class.getName()));
						if (DaoFactory.getInstance().getBodySign_dao().save(bs)) {
							return BuildResult.getResult(BuildResult.Success, bs.getId());
						}
					}
					
				}

				
    		}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return BuildResult.getResult(BuildResult.failed, "添加失败");
	}
	
	//根据时间和userid更新生命体征信息
	@GET
	@Path("updateBodySignByDate")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String updateBodySignByDate(@QueryParam("userid") String userid,
			                           @QueryParam("date") String date,
			                           @QueryParam("json") String json)
			throws UnsupportedEncodingException {
		
		try {
			if (json != null && !json.equals("")) {
				json = URLDecoder.decode(json);
				BodySign bs = (BodySign) CommonJson.Json2Obj(json, BodySign.class);
				if (bs != null) {
					bs.setUserid(userid);
					bs.setCreatedate(DateFormater.getTimestampByString(date+" 00:00:00"));
				}

				if (DaoFactory.getInstance().getBodySign_dao().update(bs)) {
					return BuildResult.getResult(BuildResult.Success, "更新成功");
				}
    		}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return BuildResult.getResult(BuildResult.failed, "更新失败");
	}

}
