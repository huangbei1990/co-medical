package com.qianshui.co.medical.server;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.qianshui.co.medical.common.BuildResult;
import com.qianshui.co.medical.db.DaoFactory;
import com.qianshui.co.medical.db.PregView;
import com.qianshui.co.medical.db.PregnantKnowledge;

@Path("PregKnowledgeUtilService")
public class PregKnowledgeUtilService {
	
	//分页读取报告列表
	@GET
	@Path("queryPregKnowledge")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryPregKnowledge(@QueryParam("Page") String page, 
			                         @QueryParam("Number") String number)
			throws UnsupportedEncodingException {
		try {
			int start=Integer.parseInt(page)*Integer.parseInt(number);
			String sql="select * from pregnant_knowledge ORDER BY createdate DESC LIMIT "+ start +"," + number;
			List<PregnantKnowledge> pk=DaoFactory.getInstance().getPregnantKnowledge_dao().query(sql);
            if (pk != null && pk.size() >= 0) {
    			return BuildResult.getListResult(BuildResult.Success, pk);
    		}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return BuildResult.getResult(BuildResult.failed, "查询失败");
	}
	
	//分页读取报告列表,view
	@GET
	@Path("queryPregKnowledgeView")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryPregKnowledgeView(@QueryParam("Page") String page, 
			                             @QueryParam("Number") String number)
			throws UnsupportedEncodingException {
		try {
			int start=Integer.parseInt(page)*Integer.parseInt(number);
			String sql="select * from preg_view ORDER BY createdate DESC LIMIT "+ start +"," + number;
			List<PregView> pk=DaoFactory.getInstance().getPregnantKnowledgeView_dao().query(sql);
            if (pk != null && pk.size() >= 0) {
    			return BuildResult.getListResult(BuildResult.Success, pk);
    		}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return BuildResult.getResult(BuildResult.failed, "查询失败");
	}
	
	//读取报告详细内容
	@GET
	@Path("queryPregKnowledgeContent")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryPregKnowledgeContent(@QueryParam("id") String id)
			throws UnsupportedEncodingException {
		try {
			List<PregnantKnowledge> pk=DaoFactory.getInstance().getPregnantKnowledge_dao().findByProperty("id", id);
            if (pk != null && pk.size() >= 0) {
    			return BuildResult.getListResult(BuildResult.Success, pk);
    		}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return BuildResult.getResult(BuildResult.failed, "查询失败");
	}

}
