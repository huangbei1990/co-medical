package com.qianshui.co.medical.server;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.qianshui.co.medical.common.BuildResult;
import com.qianshui.co.medical.db.DaoFactory;
import com.qianshui.co.medical.db.Hospital;
import com.qianshui.co.medical.db.Knowledgeclass;
import com.qianshui.co.medical.db.Knowledgeitem;

@Path("KnowledgeUtilService")
public class KnowledgeUtilService {
	
	@GET
	@Path("getKnowledgeclassByParentid")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getKnowledgeclassByParentid(
			@QueryParam("id") String id)
			throws UnsupportedEncodingException {
			List<Knowledgeclass> list = DaoFactory
					.getInstance()
					.getKnowledgeclass_dao()
					.query("select * from Knowledgeclass where parentid=\"" + id
							+ "\"");
			if (list != null && list.size() > 0) {
				return BuildResult.getListResult(BuildResult.Success, list);
			}
			
		return BuildResult.getResult(BuildResult.failed, "没有相关内容");
	}
	
	
	@GET
	@Path("getKnowledgeitemByParentid")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getKnowledgeitemByParentid(
			@QueryParam("id") String id)
			throws UnsupportedEncodingException {
		List<Knowledgeitem> list = DaoFactory
				.getInstance()
				.getKnowledgeitem_dao()
				.query("select * from Knowledgeitem where parentid=\"" + id
						+ "\"");
		for(Knowledgeitem item:list){
			item.setInfo("");
		}
		if (list != null && list.size() > 0) {
			return BuildResult.getListResult(BuildResult.Success, list);
		}	
		return BuildResult.getResult(BuildResult.failed, "没有相关内容");
	}
	
	
	@GET
	@Path("getKnowledgeitemByid")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getKnowledgeitemByid(
			@QueryParam("id") String id)
			throws UnsupportedEncodingException {
		List<Knowledgeitem> list = DaoFactory
				.getInstance()
				.getKnowledgeitem_dao()
				.query("select * from Knowledgeitem where id=\"" + id
						+ "\"");
		if (list != null && list.size() > 0) {
			return BuildResult.getResult(BuildResult.Success, list.get(0));
		}else if(list !=null && list.size() ==0){
			return BuildResult.getResult(BuildResult.Success, "noContent");
		}
		return BuildResult.getResult(BuildResult.failed, "查询出错");
	}
	
	
	
	@GET
	@Path("isLastClass")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String isLastClass(
			@QueryParam("id") String id)
			throws UnsupportedEncodingException {
		List<Knowledgeclass> list = DaoFactory
				.getInstance()
				.getKnowledgeclass_dao()
				.query("select * from Knowledgeclass where parentid=\"" + id
						+ "\"");
		if ( list !=null && list.size() > 0 ) {
			return BuildResult.getResult(BuildResult.Success, "false");
		}else if(list !=null && list.size()==0){
			return BuildResult.getResult(BuildResult.Success, "true");
		}
		return BuildResult.getResult(BuildResult.failed, "查询失败，请待会重试");
		// return CommonJson.list2Json(product);
	}
	
	@GET
	@Path("getKnowledgeitemByName")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getKnowledgeitemByName(@QueryParam("name") String name)
			throws UnsupportedEncodingException {
		List<Knowledgeitem> list = DaoFactory.getInstance().getKnowledgeitem_dao()
				.query("select * from Knowledgeitem where name like '%"+name+"%'");
		List<Knowledgeclass> returnList = new ArrayList<Knowledgeclass>();
		if (list != null && list.size() > 0) {
			for(Knowledgeitem ki:list){
				List<Knowledgeclass> list2 = DaoFactory.getInstance().getKnowledgeclass_dao()
						.findByProperty("id", ki.getId());
				if(list2!=null && list2.size()>0){
					returnList.addAll(list2);
				}				
			}
			return BuildResult.getListResult(BuildResult.Success, returnList);
		}else if(list != null && list.size() == 0){
			return BuildResult.getResult(BuildResult.failed, "没有相关内容");
		}
		
		return BuildResult.getResult(BuildResult.failed, "查询失败，请待会再试");
	}
	
}
