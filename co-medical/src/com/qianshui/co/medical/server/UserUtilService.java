package com.qianshui.co.medical.server;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.qianshui.co.medical.business.user.UserOperator;
import com.qianshui.co.medical.common.BuildResult;
import com.qianshui.co.medical.common.CommonJson;
import com.qianshui.co.medical.common.SMSCommon;
import com.qianshui.co.medical.db.Contacus;
import com.qianshui.co.medical.db.DaoFactory;
import com.qianshui.co.medical.db.Servicelaw;
import com.qianshui.co.medical.db.User;
import com.qianshui.co.medical.db.Userdayinfo;
import com.qianshui.co.medical.db.Userinfo;
import com.qianshui.co.medical.db.Userbmi;

@Path("UserUtilService")
public class UserUtilService {

	@GET
	@Path("userRegister")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String userRegister(@QueryParam("username") String username,
			@QueryParam("loginname") String loginname,
			@QueryParam("phone") String phone,
			@QueryParam("password") String pass, @QueryParam("code") String code)
			throws UnsupportedEncodingException {
		try {
			if (SMSCommon.ValidateCode(phone, code)) {
				User user = UserOperator.getInstance().Register(username,
						loginname, phone, pass);
				if (user != null) {
					
					return BuildResult.getResult(BuildResult.Success, user);

				}
			}else{
				return BuildResult.getResult(BuildResult.failed, "验证码错误");
			}
		} catch (Exception ex) {
			return BuildResult.getResult(BuildResult.failed, ex.getMessage());
		}
		return BuildResult.getResult(BuildResult.failed, "注册失败");// return
																	// CommonJson.list2Json(product);
	}

	@GET
	@Path("checkRegister")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String checkRegister(@QueryParam("phone") String phone,@QueryParam("username") String username)
			throws UnsupportedEncodingException {
		boolean rs = UserOperator.getInstance().CheckPhone(phone);
		if(!rs){
			return BuildResult.getResult(BuildResult.failed, "该手机号已注册");
		}else{
			rs = UserOperator.getInstance().CheckName(username);
			if(!rs){
				return BuildResult.getResult(BuildResult.failed, "用户名已注册");
				}
		}

		return BuildResult.getResult(BuildResult.Success, "success");
		// return CommonJson.list2Json(product);
	}

	@GET
	@Path("userLogin")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String userLogin(@QueryParam("loginname") String loginname,
			@QueryParam("password") String pass)
			throws UnsupportedEncodingException {
		User user = UserOperator.getInstance().Login(loginname, pass);
		if (user != null) {
			return BuildResult.getResult(BuildResult.Success, user);
		}
		return BuildResult.getResult(BuildResult.failed, "登录失败");
		// return URLEncoder.encode("", "utf-8");
		// return CommonJson.list2Json(product);
	}

	@GET
	@Path("getAllUser")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getAllUser() throws UnsupportedEncodingException {
		List<User> list = UserOperator.getInstance().getAllUser();
		if (list != null && list.size() > 0) {
			return BuildResult.getListResult(BuildResult.Success, list);
		}
		return BuildResult.getResult(BuildResult.failed, "user is null");
		// return CommonJson.list2Json(product);
	}

	@GET
	@Path("getUserinfo")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getUserinfo(@QueryParam("userid") String userid)
			throws UnsupportedEncodingException {
		if (userid != null && !userid.equals("")) {
			List<Userinfo> list = DaoFactory.getInstance().getUser_info_dao()
					.findByProperty("userid", userid);
			if (list != null && list.size() > 0) {
				return BuildResult.getListResult(BuildResult.Success, list);
			}
		}
		return BuildResult.getResult(BuildResult.failed, "暂无用户信息");
		// return CommonJson.list2Json(product);
	}
	
	@GET
	@Path("getContactus")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getContactus()
			throws UnsupportedEncodingException {

			List<Contacus> list = DaoFactory.getInstance().getContacus_dao()
					.findAll("idcontacus", false);
			if (list != null && list.size() > 0) {
				return BuildResult.getResult(BuildResult.Success, list.get(0));
			}
		
		return BuildResult.getResult(BuildResult.failed, "暂无练习我们相关信息");
		// return CommonJson.list2Json(product);
	}
	
	@GET
	@Path("getServiceLaw")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getServiceLaw()
			throws UnsupportedEncodingException {

			List<Servicelaw> list = DaoFactory.getInstance().getServicelaw_dao()
					.findAll("idservicelaw", false);
			if (list != null && list.size() > 0) {
				return BuildResult.getResult(BuildResult.Success, list.get(0));
			}
		
		return BuildResult.getResult(BuildResult.failed, "暂无服务条款相关信息");
		// return CommonJson.list2Json(product);
	}

	@GET
	@Path("updateUserinfo")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String updateUserinfo(@QueryParam("userid") String userid,
			@QueryParam("json") String json)
			throws UnsupportedEncodingException {
		try {

			if (json != null && !json.equals("")) {
				json = URLDecoder.decode(json);
				Userinfo userinfo = (Userinfo) CommonJson.Json2Obj(json,
						Userinfo.class);
				userinfo.setUserid(userid);
				if (userinfo != null) {
					DaoFactory.getInstance().getUser_info_dao()
							.saveOrUpdate(userinfo);
					return BuildResult.getResult(BuildResult.Success,
							"更新个人信息成功");
				}
			}
		} catch (Exception ex) {
			ex.fillInStackTrace();
		}

		return BuildResult.getResult(BuildResult.failed, "更新用户信息错误");
		// return CommonJson.list2Json(product);
	}
	
	@GET
	@Path("getUserdayinfo")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getUserdayinfo(@QueryParam("userid") String userid,@QueryParam("start") String start
			,@QueryParam("end") String end)
			throws UnsupportedEncodingException {
		if (userid != null && !userid.equals("")) {
			List<Userdayinfo> list = DaoFactory.getInstance().getUserdayinfo_dao()
					.query("select * from Userdayinfo where userid=\""+userid+"\" and inputdate>='"+start+"' and inputdate<='"+end+"'");
			if (list != null && list.size() > 0) {
				return BuildResult.getListResult(BuildResult.Success, list);
			}else if(list != null && list.size() ==0){
				return BuildResult.getResult(BuildResult.failed, "无内容");
			}
		}
		return BuildResult.getResult(BuildResult.failed, "查询失败");
		// return CommonJson.list2Json(product);
	}
	
	@GET
	@Path("getUserWeight")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getUserWeight(@QueryParam("userid") String userid)
			throws UnsupportedEncodingException {
		if (userid != null && !userid.equals("")) {
			List<Userbmi> list = DaoFactory.getInstance().getUserbmi_dao()
					.query("select * from Userbmi where userid=\""+userid+"\" order by weeknum asc");
					//.findByProperty("userid", userid);
			if (list != null && list.size() > 0) {
				return BuildResult.getListResult(BuildResult.Success, list);
			}
		}
		return BuildResult.getResult(BuildResult.failed, "暂无用户信息");
		// return CommonJson.list2Json(product);
	}
	
	@GET
	@Path("updateUserWeight")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String updateUserWeight(@QueryParam("userid") String user_id,
			@QueryParam("weight") double weight,
			@QueryParam("week") int week)
			throws UnsupportedEncodingException {
		try {
			if (user_id!=null && !user_id.equals("")) {
				String userid=URLDecoder.decode(user_id);
				
				List<Userbmi> list = DaoFactory.getInstance().getUserbmi_dao()
				.query("select * from Userbmi where userid=\""+userid+"\" and weeknum="+week);
				Userbmi bmi = null;
				if(list!=null && list.size()>0){
					bmi = list.get(0);
				}else{
					bmi = new Userbmi();
				}
							 
				bmi.setUserid(userid);
				bmi.setWeeknum(week);
				bmi.setWeight(weight);
					//保存体重
					DaoFactory.getInstance().getUserbmi_dao().saveOrUpdate(bmi);
					return BuildResult.getResult(BuildResult.Success,
							"提交体重成功");				
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return BuildResult.getResult(BuildResult.failed, "提交体重成功");
	}
	

	@GET
	@Path("getCode")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getCode(@QueryParam("phone") String phone)
			throws UnsupportedEncodingException {
		try {
			return BuildResult.getResult(BuildResult.Success,
					SMSCommon.SendSMSMEssage(phone));

		} catch (Exception ex) {

		}

		return BuildResult.getResult(BuildResult.failed, "更新用户信息错误");
		// return CommonJson.list2Json(product);
	}
	
	@GET
	@Path("forgetPass")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String forgetPass(@QueryParam("phone") String phone)
			throws UnsupportedEncodingException {
		try {
			return BuildResult.getResult(BuildResult.Success,
					SMSCommon.SendSMSPassMessage(phone));

		} catch (Exception ex) {

		}

		return BuildResult.getResult(BuildResult.failed, "更新用户信息错误");
		// return CommonJson.list2Json(product);
	}

	@GET
	@Path("updateUserPassword")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String updateUserPassword(@QueryParam("loginname") String loginname,
			@QueryParam("oldPassword") String oldPassword,
			@QueryParam("newPassword") String newPassword)
			throws UnsupportedEncodingException {
		try {

			if (UserOperator.getInstance().ChangePassword(loginname,
					oldPassword, newPassword)) {
				
				return BuildResult.getResult(BuildResult.Success, "更新密码成功");
			}

			
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return BuildResult.getResult(BuildResult.failed, "更新密码失败");
	}
	
	@GET
	@Path("updateUserAndUserinfo")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String updateUserAndUserinfo(@QueryParam("user") String json_user,
			@QueryParam("userinfo") String json_userinfo)
			throws UnsupportedEncodingException {
		try {
			if (json_userinfo != null && !json_userinfo.equals("") && json_user!=null&&!json_user.equals("")) {
				json_userinfo = URLDecoder.decode(json_userinfo);
				Userinfo userinfo = (Userinfo) CommonJson.Json2Obj(json_userinfo,Userinfo.class);
				
				json_user=URLDecoder.decode(json_user);
				User user=(User) CommonJson.Json2Obj(json_user,User.class);
				
				if (userinfo != null && user!=null ) {
					//保存用户信息
					DaoFactory.getInstance().getUser_info_dao().saveOrUpdate(userinfo);
					//保存用户
					DaoFactory.getInstance().getUser_dao().saveOrUpdate(user);
					return BuildResult.getResult(BuildResult.Success,
							"更新个人信息成功");
				}
				return BuildResult.getResult(BuildResult.failed, "个人信息更新失败");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return BuildResult.getResult(BuildResult.failed, "个人信息更新失败");
	}
	

}
