package com.qianshui.co.medical.server;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.qianshui.co.medical.business.doctor.DoctorOperator;
import com.qianshui.co.medical.business.user.UserOperator;
import com.qianshui.co.medical.common.BuildResult;
import com.qianshui.co.medical.common.CommonJson;
import com.qianshui.co.medical.common.DateFormater;
import com.qianshui.co.medical.common.DocArrangeOperation;
import com.qianshui.co.medical.common.IDOperation;
import com.qianshui.co.medical.db.Area;
import com.qianshui.co.medical.db.Arrange;
import com.qianshui.co.medical.db.City;
import com.qianshui.co.medical.db.DaoFactory;
import com.qianshui.co.medical.db.Department;
import com.qianshui.co.medical.db.DocArrange;
import com.qianshui.co.medical.db.Doctor;
import com.qianshui.co.medical.db.DoctorDAO;
import com.qianshui.co.medical.db.Hospital;
import com.qianshui.co.medical.db.Score;
import com.qianshui.co.medical.db.ScoreView;
import com.qianshui.co.medical.db.User;
import com.qianshui.co.medical.db.Userinfo;

@Path("DoctorUtilService")
public class DoctorUtilService {
	
	@GET
	@Path("getAllCity")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getAllCity() throws UnsupportedEncodingException {
		List<City> list = DoctorOperator.getInstance().getAllCity();
		if (list != null && list.size() > 0) {
			return BuildResult.getListResult(BuildResult.Success, list);
		}
		return BuildResult.getResult(BuildResult.failed, "没有城市信息");
		// return CommonJson.list2Json(product);
	}
	
	@GET
	@Path("getAllArea")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getAllArea() throws UnsupportedEncodingException {
		List<Area> list = DaoFactory.getInstance().getArea_dao().findAll("id", true);
		if (list != null && list.size() > 0) {
			return BuildResult.getListResult(BuildResult.Success, list);
		}
		return BuildResult.getResult(BuildResult.failed, "没有区域信息");
		// return CommonJson.list2Json(product);
	}
	
	@GET
	@Path("getCityByName")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getCityByName(@QueryParam("name") String name) throws UnsupportedEncodingException {
		List<City> list = DaoFactory.getInstance().getCity_dao().findByProperty("name", name);
		if (list != null && list.size() > 0) {
			return BuildResult.getListResult(BuildResult.Success, list);
		}
		return BuildResult.getResult(BuildResult.failed, "没有该名字的城市");
		// return CommonJson.list2Json(product);
	}
	
	@GET
	@Path("getAreaByCity")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getAreaByCity(@QueryParam("cityid") String id) 
			throws UnsupportedEncodingException {
		List<Area> list = DaoFactory.getInstance().getArea_dao()
				  .findByProperty("cityid", id);                                        //DoctorOperator.getInstance().getAllCity();
		if (list != null && list.size() > 0) {
			return BuildResult.getListResult(BuildResult.Success, list);
		}
		return BuildResult.getResult(BuildResult.failed, "该城市没有区域信息");
		// return CommonJson.list2Json(product);
	}
	
	@GET
	@Path("getHospitalByCityAndLevel")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getHospitalByCityAndLevel(@QueryParam("cityid") String cityid,
			@QueryParam("level") String level) throws UnsupportedEncodingException {
		List<Hospital> list = DaoFactory.getInstance().getHospital_dao()
				       .getHospitalByDengjiCity(level, cityid);                                  //DoctorOperator.getInstance().getAllCity();
		if (list != null && list.size() > 0) {
			return BuildResult.getListResult(BuildResult.Success, list);
		}
		return BuildResult.getResult(BuildResult.failed, "没有该等级的医院");
		// return CommonJson.list2Json(product);
	}
	
	
	@GET
	@Path("getHospitalByArea")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getHospitalByArea(@QueryParam("areaid") String areaid) throws UnsupportedEncodingException {
		List<Hospital> list = DaoFactory.getInstance().getHospital_dao().findByProperty("areaid", areaid);                                  //DoctorOperator.getInstance().getAllCity();
		if (list != null && list.size() > 0) {
			return BuildResult.getListResult(BuildResult.Success, list);
		}
		return BuildResult.getResult(BuildResult.failed, "没有该区域的医院");
		// return CommonJson.list2Json(product);
	}

	@GET
	@Path("queryHospitalById")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryHospitalById(@QueryParam("id") String id)
			throws UnsupportedEncodingException {
		List<Hospital> list = DaoFactory.getInstance().getHospital_dao()
				.findByProperty("id", id);
		if (list != null && list.size() > 0) {
			return BuildResult.getResult(BuildResult.Success, list.get(0));
		}
		return BuildResult.getResult(BuildResult.failed, "没有该ID的医院");
	}
	
	@GET
	@Path("queryHospitalByName")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryHospitalByName(@QueryParam("name") String name)
			throws UnsupportedEncodingException {
		List<Hospital> list = DaoFactory.getInstance().getHospital_dao()
				.query("select * from Hospital where name like '%"+name+"%'");
		if (list != null && list.size() > 0) {
			return BuildResult.getListResult(BuildResult.Success, list);
		}
		return BuildResult.getResult(BuildResult.failed, "没有该医院");
	}
	
	
	
	@GET
	@Path("getHospitalAll")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getHospitalAll()
			throws UnsupportedEncodingException {
		List<Hospital> list = DaoFactory.getInstance().getHospital_dao()
				.findAll("id", true);
		if (list != null && list.size() > 0) {
			return BuildResult.getListResult(BuildResult.Success, list);
		}
		return BuildResult.getResult(BuildResult.failed, "没有任何医院信息");
	}
	
	@GET
	@Path("getHospitalAllByCity")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getHospitalAllByCity(@QueryParam("cityid") String cityid)
			throws UnsupportedEncodingException {
		List<Hospital> list = DaoFactory.getInstance().getHospital_dao()
				.getHospitalAllByCity(cityid);
		if (list != null && list.size() > 0) {
			return BuildResult.getListResult(BuildResult.Success,list);
		}
		return BuildResult.getResult(BuildResult.failed, "没有任何医院信息");
	}

	@GET
	@Path("queryHospitalByArea")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryHospitalByArea(@QueryParam("areaid") String areaid)
			throws UnsupportedEncodingException {
		List<Hospital> list = DaoFactory.getInstance().getHospital_dao()
				.findByProperty("areaid", areaid);
		if (list != null && list.size() > 0) {
			return BuildResult.getListResult(BuildResult.Success, list);
		}
		return BuildResult.getResult(BuildResult.failed, "没有医院信息");
	}

	@GET
	@Path("queryDepartmentByHospital")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryDepartmentByHospital(
			@QueryParam("hospitalid") String hospitalid)
			throws UnsupportedEncodingException {
		List<Department> list = DaoFactory.getInstance().getDepartment_dao()
				.findByProperty("hospitalid", hospitalid);
		if (list != null && list.size() > 0) {
			return BuildResult.getListResult(BuildResult.Success, list);
		}
		return BuildResult.getResult(BuildResult.failed, "没有科室信息");
	}
	
	@GET
	@Path("queryDepartmentByHospitalAndName")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryDepartmentByHospitalAndName(
			@QueryParam("hospitalid") String hospitalid,@QueryParam("name") String name)
			throws UnsupportedEncodingException {
		List<Department> list = DaoFactory.getInstance().getDepartment_dao()
				.findByProperty(new String[]{"hospitalid","name"}, new String[]{hospitalid,name});
		if (list != null && list.size() > 0) {
			return BuildResult.getListResult(BuildResult.Success, list);
		}
		return BuildResult.getResult(BuildResult.failed, "没有部门信息");
	}

	@GET
	@Path("queryDepartmentById")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryDepartmentById(@QueryParam("id") String id)
			throws UnsupportedEncodingException {
		List<Department> list = DaoFactory.getInstance().getDepartment_dao()
				.findByProperty("id", id);
		if (list != null && list.size() > 0) {
			return BuildResult.getResult(BuildResult.Success, list.get(0));
		}
		return BuildResult.getResult(BuildResult.failed, "没有该部门信息");
	}

	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryDoctorById(@QueryParam("id") String id)
			throws UnsupportedEncodingException {
		List<Doctor> list = DaoFactory.getInstance().getDoctor_dao()
				.findByProperty("id", id);
		if (list != null && list.size() > 0) {
			return BuildResult.getResult(BuildResult.Success, list.get(0));
		}
		return BuildResult.getResult(BuildResult.failed, "没有该医生信息");
	}

	@GET
	@Path("queryDoctorByDepartment")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryDoctorByDepartment(
			@QueryParam("departmentid") String deparmentid)
			throws UnsupportedEncodingException {
		List<Doctor> list = DaoFactory.getInstance().getDoctor_dao()
				.query("select * from doctor where departmentid='"+deparmentid+"' order by sort asc");
		if (list != null && list.size() > 0) {
			return BuildResult.getListResult(BuildResult.Success, list);
		}
		return BuildResult.getResult(BuildResult.failed, "没有医生信息");
	}

	@GET
	@Path("queryArrangeByDoctor")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryArrangeByDoctor(@QueryParam("doctorid") String doctorid,
			@QueryParam("startdate") String startdate,
			@QueryParam("enddate") String enddate)
			throws UnsupportedEncodingException {
		List<DocArrange> list = DaoFactory.getInstance().getArrange_dao()
				.QueryArrangeByDoctor(doctorid, startdate, enddate);
		if (list != null && list.size() > 0) {
			return BuildResult.getListResult(BuildResult.Success, list);
		}
		return BuildResult.getResult(BuildResult.failed, "没有安排计划");
	}
	
	@GET
	@Path("queryArrangeByDepartment")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryArrangeByDepartment(@QueryParam("departmentid") String departmentid,
			@QueryParam("startdate") String startdate,
			@QueryParam("enddate") String enddate)
			throws UnsupportedEncodingException {
		List<DocArrange> list = DocArrangeOperation.getArrangeByDepartment(departmentid, startdate, enddate);
		if (list != null && list.size() > 0) {
			return BuildResult.getListResult(BuildResult.Success, list);
		}
		return BuildResult.getResult(BuildResult.failed, "没有安排计划");
	}
	
	
	

	@GET
	@Path("addScore")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String addScore(@QueryParam("json") String json)
			throws UnsupportedEncodingException {
		if (json != null && !json.equals("")) {
			json = URLDecoder.decode(json);
			Score score = (Score) CommonJson.Json2Obj(json, Score.class);
			if (score != null) {
				score.setId(IDOperation.getClassID(Score.class.getName()));
				score.setCreatedate(DateFormater.getTimestampByString(DateFormater.formatLongDateTime(new Date())) );
			}

			if (DaoFactory.getInstance().getScore_dao().save(score)) {
				return BuildResult.getResult(BuildResult.Success, "评分完成");
			}
		}
		return BuildResult.getResult(BuildResult.failed, "评分失败");
	}
	
	//读取评分过的医生列表
	@GET
	@Path("queryUserScored")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String queryUserScored(@QueryParam("userid") String userid,
									@QueryParam("Page") String page, 
							        @QueryParam("Number") String number)
			throws UnsupportedEncodingException {
		    
//		    String sql="select * from score where userid='"+userid+"' and createdate>='"+DateFormater.getCurrentDate()+"'";
//	        List<Score> list=DaoFactory.getInstance().getScore_dao().query(sql);
		    int start=Integer.parseInt(page)*Integer.parseInt(number);
		    String sql="select * from score_view where userid='"+userid+"' ORDER BY createdate DESC LIMIT "+ start +"," + number;
			List<ScoreView> list=DaoFactory.getInstance().getScoreView_dao().query(sql);	    
			if (list != null && list.size()>=0) {
			return BuildResult.getListResult(BuildResult.Success, list);	
		}
		return BuildResult.getResult(BuildResult.failed, "读取失败");
	}
	
	//医生今天是否被评分过
	@GET
	@Path("isDoctorScored")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String isDoctorScored(@QueryParam("userid") String userid,@QueryParam("Doctorid") String Doctorid)
			throws UnsupportedEncodingException {
		    String sql="select * from score where userid='"+userid+"' and createdate>='"+DateFormater.getCurrentDate()+
		    		"' and doctorid='"+Doctorid+"'";
//				List<Score> list=DaoFactory.getInstance().getScore_dao().findByProperty("userid", userid);
		    List<Score> list=DaoFactory.getInstance().getScore_dao().query(sql);
			if (list != null && list.size()>=0) {
			return BuildResult.getListResult(BuildResult.Success, list);	
		}
		return BuildResult.getResult(BuildResult.failed, "读取失败");
	}
}
