package com.qianshui.co.medical.server;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//上传图片
public class FileUpLoad extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public FileUpLoad() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
		out.println("<HTML>");
		out.println("  <HEAD><TITLE>A Servlet</TITLE></HEAD>");
		out.println("  <BODY>");
		out.print("    This is ");
		out.print(this.getClass());
		out.println(", using the GET method");
		out.println("  </BODY>");
		out.println("</HTML>");
		out.flush();
		out.close();
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
 
		String tag="";
		String id="";
		String path="";
       try{
    	    tag=request.getParameter("TAG").toString();
    	    if(tag.equals("questionID")){
    	       id=request.getParameter("questionID").toString();
    	       path=this.getServletContext().getRealPath("/Images/question")+"\\";
    	    }else if(tag.equals("answerID")){
    	    	id=request.getParameter("answerID").toString();
    	    	path=this.getServletContext().getRealPath("/Images/question")+"\\";
    	    }else if(tag.equals("forumID")){
    	    	id=request.getParameter("forumID").toString();
    	    	path=this.getServletContext().getRealPath("/Images/forum")+"\\";
    	    }else if(tag.equals("replyID")){
    	    	id=request.getParameter("replyID").toString();
    	    	path=this.getServletContext().getRealPath("/Images/forum")+"\\";
    	    }else if(tag.equals("reportID")){
    	    	id=request.getParameter("reportID").toString();
    	    	path=this.getServletContext().getRealPath("/Images/report")+"\\";
    	    }else if(tag.equals("reportAnswerID")){
    	    	id=request.getParameter("reportAnswerID").toString();
    	    	path=this.getServletContext().getRealPath("/Images/report")+"\\";
    	    }
    	    String imagePath="";
            
			ServletInputStream is=request.getInputStream();
			byte[] buf=new byte[2048];
			int c=0;
			
			while((c=is.readLine(buf, 0, buf.length))!=-1){
				
				String msg=new String(buf,0,c);
				//System.out.println(msg);
				
				if(msg.endsWith("--")){
					
					break;
				}
				
				if(msg.indexOf("filename=\"")!=-1){
					
					String fileName = "";
					fileName = msg
							.substring(msg.indexOf("filename=\"") + 10);
					fileName = fileName.substring(0, fileName
							.indexOf("\""));
					fileName = fileName.substring(fileName
							.lastIndexOf("\\") + 1);
					
					is.readLine(buf, 0, buf.length);
					is.readLine(buf, 0, buf.length);
					
					imagePath=System.currentTimeMillis()+fileName.substring(fileName.lastIndexOf("."));
					FileOutputStream fos=new FileOutputStream(path+imagePath);
					
					while((c=is.readLine(buf, 0, buf.length))!=-1){
						msg=new String(buf,0,c);
						if(msg.startsWith("-------------------")){
							
							break;
						}
						fos.write(buf,0,c);	
					}
					fos.close();
				}
	
			}
           //写入数据库
			if(tag.equals("questionID")){
                new AdvisoryUtilService().addQuestionImage(id, imagePath);
			}else if(tag.equals("answerID")){
				new AdvisoryUtilService().addAnswerImage(id, imagePath);
			}else if(tag.equals("forumID")){
				new ForumUtilService().addForumImage(id, imagePath);
			}else if(tag.equals("replyID")){
				new ForumUtilService().addReplyImage(id, imagePath);
			}else if(tag.equals("reportID")){
				new ReportUtilService().addReportImage(id, imagePath);
			}else if(tag.equals("reportAnswerID")){
				new ReportUtilService().addReportAnswerImage(id, imagePath);
			}
	
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		
	}

}
